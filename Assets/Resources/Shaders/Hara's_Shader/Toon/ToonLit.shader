﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/12

＜内容＞
・

---------------------------------------------------------------------------------------*/
Shader "Toon/Lit" 
{
	///-------------------------------------------------------------- 
	/// プロパティ
	///--------------------------------------------------------------  
    Properties 
	{
        _Color ("Main Color", Color) = (0.5,0.5,0.5,1)
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _Ramp ("Toon Ramp (RGB)", 2D) = "gray" {} 
    }

	///--------------------------------------------------------------
	///サブシェーダ
	///--------------------------------------------------------------
    SubShader 
	{
        Tags { "RenderType"="Opaque" }
        LOD 200
		CGPROGRAM
		#pragma surface surf ToonRamp
		#pragma lighting ToonRamp exclude_path:prepass

		///--------------------------------------------------------------
		/// 変数
		///-------------------------------------------------------------- 
		float4 _Color;
		sampler2D _MainTex;
		sampler2D _Ramp;

		///-------------------------------------------------------------- 
		/// 構造体
		///-------------------------------------------------------------- 
		struct Input 
		{
			float2 uv_MainTex : TEXCOORD0;
		};

		///-------------------------------------------------------------- 
		/// トゥーンマッピングでのライティング処理
		///-------------------------------------------------------------- 
		inline half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half atten)
		{
			#ifndef USING_DIRECTIONAL_LIGHT
			lightDir = normalize(lightDir);
			#endif
    
			half d = dot (s.Normal, lightDir)*0.5 + 0.5;
			half3 ramp = tex2D (_Ramp, float2(d,d)).rgb;
    
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
			c.a = 0;
			return c;
		}

		///-------------------------------------------------------------- 
		/// ピクセルシェーダ
		///-------------------------------------------------------------- 
		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG

    } 

    Fallback "Diffuse"
}