﻿Shader "Custom/StencilBackShader"
{
	//---------------------------------------------
	// プロパティ
	//---------------------------------------------
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color("Color", Color) = (1, 1, 1)
	}
 
	//---------------------------------------------
	// サブシェーダ
	//---------------------------------------------
	SubShader
	{
		//---------------------------------------------
		// 初期設定
		//---------------------------------------------
        Tags 
		{
			"Queue"="Transparent - 2"
			"IgnoreProjector" = "True"
			"RenderType"="Transparent"
		}

        ZWrite Off								// 不透明を有効
        Cull Off								// カリングなし
        Blend SrcAlpha OneMinusSrcAlpha			// アルファブレンディングを有効
		
		//---------------------------------------------
		// ステンシルの設定
		//---------------------------------------------
        Stencil 
		{
            Ref 1				// ステンシルを比較するための数値
            Comp NotEqual		// 参照値がバッファ値と等しくない参照値のピクセルのみレンダリング
        }
     
		//---------------------------------------------
		// パス
		//---------------------------------------------
        Pass
        {
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

			//---------------------------------------------
			// 変数
			//---------------------------------------------
            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _Color;

			//---------------------------------------------
			// バーテックス用の構造体
			//---------------------------------------------
            struct appdata_t
            {
				float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };
 
			//---------------------------------------------
			// フラグメント用の構造体
			//---------------------------------------------
			struct v2f
            {
				float4 vertex : SV_POSITION;
                half2 texcoord : TEXCOORD0;
            };

			//---------------------------------------------
			// バーテックス処理
			//--------------------------------------------- 
            v2f vert (appdata_t v)
            {
				v2f o;
                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
            }
                     
			//---------------------------------------------
			// フラグメント処理
			//---------------------------------------------
            fixed4 frag (v2f i) : COLOR
            {
				// テクスチャを適応させただけの描画
				fixed4 col = tex2D(_MainTex, i.texcoord) * _Color;
                return col;
            }

            ENDCG
		}
	}
}
