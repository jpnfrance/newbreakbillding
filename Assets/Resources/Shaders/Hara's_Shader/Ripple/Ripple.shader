﻿Shader "Custom/RippleShader" 
{
	//---------------------------------------------
	// プロパティ
	//---------------------------------------------
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" {}
		_Scale("Scale", Range(0.5,500.0)) = 3.0
		_Speed("Speed", Range(-50,50.0)) = 1.0
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	}

	//---------------------------------------------
	// サブシェーダ
	//---------------------------------------------
	SubShader
	{
		//---------------------------------------------
		// 初期化処理
		//---------------------------------------------
		Tags 
		{
			"Queue"="AlphaTest"
			"IgnoreProjector"="True" 
			"RenderType"="TransparentCutout"
		}
		LOD 200
		Cull Off
		AlphaTest Greater 0.5		
		Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
		#pragma surface surf Lambert alphatest:_Cutoff
		#include "UnityCG.cginc"

		//---------------------------------------------
		// 変数
		//---------------------------------------------
		half4 _Color;
		half _Scale;
		half _Speed;
		sampler2D _MainTex;

		//---------------------------------------------
		// 入力用の構造体
		//---------------------------------------------
		struct Input 
		{
			float2 uv_MainTex;
		};

		//---------------------------------------------
		// 描画処理
		//---------------------------------------------
		void surf(Input IN, inout SurfaceOutput o) 
		{
			half2 uv = (IN.uv_MainTex - 0.5) * _Scale;
			half r = sqrt(uv.x*uv.x + uv.y*uv.y);
			half z = sin(r + _Time[1] * _Speed) / r;
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex + z) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Normal = (z, z, z);
			
			// アルファ値が0.1以下なら描画自体しない
			if( o.Alpha < 0.1 ) discard;
		}
		ENDCG
	}
	FallBack "Diffuse"
}