﻿using UnityEngine;
using System.Collections;

public class ItemScript : MonoBehaviour {

    public int addBombNum = 5;

    public RoomScript.bombs bombId;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // ボムの追加
            other.GetComponent<PlayerControl>().AddBomb((int)this.bombId, this.addBombNum);
            //Debug.Log("waiwaiwai");
            this.gameObject.SetActive(false);
        }
    }
}
