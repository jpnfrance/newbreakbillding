﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RoomFurnituresObject //Not a MonoBehaviour!
{
    public GameObject leftDoorObject    ;
    public GameObject rightDoorObject   ;
    public GameObject stairObject       ;
    public Transform playerResporn      ;
    public Transform ladderResporn      ;
    public StairScript StairUpTrigger   ;
    public GameObject blast             ;
    public GameObject wideBombItem      ;
    public GameObject verticalBombItem  ;
}

[System.Serializable]
public class BombObjects //Not a MonoBehaviour!
{
    public GameObject normalBomb;
    public GameObject verticalBomb;
    public GameObject wideBomb;
}

[System.Serializable]
public class RoomDesign //Not a MonoBehaviour!
{
    public bool stair = false;
    public bool rightDoor = false;
    public bool leftDoor = false;
    public RoomScript.Items item = RoomScript.Items.NONE;

    public bool isStartRoom = false;
}

public class RoomScript : MonoBehaviour {

    // いーなむ達
    enum verticals      // 上下のイーナム 
    {
        UP = 1,
        DOWN = -1,
    }    
    public enum bombs   // ボムの種類
    {
        NORMAL_BOMB = 0,
        WIDEBOMS,
        VERTICALBOMB,
    }
    public enum Items   // アイテムの種類
    {
        NONE = 0,
        WIDE_ITEM,
        VERTICAL_ITEM,
    }

    public bool deathTrigger = false;   // 死ぬトリガー

    public RoomFurnituresObject furnitures; // 上参照
    public RoomDesign roomDesign;           // 上参照
    public BombObjects boms;                // 上参照

    public GameObject createExprojonPrefab;

	private int roomX;
	private int roomY;

    protected Vector3 firstPosition;      // 初期位置

    private bool resetTrigger;
    private bool checkTrigger;

    private GameObject theGameObject = null;    // トリガーで使う入れ物
    private RoomScript upRoomScript = null;
    private RoomScript downRoomScript = null;
    private RoomScript leftRoomScript = null;
    private RoomScript rightRoomScript = null;
    private BombManager theBombManager = null;

    private Transform downRoomResporn;

    private bool rightRoom = false;
    private bool leftRoom = false;
    private bool rightRoomLeftDoorTrigger = false;
    private bool leftRoomRightDoorTrigger = false;
    private bool upRoomTrigger = false;
    private bool downRoomStairTrigger = false;
    private bool canUp = false;
    private bool canDown = false;
    private bool thisBomd = false;

    protected RoomManager roomManager;
    private GameObject rightWallCollider;
    private GameObject leftWallCollider;

    protected SeManager seManager;

	// 2016/01/27追加
	//private Billding billding;


    // デバッグ変数
    public bool yokobaku = false;
    
	void Awake()
	{
		// 家具類のゲームオブジェクトの取得
		this.rightWallCollider = this.transform.FindChild("Collider/RightWall").gameObject;
		this.leftWallCollider = this.transform.FindChild("Collider/LeftWall").gameObject;
	}

	// Use this for initialization
	void Start()
	{
        // 最初の爆発
        Instantiate(createExprojonPrefab, transform.position, Quaternion.identity);

        seManager = SeManager.GetInstance;

		// 最初の初期化処理
		this.FirstInitiation();

		// 初期化処理
		this.Initiation();
	}

	// 最初だけの初期化処理
	void FirstInitiation()
	{
		// 壁当たり判定の初期化
		this.rightWallCollider.SetActive(false);
		this.leftWallCollider.SetActive(false);

		// 家具表示の初期化
		this.furnitures.leftDoorObject.SetActive(false);
		this.furnitures.rightDoorObject.SetActive(false);
		this.furnitures.stairObject.SetActive(false);



        // ルームマネージャーセット
        roomManager = GameObject.FindWithTag("Manager").GetComponent<RoomManager>(); //RoomManager.instance;

		// 初期位置セット
		this.firstPosition = this.gameObject.transform.position;
	}

    // 初期化するときに呼ぶ関数
    public virtual void Initiation()
    {
        this.thisBomd = false;
        this.deathTrigger = false;

        this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.gameObject.transform.position = this.firstPosition;

		// 部屋の属性オブジェクトの追加処理

		// 階段
        if (this.roomDesign.stair)
        {
            this.furnitures.stairObject.SetActive(true);
        }else{
			this.furnitures.stairObject.SetActive(false);
		}

		// 右扉
        if (this.roomDesign.rightDoor && !(roomX == roomManager.roomDesignProperties.sideRooms -1))
        { 
            this.furnitures.rightDoorObject.SetActive(true);
        }else{
			this.furnitures.rightDoorObject.SetActive(false);
		}

		// 左扉
        if (this.roomDesign.leftDoor && !(roomX == 0))
        {
            this.furnitures.leftDoorObject.SetActive(true);
		}else{
			this.furnitures.leftDoorObject.SetActive(false);	
		} 

        switch (this.roomDesign.item)
        {
            case RoomScript.Items.NONE:
                this.furnitures.wideBombItem.SetActive(false);
                this.furnitures.verticalBombItem.SetActive(false);
                break;

            case RoomScript.Items.WIDE_ITEM:
                this.furnitures.wideBombItem.SetActive(true);
                this.furnitures.verticalBombItem.SetActive(false);
                break;

            case RoomScript.Items.VERTICAL_ITEM:
                this.furnitures.wideBombItem.SetActive(false);
                this.furnitures.verticalBombItem.SetActive(true);
                break;

            default:
                break;
        }

        this.SetResetTrigger();
    }

	// リセットトリガーオン、ついでに周りの部屋情報もリセット
	public virtual void SetResetTrigger()
	{
		upRoomScript = null;
		downRoomScript = null;
		leftRoomScript = null;
		rightRoomScript = null;

		this.resetTrigger = true;
	}

	// Update is called once per frame
	void Update()
	{
		// 壁チェックと初期化
		this.CheckOther();

		if (deathTrigger) this.RoomDestroy();
		if (yokobaku) this.SideBakkan();


		// ここからデバッグ
		//if (Input.GetKeyDown(KeyCode.A))
		//{
		//    Debug.Log("wai");
		//    this.Bakkan();
		//}
		if (Input.GetKeyDown(KeyCode.Q))
		{
			Debug.Log(this.GetRoomManager().tag);

		}
	}

	// 壁の当たり判定や上下移動の可否をチェックする関数
	void CheckOther()
	{
		if (this.checkTrigger)
		{
			// 当たり判定の有無処理
			if ((this.roomDesign.rightDoor && this.rightRoom) || this.rightRoomLeftDoorTrigger)
			{
				// 右壁OFF
				//Debug.Log("MigiOn");
				this.rightWallCollider.SetActive(false);

			}
			else
			{
				this.rightWallCollider.SetActive(true); // 右壁ON
			}

			// 当たり判定の有無
			if ((this.roomDesign.leftDoor && this.leftRoom) || this.leftRoomRightDoorTrigger)
			{
				// 左壁OFF
				this.leftWallCollider.SetActive(false);
			}
			else
			{
				this.leftWallCollider.SetActive(true);
			}

			// 上に行けるかどうか
			if (this.roomDesign.stair && this.upRoomTrigger)
			{
				this.canUp = true;
			}
			else
			{
				this.canUp = false;
			}

			// 下に行けるかどうか
			this.canDown = downRoomStairTrigger;
			//            if (downRoomStairTrigger)
			//            {
			//                this.canDown = true;
			//            }
			//            else
			//            {
			//                this.canDown = false;
			//            }


			this.TriggerRiset();
		}

	}

	// トリガーのリセット
	void TriggerRiset()
	{
		this.rightRoom = false;
		this.leftRoom = false;

		this.rightRoomLeftDoorTrigger = false;
		this.leftRoomRightDoorTrigger = false;

		this.upRoomTrigger = false;
		this.downRoomStairTrigger = false;

		this.checkTrigger = false;
		this.resetTrigger = false;
	}

	// 部屋を無くすときのやーつ
	public void RoomDestroy()
	{
		if (this.theBombManager != null)
		{
			this.theBombManager.BombExprosion();
			return;
		}
		//破壊されるエフェクトを呼び出す
		GameObject blast = Instantiate(this.furnitures.blast, this.furnitures.playerResporn.position, Quaternion.identity) as GameObject;
        blast.transform.parent = null;
        this.GetRoomManager().RoomDestroy(this.gameObject);
	}

    // 爆発のみを発生させる。リセット用
    public void Blast()
    {
        GameObject blast = Instantiate(this.furnitures.blast, this.furnitures.playerResporn.position, Quaternion.identity) as GameObject;
        blast.transform.parent = null;
        seManager.Play((int)E_SeType.BURST_NORMAL);
    }

	// 部屋のデストロイ
	public void Bakkan()
	{
		this.deathTrigger = true;
        seManager.Play((int)E_SeType.BURST_NORMAL);

        if (this.leftRoomScript != null)
		{
			if(this.leftRoomScript);
		}
	}

	// 横一列デストロイ
	public virtual void SideBakkan()
	{
		this.Bakkan();

		if(null != this.leftRoomScript)
		{
			if(!this.leftRoomScript.deathTrigger)
			{
				this.leftRoomScript.SideBakkan();
			}
		}

		if (null != this.rightRoomScript)
		{
			if (!this.rightRoomScript.deathTrigger)
			{
				this.rightRoomScript.SideBakkan();
			}
		}
	}

	// 縦三マスデストロイ
	public void VerticalBakkan()
	{
		this.Bakkan();

		if (null != this.upRoomScript)
		{
			this.upRoomScript.Bakkan();
		}

		if (null != this.downRoomScript)
		{
			this.downRoomScript.Bakkan();
		}

	}
    
    // 爆弾をクリエイト（引数：爆弾の種類）
    public bool PutBomb(int id)
    {
		// 爆弾が置ける状況かどうか
		if(thisBomd) return false;
            
		// 爆弾の種類の判別 / 生成
        switch(id)
        {
            case (int)bombs.NORMAL_BOMB:
                this.theGameObject = Instantiate(	this.boms.normalBomb,
													this.furnitures.ladderResporn.position,
													this.transform.rotation) as GameObject;
                break;

            case (int)bombs.VERTICALBOMB:
                this.theGameObject = Instantiate(	this.boms.verticalBomb, 
													this.furnitures.ladderResporn.position, 
													this.transform.rotation) as GameObject;
                break;

            case (int)bombs.WIDEBOMS:
                this.theGameObject = Instantiate(	this.boms.wideBomb, 
													this.furnitures.ladderResporn.position, 
													this.transform.rotation) as GameObject;
                break;

			// 異常値
            default:return false;
        }

		// 爆弾の設置
		this.theBombManager = this.theGameObject.GetComponent<BombManager>();
		this.theBombManager.SetRoom(this.gameObject);
		this.thisBomd = true;

        return true;
    }
		
    // 縦の移動チェック
    public bool VerticalMoveCheck(int id, PlayerControl player)
    {
        // idが上なら
        if(id == (int)verticals.UP)
        {
            // 上に行けるかチェック
            if(canUp)
            {
                // 上に行く処理
                //Debug.Log("haiahi");
                // プレイヤーの位置を上のはしごに合わせる
                player.SetLadder(this.furnitures.playerResporn.transform.position);
                return true;
            }
        }
        
        if(id == (int)verticals.DOWN)
        {
            // 下に行けるかチェック
            if(canDown)
            {
                //Debug.Log("ikemasse");
                // 下に行く処理
                // プレイヤーの位置を下のはしごに合わせる
                player.SetLadder(this.downRoomScript.GetLadderResporn().position);
                return true;
            }
        }


        return false;
    }    		

    public virtual void OnTriggerStay(Collider other)
    {
        if (this.resetTrigger) this.CheckOtherRooms(other);

		// プレイヤーが内部に存在し、
        if (other.gameObject.tag == "Player")
        {
			// 
            if (this.deathTrigger)
            {
                // プレイヤー殺す。
				other.gameObject.SendMessage("deathPlayerCharacter");
            }
        }
    }

	// フラグ入ってたらまわりのルームのチェックしますよ
	void CheckOtherRooms(Collider other)
	{
		// タグで判定
		switch (other.gameObject.tag)
		{
		// RoomTriggerScriptからRoomScriptのあるゲームオブジェクトを取得
		// その後、RoomScript取得
		case "Up":
			this.theGameObject = other.gameObject.GetComponent<RoomTriggerScript>().parentRoom;
			this.downRoomScript = this.theGameObject.GetComponent<RoomScript>();
			this.downRoomStairTrigger = downRoomScript.roomDesign.stair;
			break;

		case "Down":
			this.theGameObject = other.gameObject.GetComponent<RoomTriggerScript>().parentRoom;
			this.upRoomScript = this.theGameObject.GetComponent<RoomScript>();
			this.upRoomTrigger = true;
			this.furnitures.StairUpTrigger.playerResporn = this.upRoomScript.GetPlayerResporn();
			break;

		case "Left":
			this.rightRoom = true;
			this.theGameObject = other.gameObject.GetComponent<RoomTriggerScript>().parentRoom;
			this.rightRoomScript = this.theGameObject.GetComponent<RoomScript>();
			this.rightRoomLeftDoorTrigger = rightRoomScript.roomDesign.leftDoor;
			break;

		case "Right":
			this.leftRoom = true;
			this.theGameObject = other.gameObject.GetComponent<RoomTriggerScript>().parentRoom;
			this.leftRoomScript = this.theGameObject.GetComponent<RoomScript>();
			this.leftRoomRightDoorTrigger = leftRoomScript.roomDesign.rightDoor;
			break;


		default:
			break;
		}
		checkTrigger = true;
	}

	// アクセサ
	public Vector3 GetRespornPos()
	{
		return this.furnitures.playerResporn.position;
	}

	public RoomManager GetRoomManager()
	{
		if(this.roomManager == null)
		{
            // ルームマネージャーセット
            //roomManager = RoomManager.instance;
            GameObject managerObject = GameObject.FindWithTag("Manager");
            if (managerObject == null) return null ;

            roomManager = managerObject.GetComponent<RoomManager>();
		}
		return this.roomManager;
	}

    public Transform GetPlayerResporn() // プレイヤーリスポン位置のゲッター
    {
        return this.furnitures.playerResporn;
    }
    public Transform GetLadderResporn() // 上から降りてきたときの初期位置
    {
        return this.furnitures.ladderResporn;
    }

    public void setX(int roomX){this.roomX = roomX;}
	public int getX(){return this.roomX;}

	public void setY(int roomY){this.roomY = roomY;}
	public int getY(){return this.roomY;}

    //void OnTriggerExit(Collider other)
    //{
    //    switch (other.gameObject.tag)
    //    {
    //        case "Up":
    //            break;

    //        case "Down":
    //            break;

    //        case "Left":
    //            this.rightRoomLeftDoor = false;
    //            this.CheckWall();
    //            break;

    //        case "Right":
    //            this.leftRoomRightDoor = false;
    //            this.CheckWall();
    //            break;


    //        default:
    //            break;
    //    }
    //}
}
