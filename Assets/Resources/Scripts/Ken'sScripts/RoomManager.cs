﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class RoomDesignProperties //Not a MonoBehaviour!
{
    public int storeys; // 階層
    public int sideRooms; // 横の部屋数

    public int ifif3d;
}

[System.Serializable]
public class RoomSize //Not a MonoBehaviour!
{
    public float width; // 横幅
    public float height; // 縦幅
}

[System.Serializable]
public class GameDebug //Not a MonoBehaviour!
{
    public GameObject player;
    public GameObject billding;
}


public class RoomManager : MonoBehaviour {

    static float roomPlay = 1.0f;
    public bool random;
    public RoomDesignProperties roomDesignProperties;
    public RoomSize roomSize;
    public GameDebug gameDebug;

    public float resetTime = 0.3f;

    //public static RoomManager instance;

    private List<RoomScript> roomList = new List<RoomScript>();
    //private List<ItemScript> ItemList = new List<ItemScript>();
    private GameObject roomParent;
    private GameObject room;
    private GameObject goal;
    private GameObject SideRoof;

    private GameObject roomParentObject;
    private GameObject roomObject;
    private GameObject playerObject;

    private GameObject theObject;

    private Vector3 roomPosition;
    private float resetTimeCount;

	private float xOrigin;

	// Use this for initialization
	void Start () {
        Debug.Log("Room Manager Start");
		//if (instance == null)
		//{
		//	instance = this;
		//}else{
		//	Destroy(this);
		//	return;
		//}
		this.roomParent	 = (GameObject)Resources.Load("Prefabs/Ken'sPrefabs/Billding");
		this.room		 = (GameObject)Resources.Load("Prefabs/Ken'sPrefabs/Room");
		this.goal		 = (GameObject)Resources.Load("Prefabs/Ken'sPrefabs/Goal");
		this.SideRoof	 = (GameObject)Resources.Load("Prefabs/Ken'sPrefabs/SideRoof");

		this.roomPosition 	= Vector3.zero;
		this.resetTimeCount = 0.0f;

        // ビルの左端の位置を計算する
        xOrigin = (((float)this.roomDesignProperties.sideRooms * 0.5f) - 0.5f) * this.roomSize.width * -1;

        this.FirstRoomGet();

        // プレイヤーの取得
        if (this.playerObject == null)
		{
			this.playerObject = GameObject.FindWithTag("Player");
			if (this.playerObject == null)
			{
                // プレイヤの生成処理はPlayerManagerに移行しました。
                //StartCoroutine(WaitFallRoom());
			}
			//this.playerObject.GetComponent<PlayerControl>().positionZ = 0;
			//Debug.Log("are?");
			//this.playerObject = this.gameDebug.player;
		}

		
		//this.Restart();

		// ここからデバック用
		//this.FirstRoomCreate();
		//this.RandSetRoom();
		//this.RandSetItem();

		//int randNum = Random.Range((int)0, this.roomList.Count);
		//this.playerObject.transform.position = this.roomList[randNum].GetRespornPos();
	}

	void FirstRoomGet()
	{

		// ランダムモード時
		if (random || this.gameDebug.billding == null)
		{
			this.FirstRoomCreate();
			this.RandSetRoom();
            //this.RandSetItem();
			return;
		}

		// ステージモード時
		this.roomParentObject = this.gameDebug.billding;

		//子供のオブジェクトを取得
		foreach (Transform child in this.gameDebug.billding.transform)
		{
			// リストに追加する
			this.roomList.Add(child.GetComponent<RoomScript>());
		}
		//GameObject[] childObjects = this.gameObject.GetChildren(this.gameDebug.billding);
		//foreach (GameObject child in childObjects)
		//{
		//    // リストに追加する
		//    this.roomList.Add(child.GetComponent<RoomScript>());
		//}

		int roomNum = this.roomList.Count;
		for (int i = 0; i < roomNum; i++)
		{
			if (this.roomList[i].roomDesign.isStartRoom)
			{
				this.playerObject.transform.position = this.roomList[i].GetRespornPos();
			}
		}
	}

    IEnumerator WaitFallRoom()
    {
        yield return new WaitForSeconds(1.5f);
        // write func after this.

        GeneratePlayer();
    }

    void GeneratePlayer()
    {
        //GameObject player = null;

        Transform spawnPoint = GetSpawnPoint();

        this.playerObject = Instantiate(this.gameDebug.player, spawnPoint.position, spawnPoint.rotation) as GameObject;

        //return player;
    }

	// Update is called once per frame
	void Update () {

		this.AllResetRooms();

		// ここからデバッグ用
		if(Input.GetKeyDown(KeyCode.D))
		{
			Debug.Log(this.roomList.Count);
		}
		if (Input.GetKeyDown(KeyCode.S))
		{
			Debug.Log(this.roomParentObject.transform.childCount);
		}

		if (Input.GetKeyDown(KeyCode.A))
		{
            //CreateRoom();
            //this.Restart();
            Application.CaptureScreenshot(System.DateTime.Now.ToString("hh_mm_ss")+".png");
		}

		if(Input.GetKeyDown(KeyCode.F) || Input.GetKey(KeyCode.G))
		{
			int randNum = Random.Range((int)0, this.roomList.Count);
			this.roomList[randNum].Bakkan();
		}
	}


    // 呼ばれたらリストからそのゲームオブジェクトを非アクティブ化
    public void RoomDestroy(GameObject gameObject)
    {
        int num = this.roomList.IndexOf(gameObject.GetComponent<RoomScript>());
        if (-1 == num) return;

        RoomScript theScript = this.roomList[num];

        //theScript.gameObject.SetActive(false);

		int x = theScript.getX();
		int y = theScript.getY();
		foreach(RoomScript i in roomList)
		{
			int iX = i.getX();
			int iY = i.getY();

			if(iX == x)
			{
				if(iY > y)
				{
					i.setY(iY -1);
				}
			}
		}

		CreateRoom(x);
        roomList.Remove(theScript);
        Destroy(gameObject);
    }

    

    // ルームを初期位置に戻す
    void ResetBillding()
    {
        //int roomNum = this.roomList.Count;

        //for (int i = 0; i < roomNum; i++)
        //{
        //    this.roomList[i].gameObject.SetActive(true);
        //    this.roomList[i].Initiation();

        //    if(this.roomList[i].roomDesign.isStartRoom)
        //    {
        //        this.playerObject.transform.position = this.roomList[i].GetRespornPos();
        //    }
        //}

        foreach(RoomScript x in roomList)
        {
            x.gameObject.SetActive(true);
            x.Initiation();

            // ステージモード用の仕様　ステージモード自体が消えたためCO
            //if(x.roomDesign.isStartRoom)
            //{
            //    this.playerObject.transform.position = x.GetRespornPos();
            //}
        }
    }

    

    // ルームの更新
    void AllResetRooms()
    {
        if (resetTimeCount >= resetTime)
        {
            //int roomNum = this.roomList.Count;
//            for (int i = 0; i < roomNum; i++)
//            {
//            	this.roomList[i].SetResetTrigger();
//            }

			foreach(RoomScript x in this.roomList)
			{
				x.SetResetTrigger();
			}

            resetTimeCount = 0;
        }else{ 
			resetTimeCount += Time.deltaTime;
		}
    }

    // リスタート
    public void Restart()
    {
        if (random || this.gameDebug.billding == null)
        {
       //     this.RandSetRoom();
        }

		RandSetRoom();

        // 古い部屋を全て破壊
        foreach (RoomScript x in roomList)
        {
            x.Blast();
            x.Initiation();
        }

        //CreateRoom();



        //RandSetItem();
        

        //this.ResetBillding();
        //this.playerObject.GetComponent<PlayerControl>().Initialize();

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject x in players)
        {
            x.GetComponent<PlayerControl>().Initialize();

            if(random||this.gameDebug.billding ==null)x.transform.position = GetSpawnPoint().position;
        }

        //if (random || this.gameDebug.billding == null)
        //{
            //this.playerObject = GameObject.FindWithTag("Player");
            

            //Transform respawnPoint = GetSpawnPoint();

            //if (this.playerObject == null)
            //{
            //    this.playerObject = Instantiate(this.gameDebug.player, respawnPoint.position, respawnPoint.rotation) as GameObject;
            //}else { 
            //    this.playerObject.transform.position = respawnPoint.position;
            //}
			//int i = Random.Range(0, this.roomList.Count);
            //this.playerObject.transform.position = this.roomList[i].GetRespornPos();
            //this.playerObject.GetComponent<PlayerControl>().positionZ = 0;
        //}

    }

    // roomDesignPropertiesで指定されたサイズのビルを自動で生成する関数
    void CreateRoom()
    {
        if (this.roomParentObject == null)
        {
            // ルーム達の親の作成
            this.roomParentObject = Instantiate(this.roomParent, Vector3.zero, Quaternion.identity) as GameObject;
        }
        for (int i = 0; i < this.roomDesignProperties.sideRooms; i++)
        {
            for (int j = 0; j < this.roomDesignProperties.storeys; j++)
            {
                CreateRoom(i, j);
            }
        }
    }

    // 指定位置のroomを生成する
    void CreateRoom(int roomX,int roomY)
	{
		float x = xOrigin + this.roomSize.width * roomX;
		float y = (this.roomSize.height + roomPlay) * roomY;
		Vector3 roomPosition = new Vector3(x,y,0f);

		GameObject roomInstance = Instantiate(this.room,roomPosition,Quaternion.identity)as GameObject;

		RoomScript roomScript = roomInstance.GetComponent<RoomScript>();

		roomScript.setX(roomX);
		roomScript.setY(roomY);

		this.roomList.Add(roomScript);

		// ランダムに家具を配置する。

		float a = Random.Range(0.0f, 3.0f);
		float b = Random.Range(0.0f, 3.0f);
		float c = Random.Range(0.0f, 3.0f);

		if (a < 1){
			roomScript.roomDesign.rightDoor = true;
		}else{
			roomScript.roomDesign.rightDoor = false;
		}

		if (b < 1){
			roomScript.roomDesign.leftDoor = true;
		}else{
			roomScript.roomDesign.leftDoor = false;
		}

		if (c < 1){
			roomScript.roomDesign.stair = true;
		}else{
			roomScript.roomDesign.stair = false;
		}

        // 親子を結びつける
        this.roomObject.transform.parent = this.roomParentObject.transform;
    }

    // 指定された列のルームを生成する(高さは10固定)
	void CreateRoom(int roomX)
	{
		CreateRoom(roomX,10);
	}



    // ビルを作る関数（仮）
    void FirstRoomCreate()
    {
        if (this.roomParentObject == null)
        {
            // ルーム達の親の作成
            this.roomParentObject = Instantiate(this.roomParent, Vector3.zero, Quaternion.identity) as GameObject;
        }
        

        // 縦と横のループ
        for (int i = 0; i < this.roomDesignProperties.sideRooms; i++)
        {
            // x座標の設定
            this.roomPosition.x = xOrigin + ((this.roomSize.width) * i);

            for (int j = 0; j < this.roomDesignProperties.storeys; j++)
            {
                // y座標の設定
                this.roomPosition.y = (this.roomSize.height + roomPlay) * j;

                // ルームのインスタンシエート
                this.roomObject = Instantiate(this.room, this.roomPosition, Quaternion.identity) as GameObject;
                // 親子を結びつける
                this.roomObject.transform.parent = this.roomParentObject.transform;

				RoomScript roomScript = this.roomObject.GetComponent<RoomScript>();
				roomScript.setX(i);
				roomScript.setY(j);
                // リストに追加する
				this.roomList.Add(roomScript);
            }
        }

        // 屋根のセット
        {
            // y座標の設定
            // TODO 一時的に数値を定数に変更
            this.roomPosition.y = 30f;//(this.roomSize.height + roomPlay) * this.roomDesignProperties.storeys;

            // 横のループ
            for (int i = 0; i < this.roomDesignProperties.sideRooms; i++)
            {
                // x座標の設定
                this.roomPosition.x = xOrigin + ((this.roomSize.width) * i);


                // 屋根のインスタンシエート
                this.roomObject = Instantiate(this.SideRoof, this.roomPosition, Quaternion.identity) as GameObject;
                // 親子を結びつける
                this.roomObject.transform.parent = this.roomParentObject.transform;
                if (1 == i % 2)
                {
                    Vector3 change = this.roomObject.transform.localScale;
                    change.x *= -1;
                    this.roomObject.transform.localScale = change;
                }
                // リストに追加する
                this.roomList.Add(this.roomObject.GetComponent<RoomScript>());
            }
        }
        // ゴールのセット
        {
            float a = Random.Range(0.0f, 2.0f);
            if (a >= 1)  // 右端にゴールセット
            {
                // x座標の設定
                this.roomPosition.x = xOrigin + ((this.roomSize.width) * this.roomDesignProperties.sideRooms);
                // y座標の設定
                this.roomPosition.y = 0;
                // ルームのインスタンシエート
                this.roomObject = Instantiate(this.goal, this.roomPosition, Quaternion.identity) as GameObject;
                // 親子を結びつける
                this.roomObject.transform.parent = this.roomParentObject.transform;
                // リストに追加する
                this.roomList.Add(this.roomObject.GetComponent<RoomScript>());
                //左扉ON
                this.roomList[(this.roomList.Count - 1)].roomDesign.leftDoor = true;
            }
            else
            {
                // x座標の設定
                this.roomPosition.x = xOrigin - ((this.roomSize.width));
                // y座標の設定
                this.roomPosition.y = 0;
                // ルームのインスタンシエート
                this.roomObject = Instantiate(this.goal, this.roomPosition, Quaternion.identity) as GameObject;
                // 親子を結びつける
                this.roomObject.transform.parent = this.roomParentObject.transform;
                // リストに追加する
                this.roomList.Add(this.roomObject.GetComponent<RoomScript>());
                //右扉ON
                this.roomList[(this.roomList.Count - 1)].roomDesign.rightDoor = true;
            }
        }

    }

    // 家具をランダムでセット
    void RandSetRoom()
    {
        int roomNum = this.roomList.Count;

        // ゴールをランダムしないために-1しています
        for(int i = 0; i < roomNum - 1; i++)
        {
            float a = Random.Range(0.0f, 3.0f);
            float b = Random.Range(0.0f, 3.0f);
            float c = Random.Range(0.0f, 3.0f);

            if (a < 1)
            {
                this.roomList[i].roomDesign.rightDoor = true;
            }
            else
            {
                this.roomList[i].roomDesign.rightDoor = false;
            }
            if (b < 1)
            {
                this.roomList[i].roomDesign.leftDoor = true;
            }
            else
            {
                this.roomList[i].roomDesign.leftDoor = false;
            }
            if (c < 1)
            {
                this.roomList[i].roomDesign.stair = true;
            }
            else
            {
                this.roomList[i].roomDesign.stair = false;
            }
            //this.roomList[i].Initiation();
        }
        //this.roomList[(this.roomList.Count - 1)].Initiation();
    }

    // アイテムをランダムでセット
    void RandSetItem()
    {
        int roomNum = this.roomList.Count;

        // ゴールをランダムしないために-1しています
        for (int i = 0; i < roomNum - 1; i++)
        {
            float a = Random.Range(0.0f, 7.0f);

            if(a >= 6.0f)
            {
                float b = Random.Range(0.0f, 2.0f);
                if (b > 1.0f)
                {
                    // 縦アイテム
                    this.roomList[i].roomDesign.item = RoomScript.Items.VERTICAL_ITEM;
                }
                else
                {
                    // 横アイテム
                    this.roomList[i].roomDesign.item = RoomScript.Items.WIDE_ITEM;
                }

            }

        }
    }

	public Transform GetSpawnPoint()
	{
        bool didFinishSelecting = true;
        while (didFinishSelecting)
        {
            int x = (int)Random.Range(0f, roomDesignProperties.sideRooms);
            int y = (int)Random.Range(roomDesignProperties.storeys-3f, roomDesignProperties.storeys);

            Debug.Log("Spawn to... " + " x:" + x + " y:" + y);

            foreach (RoomScript i in roomList)
            {
                if (i.getX() == x && i.getY() == y)
                {
                    if (!i.furnitures.rightDoorObject.activeSelf &&
                        !i.furnitures.leftDoorObject .activeSelf &&
                        !i.furnitures.stairObject    .activeSelf)
                    {
                        Debug.Log("This point can't spawn player!");
                        break;
                    }
                    didFinishSelecting = false;
                    return i.furnitures.playerResporn;
                }
            }
        }

		return null;
	}

    // ビルを作る関数3D(未使用
    //void RoomCreate3D()
    //{
    //    if (this.roomParentObject == null)
    //    {
    //        // ルーム達の親の作成
    //        this.roomParentObject = Instantiate(this.roomParent, Vector3.zero, Quaternion.identity) as GameObject;
    //    }
    //    // ビルの左端の位置を計算する
    //    float xOrigin = (((float)this.roomDesignProperties.sideRooms * 0.5f) - 0.5f) * this.roomSize.width * -1;
    //    float zOrigin = (((float)this.roomDesignProperties.ifif3d * 0.5f) - 0.5f) * this.roomSize.width * -1;

    //    // 縦と横のループ
    //    for (int i = 0; i < this.roomDesignProperties.sideRooms; i++)
    //    {
    //        // x座標の設定
    //        this.roomPosition.x = xOrigin + ((this.roomSize.width) * i);

    //        for (int j = 0; j < this.roomDesignProperties.storeys; j++)
    //        {
    //            // y座標の設定
    //            this.roomPosition.y = (this.roomSize.height + roomPlay) * j;
    //            for (int k = 0; k < this.roomDesignProperties.ifif3d; k++)
    //            {
    //                // z座標の設定
    //                this.roomPosition.z = zOrigin + ((this.roomSize.width) * k);
    //                // ルームのインスタンシエート
    //                this.roomObject = Instantiate(this.room, this.roomPosition, Quaternion.identity) as GameObject;
    //                // 親子を結びつける
    //                this.roomObject.transform.parent = this.roomParentObject.transform;
    //                // リストに追加する
    //                this.roomList.Add(this.roomObject.GetComponent<RoomScript>());
    //            }
    //        }
    //    }
    //}
}
