﻿using UnityEngine;
using System.Collections;

public class GoalScript : RoomScript {

    // 修正
    public override void Initiation()
    {
        this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.gameObject.transform.position = this.firstPosition;

        if (this.roomDesign.rightDoor)
        {
            //右扉を表示する処理
            this.furnitures.rightDoorObject.SetActive(true);
        }

        if (this.roomDesign.leftDoor)
        {
            //左扉を表示する処理
            this.furnitures.leftDoorObject.SetActive(true);
        }
    }

    // 横爆破で巻き込まれないように修正
    public override void SideBakkan()
    {
        return;
    }

    // 修正
    public override void SetResetTrigger()
    {
        return;
    }

        // Use this for initialization
     void Start()
    {
        roomManager = GameObject.Find("Managers").GetComponent<RoomManager>();
        roomManager.GetSpawnPoint();

        // 家具表示の初期化
        this.furnitures.leftDoorObject.SetActive(false);
        this.furnitures.rightDoorObject.SetActive(false);

        // 初期位置セット
        this.firstPosition = this.gameObject.transform.position;

        this.Initiation();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerControl>().GameClear();
            //Debug.Log("CLear");
            roomManager.Restart();
        }
    }

    public override void OnTriggerStay(Collider other)
    {
        return;
    }
}
