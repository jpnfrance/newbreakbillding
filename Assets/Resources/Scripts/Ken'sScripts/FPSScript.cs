﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class FPSScript : MonoBehaviour {

    public GameObject myText;
    private Text theText;

    int frameCount;
    float prevTime;

    // Use this for initialization
    void Start () {
        this.theText = this.myText.GetComponent<Text>();

        frameCount = 0;
        prevTime = 0.0f;
    }
	
	// Update is called once per frame
	void Update () {
        ++frameCount;
        float time = Time.realtimeSinceStartup - prevTime;

        if (time >= 0.5f)
        {
            int count = (int)(frameCount / time);
            this.theText.text = "FPS : " + count.ToString();

            frameCount = 0;
            prevTime = Time.realtimeSinceStartup;
        }

        //int count = (int)(1f / Time.deltaTime);
        //this.theText.text = "FPS : " + count.ToString();
    }
}
