﻿using UnityEngine;
using System.Collections;

public class RoofScript : RoomScript
{
    // 修正
    public override void Initiation()
    {
        this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.gameObject.transform.position = this.firstPosition;
        return;
    }

    // 横爆破で巻き込まれないように修正
    public override void SideBakkan()
    {
        return;
    }

    // 修正
    public override void SetResetTrigger()
    {
        return;
    }

    void Awake()
    {

    }

    // Use this for initialization
    void Start () {
        // 初期位置セット
        this.firstPosition = this.gameObject.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
