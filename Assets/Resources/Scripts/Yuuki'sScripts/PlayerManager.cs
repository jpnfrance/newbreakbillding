﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//プレイヤーマネージャ

public class PlayerManager : MonoBehaviour {
    public enum GAME_MODE
    {
        SINGLE_MODE,
        MULTI_MODE,
    }

    public GAME_MODE mode;
    public int players;

    public GameObject playerPrefab;

    public List<int> idList;

    private List<GameObject> playerList;
    private Dictionary<int, int> pointMap;      // id , point
    private Dictionary<int, int> deadCountMap;  // id , deadCount
    private Dictionary<int, int> usedBombCountMap;
    private Dictionary<int, int> usedWideBombCountMap;
    private Dictionary<int, int> usedVerticalBombCountMap;
    private RoomManager roomManager;

    // Use this for initialization
    void Start ()
    {
        //DontDestroyOnLoad(this);

        playerList = new List<GameObject>();
        idList = new List<int>();
		pointMap = new Dictionary<int, int>();
		deadCountMap = new Dictionary<int, int>();

        usedBombCountMap = new Dictionary<int, int>();
        usedWideBombCountMap = new Dictionary<int, int>();
        usedVerticalBombCountMap = new Dictionary<int, int>();

        roomManager = GameObject.Find("Managers").GetComponent<RoomManager>();

        if(MessageToPlayScene.isExist())
        {
            //GeneratePlayer((int)MessageToPlayScene.GetInstance().GetPressPad());
			StartCoroutine("GeneratePlayerSingle",(int)MessageToPlayScene.GetInstance().GetPressPad());
			MessageToPlayScene.InitializeMessage ();
        }
        //GeneratePlayer();
        //GeneratePlayer(1);
	}

	private IEnumerator GeneratePlayerSingle(int id)
	{
		yield return null;
		GeneratePlayer (id);

	}

    //public void GeneratePlayer()
    //{
    //    switch (mode)
    //    {
    //        case GAME_MODE.SINGLE_MODE:
    //            Invoke("GeneratePlayerSingle", 1f);
    //            //GeneratePlayerSingle();
    //            break;
    //        case GAME_MODE.MULTI_MODE:
    //            Invoke("GeneratePlayerMulti", 1f);
    //            //GeneratePlayerMulti();
    //            break;
    //    }
    //}

    // プレイヤーの生成関数　1-4　の範囲でｵﾅｼｬｽ,ｾﾝｾﾝｼｬﾙ
    public GameObject GeneratePlayer(int id)
    {
		Debug.Log ("Generating Player ... ID:" + id);
        GameObject player;
        Transform spawnPoint = roomManager.GetSpawnPoint();
        player = (GameObject)Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);

        player.GetComponent<PlayerControl>().SetID(id);

        idList.Add(id);

        pointMap.Add(id, 0);
        deadCountMap.Add(id, 0);

        usedBombCountMap.Add(id, 0);
        usedWideBombCountMap.Add(id, 0);
        usedVerticalBombCountMap.Add(id, 0);

		playerList.Add(player);

        return player;
    }

    public void GeneratePlayerWithJoinPlayerList(List<int> joinPlayerList)
    {
        foreach(int x in joinPlayerList)
        {
            GeneratePlayer(x);
        }
    }

    void GeneratePlayerSingle()
    {
        GameObject player = GeneratePlayer(1);

        playerList.Add(player);
    }

    void GeneratePlayerMulti()
    {
        for(int i=1;i<players+1;i++)
        {
            Debug.Log("Generate Player " + i);
            GameObject player = GeneratePlayer(i);
            playerList.Add(player);
        }
    }

    public GameObject GetPlayerObject(int id)
    {
        foreach(GameObject x in playerList)
        {
            if(x.GetComponent<PlayerControl>().GetID() == id)
            {
                return x;
            }
        }
        return null;
    }

    // プレイヤコントロール を返す　引数　ユーザID　1-4
    public PlayerControl GetPlayerControl(int id)
    {
        foreach(GameObject x in playerList)
        {
            PlayerControl playerControl = x.GetComponent<PlayerControl>();
            if(playerControl.GetID() == id)
            {
                return playerControl;
            }
        }
        return null;
    }

    // プレイヤのポイントを返す　引数　ユーザID　1-4
    public int GetPlayerPoint(int id)
    {
        return pointMap[id];
        //return GetPlayerControl(id).point;
    }

    // プレイヤの死亡回数を返す　引数　ユーザID　1-4
    public int GetPlayerDeadCount(int id)
    {
        return deadCountMap[id];
        //return GetPlayerControl(id).deadCount;
    }

    public int GetPlayerUseBombCount(int id)
    {
        return usedBombCountMap[id];
    }

    public int GetPlayerUseWideBombCount(int id)
    {
        return usedWideBombCountMap[id];
    }

    public int GetPlayerUseVerticalBombCount(int id)
    {
        return usedVerticalBombCountMap[id];
    }

    public List<int> GetPlayerIDList()
    {
        return idList;
    }

	// Update is called once per frame
	void Update () 
    {
        foreach(GameObject x in playerList)
        {
            PlayerControl playerControl = x.GetComponent<PlayerControl>();

            if (playerControl == null) break;

            int id = playerControl.GetID();

            pointMap[id]     = playerControl.point;
            deadCountMap[id] = playerControl.deadCount;

            usedBombCountMap[id]         = playerControl.usedBombCount;
            usedWideBombCountMap[id]     = playerControl.usedWideBombCount;
            usedVerticalBombCountMap[id] = playerControl.usedVerticalBombCount;
        }
	}

    //プレイヤーオブジェクトを生成
    //public void PlayerGenerate()
    //{
    //    Instantiate(player);
    //}

    //public void PlayerGenerate(Vector3 pos)
    //{
    //    Instantiate(player, pos, Quaternion.identity);
    //}

    //public void PlayerGenerate(Vector3 pos ,Quaternion qua)
    //{
    //    Instantiate(player, pos, qua);
    //}

}
