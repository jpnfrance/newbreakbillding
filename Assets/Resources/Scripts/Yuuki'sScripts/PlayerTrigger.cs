﻿using UnityEngine;
using System.Collections;

public class PlayerTrigger : MonoBehaviour {

    public bool theDebug;

    //親のゲームオブジェクト
    public GameObject parentGameObject;

    private GameObject currentRoomObject;

    //親のコントローラー上下インプット
    private int parentVerticalInput;


	// Use this for initialization
	void Start () 
    {
        parentGameObject = this.transform.root.gameObject;

        parentVerticalInput = 0;

	}


    //セッター
    public void SetPlayerInput(int input)
    {
        parentVerticalInput = input;
    }


    //当たり判定

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Ladder")
        {
            parentGameObject.SendMessage("SetIsCanMoveLadder",true);
           
        }
      
    }
    

    void OnTriggerStay(Collider col)
    {
        //部屋にいるとき
        if (col.gameObject.tag == "Room")
        {
            if(currentRoomObject != col.gameObject)
            {
                currentRoomObject = col.gameObject;
            }

            RoomTriggerScript roomTriggerScript = currentRoomObject.GetComponent<RoomTriggerScript>();
            GameObject theGameObject = roomTriggerScript.parentRoom;
            RoomScript roomScript = theGameObject.GetComponent<RoomScript>();

            parentGameObject.SendMessage("PutBomb", roomScript.gameObject);
        }

        //梯子に触れているとき
        if(col.gameObject.tag == "Ladder")
        {
            //梯子を移動する操作がされた
            if (parentVerticalInput != 0)
            {
                PlayerControl playerControll = parentGameObject.GetComponent<PlayerControl>();
                
                if(currentRoomObject == null)
                {
                    if(theDebug)
                    {
                        playerControll.SetIsMoveLadder(true);
                    }
                }
                else 
                {

                    RoomTriggerScript roomTriggerScript = currentRoomObject.GetComponent<RoomTriggerScript>();
                    GameObject theGameObject = roomTriggerScript.parentRoom;
                    RoomScript roomScript = theGameObject.GetComponent<RoomScript>();
                    

                    playerControll.SetIsMoveLadder(roomScript.VerticalMoveCheck(parentVerticalInput, playerControll));
                }


                parentVerticalInput = 0;
            }

        }

    }


    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Ladder")
        {
            parentGameObject.SendMessage("SetIsCanMoveLadder", false);

        }
    }
}
