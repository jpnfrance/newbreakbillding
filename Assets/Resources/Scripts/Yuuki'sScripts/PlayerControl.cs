﻿/*------------------------------------------------------------------------------------

◆ 制作社：渡邉謙
◆ 制作日：不明
◆ 編集者：原昌志
◆ 編集者：山雄丞
◆ 編集日：2016/01/18
◆ 変更内容：関数名を一部適切な名前に変更

＜内容＞
・プレイヤーをコントロールする。

---------------------------------------------------------------------------------------*/
using UnityEngine;
using System.Collections;
using GamepadInput;

///-------------------------------------------------------------- 
/// <summary>
/// プレイヤーのコントロールするクラス
/// </summary>
///-------------------------------------------------------------- 
public class PlayerControl : MonoBehaviour
{
    /*** public変数 ***/ 
    public float duration;
    public float positionZ;                         // Z軸 
    public float speed = 10.0f;                     // 移動スピード
    public float rotationSpeed = 10.0f;             // 回転スピード
    public float gravity = 9.80665f;                // 重力
    public int point        =0;                     // 獲得ポイント(ゴールした回数
    public int deadCount    =0;                     // 死亡した回数。
    public Material[] materialList;

    public int usedBombCount         = 0;   // 使用した　ボムの数
    public int usedWideBombCount     = 0;   // 使用した横ボムの数
    public int usedVerticalBombCount = 0;   // 使用した縦ボムの数

    public GameObject destractEffectPrefab;

    /*** private変数 ***/ 
    private int[] hasBomb;                          // 持ってるボムの数
    private int currentSelect;                      // 現在選択されているかの番号(仮)
    private bool isCanMoveLadder;                   // 梯子を移動することのできる状態か
    private bool isMoveLadder;                      // 梯子を移動しているか
    private bool isMoveLadderLastFlame;             // 1フレーム前に梯子を移動していたか
    private bool isDeath;                           // 死んでいるか

    private Vector3 direction = Vector3.zero;       // 移動方向ベクトル
    private Animator animator;                      // アニメーター
    private CharacterController controller;         // キャラクターコントローラー
    private GameObject[] childObjects;              // 子供オブジェクト
    private GameObject bombUI;                      // ボムUIオブジェクト

	private RoomManager roomManager;				// ルームマネージャー(リスポン位置取得用
	private DeathAction deathAction;

    private int id;                                  // id
    private GamePad.Index usePad = GamePad.Index.None;//

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 動ける状態かセットする
    /// </summary>
    /// <param name="flg"></param>
    ///--------------------------------------------------------------
    public void SetIsCanMoveLadder(bool flg)
    {
        isCanMoveLadder = flg;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// 移動の状態をセットする
    /// </summary>
    /// <param name="flg"></param>
    ///--------------------------------------------------------------
    public void SetIsMoveLadder(bool flg)
    {
        isMoveLadder = flg;

        //梯子移動を始めたら移動可能状態は解除しておく
        if (isMoveLadder)
        {
            SetIsCanMoveLadder(false);
        }
        else
        {
            //強制移動ベクトルリセット
            this.direction = Vector3.zero;
        }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// プレイヤーを殺す
    /// </summary>
    ///--------------------------------------------------------------
    public void Death()
    {
        isDeath = true;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// プレイヤーが死んでいる状態のゲッター
    /// </summary>
    /// <returns></returns>
    ///--------------------------------------------------------------
    public bool GetIsDeath()
    {
        return isDeath;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// 動ける状態かのゲッター
    /// </summary>
    /// <returns></returns>
    ///--------------------------------------------------------------
    public bool GetIsMoveLadder()
    {
        return isMoveLadder;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// ボムのIDのゲッター
    /// </summary>
    /// <returns></returns>
    ///--------------------------------------------------------------
    public int  GetCurrentSelectBombID()
    {
        return currentSelect;
    }
    
    ///-------------------------------------------------------------- 
	/// <summary>
	/// スタート処理
	/// </summary>
    ///--------------------------------------------------------------
	void Start ()
    {
	    //アニメーターをリバインド
        this.animator = GetComponent<Animator>();
        this.animator.Rebind();

        //コントローラーのコンポーネント取得
        controller = GetComponent<CharacterController>();

        //子供のオブジェクトを取得
        childObjects = this.gameObject.GetChildren(this);

        //ボム関係の初期化
        bombUI = GameObject.FindGameObjectWithTag("BombUI");
        if (bombUI != null) bombUI.SetActive(false);

		currentSelect = 0;
		hasBomb = new int[3];

		roomManager = GameObject.Find("Managers").GetComponent<RoomManager>();

		deathAction = this.gameObject.GetComponent<DeathAction>();

        // 初期化処理
        Initialize();
	}

	///-------------------------------------------------------------- 
	/// <summary>
	/// 初期化処理
	/// </summary>
	///-------------------------------------------------------------- 
	public void Initialize()
	{
		// 所持ボム数の初期化
		hasBomb[0] = int.MaxValue;
		hasBomb[1] = 1;
		hasBomb[2] = 1;

		//梯子関係フラグの初期化
		isMoveLadder = false;
		isCanMoveLadder = false;
		isMoveLadderLastFlame = false;
		isDeath = false;

		// レンダラの有効化
		foreach(SkinnedMeshRenderer x in transform.GetComponents<SkinnedMeshRenderer>())
		{
			x.enabled = true;
		}

		// 自身の当たり判定を有効化する
		foreach(BoxCollider x in transform.GetComponentsInChildren<BoxCollider>())
		{
			x.isTrigger = true;
		}
	}

	///-------------------------------------------------------------- 
	/// <summary>
	/// 重力計算
	/// </summary>
	///-------------------------------------------------------------- 
	void FixedUpdate()
	{
		if(!isMoveLadder || !CheckIsGrounded()) this.direction.y -= gravity * Time.deltaTime;
	}

    ///--------------------------------------------------------------
	/// <summary>
	/// 更新処理
	/// </summary>
    ///--------------------------------------------------------------
	void Update () 
    {
        // 死んでいる状態ならこの後の処理を行わない
        if (isDeath) return;

        //ボムの選択
        SelectBomb();

        //進行方向にキャラを向ける
        AttachRotation();

        //カメラに沿ったコントロール
        CameraAxisControl();

        //移動
        AttachMove();

        if (GamePad.GetButtonDown(GamePad.Button.Y, usePad))
        {
            Destruct();
        }

    }

	///-------------------------------------------------------------- 
	/// <summary>
	/// 爆弾を選択
	/// </summary>
	///-------------------------------------------------------------- 
	void SelectBomb()
	{
		if (bombUI != null)
		{
			//LeftButton
			if (Input.GetButtonDown("ChangeBombToLeft"))
			{
				this.currentSelect = (currentSelect + (3 - 1)) % 3;

				Debug.Log(bombUI);

				//UIを有効化
				bombUI.SetActive(true);

				//選択中のボムを更新
				bombUI.GetComponent<BombUI>().SetCurrentBomb(currentSelect);
			}

			//Right Button
			if (Input.GetButtonDown("ChangeBombToRight"))
			{
				this.currentSelect = (currentSelect + 1) % 3;

				Debug.Log(bombUI);

				bombUI.SetActive(true);
				bombUI.GetComponent<BombUI>().SetCurrentBomb(currentSelect);
			}

			//もてるボムの個数の最新版をUIに教える
			//bombUI.GetComponent<BombUI>().SetHasBomb(hasBomb);
		}
	}

	///-------------------------------------------------------------- 
	/// <summary>
	/// 進行方向へキャラを向ける
	/// </summary>
	///-------------------------------------------------------------- 
	void AttachRotation()
	{
		Vector3 moveDirectionYZero = direction;
		moveDirectionYZero.y = 0;

		//ベクトルの二乗の長さが0.001以上なら方向を変える
		if(moveDirectionYZero.sqrMagnitude > 0.001)
		{
			//2点の角度をなだらかに繋げながら回転
			float step = rotationSpeed * Time.deltaTime;
			Vector3 newDir = Vector3.RotateTowards(this.transform.forward,moveDirectionYZero,step,0);
			this.transform.rotation = Quaternion.LookRotation(newDir);
		}
	}
    
    ///-------------------------------------------------------------- 
    /// <summary>
    /// カメラに沿ったコントロール
    /// </summary>
    ///-------------------------------------------------------------- 
    void CameraAxisControl()
    {
        //地面についていたら移動
        if (CheckIsGrounded() || isMoveLadder)
        {
            //通常移動
            Vector3 right = Camera.main.transform.TransformDirection(Vector3.right);
            Vector3 up    = Camera.main.transform.TransformDirection(Vector3.up);

            //float vertical   = Input.GetAxisRaw("Vertical");
            //float horizontal = Input.GetAxisRaw("Horizontal");

            //usePad = GamePad.Index.Four;

            Vector2 lStickAxis = GamePad.GetAxis(GamePad.Axis.LeftStick, usePad);

            // GamePadからの入力が不確かだった場合はCOを解除すること。
            //Debug.Log("lStickAxis..." + "x:" +lStickAxis.x+  "y:" + lStickAxis.y + "UsePad:" + usePad);

            //梯子を移動していないとき
            if (!isMoveLadder)
            {
                //梯子を移動することができてジョイスティックが上下方向に半分以上倒されているなら
                //if (isCanMoveLadder && Mathf.Abs(vertical) >= 0.5f && !isMoveLadderLastFlame)
                if(isCanMoveLadder && Mathf.Abs(lStickAxis.y) >= 0.5f && !isMoveLadderLastFlame)
                {
                    //上か下のどちらに行くか
                    //childObjects[0].GetComponent<PlayerTrigger>().SetPlayerInput((int)(vertical / Mathf.Abs(vertical)));
                    childObjects[0].GetComponent<PlayerTrigger>().SetPlayerInput((int)(lStickAxis.y/Mathf.Abs(lStickAxis.y)));
                }
                else
                {
                    //左右移動
                    //this.direction = horizontal * right;
                    this.direction = lStickAxis.x * right;

                    //梯子から離れた後、上下入力が半分以下になったら再び梯子に上れるようにする
                    //if (isMoveLadderLastFlame && Mathf.Abs(vertical) <= 0.2f)
                    if(isMoveLadderLastFlame && Mathf.Abs(lStickAxis.y) <= 0.2f)
                    {
                        isMoveLadderLastFlame = false;
                    }
                }
            }

            //梯子を移動しているとき (ただし梯子を離れてから一度ジョイパッドの上下がリセットされていること)
            else
            {
                //キャンセルボタン入力
                //if (!Input.GetButtonDown("Cancel"))
                if(!GamePad.GetButtonDown(GamePad.Button.A,usePad))
                {
                    //上下移動
                    //this.direction = vertical * up;

                    //this.direction.x = 0;
                    this.direction = lStickAxis.y * up;
                }
                else
                {
                    isMoveLadder = false;
                    this.direction.x = 0;
                    this.direction.z = 0;
                }
            }
            
            //速度を乗算
            this.direction *= speed;

            this.direction.x = Mathf.Clamp(this.direction.x, -10, 10);
            this.direction.y = Mathf.Clamp(this.direction.y, -10, 10);
            
            if(this.direction.x > 10 || this.direction.y > 10 || this.direction.x < -10 || this.direction.y < -10)
            {
                //Debug.Log("Vertical   : " + vertical);
                //Debug.Log("Horizontal : " + horizontal);
                //Debug.Log("Direction  : " + direction);
            }
        }
    }


    ///-------------------------------------------------------------- 
    /// <summary>
    /// 移動処理
    /// </summary>
    ///-------------------------------------------------------------- 
    void AttachMove()
    {
        Vector3 snapGround = Vector3.zero;

        if (CheckIsGrounded() && !isMoveLadder)
            snapGround = Vector3.down;

		//重力計算
        //FixedUpdate();
        
        //キャラクターコントローラーによる移動
        this.controller.Move(this.direction * Time.deltaTime+ snapGround);
        
        //ただしZ座標は固定
        this.controller.transform.localPosition = new Vector3(this.controller.transform.localPosition.x,this.controller.transform.localPosition.y,positionZ);

    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// Raycastを使ったCharacterControllerのisGroundedの精度改善
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    bool CheckIsGrounded()
    {
        //もともとtrueならそのまま
        if(controller.isGrounded) return true;

        //レイを作成
        Ray ray = new Ray(this.transform.position + Vector3.up * 0.1f, Vector3.down);

        //のばすレイの距離
        float tolerance = 0.5f;
        return Physics.Raycast(ray, tolerance);
    }

    

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 爆弾を設置
    /// </summary>
    /// <param name="room"></param>
    ///-------------------------------------------------------------- 
    public  void PutBomb(GameObject room)
    {
        //コントローラーAボタン
        //if (Input.GetButtonDown("PutBomb"))
        //{
        //    GameObject currentRoom = room;

        //    if (hasBomb[currentSelect] > 0)
        //    {
        //        //ボムがおけたら
        //        currentRoom.GetComponent<RoomScript>().PutBomb(this.currentSelect);
        //        //ノーマルボム以外のボムが置かれたらおける個数を一つ減らす
        //        if(currentSelect != 0 ) hasBomb[currentSelect]--;
        //    }
        //    Debug.Log("Put Bomb");
        //}

        // TODO ボムの管理を実数型で扱っているので、暇があったら列挙型に切り替えること
        // TODO ボムの配置処理を複数回に渡って記述しているため、関数を作って纏めること


        if (GamePad.GetButtonDown(GamePad.Button.A, usePad))
        {
            room.GetComponent<RoomScript>().PutBomb((int)RoomScript.bombs.NORMAL_BOMB);
            usedBombCount++;
        }

        if (GamePad.GetButtonDown(GamePad.Button.B, usePad))
        {
            //if (hasBomb[(int)RoomScript.bombs.VERTICALBOMB] > 0)
            {
                room.GetComponent<RoomScript>().PutBomb((int)RoomScript.bombs.VERTICALBOMB);
                usedVerticalBombCount++;
                //hasBomb[(int)RoomScript.bombs.VERTICALBOMB]--;
            }
            //room.GetComponent<RoomScript>().PutBomb((int)RoomScript.bombs.VERTICALBOMB);
        }

        if (GamePad.GetButtonDown(GamePad.Button.X, usePad))
        {
            //if (hasBomb[(int)RoomScript.bombs.WIDEBOMS] > 0)
            {
                room.GetComponent<RoomScript>().PutBomb((int)RoomScript.bombs.WIDEBOMS);
                usedWideBombCount++;
                //hasBomb[(int)RoomScript.bombs.WIDEBOMS]--;
            }
            //room.GetComponent<RoomScript>().PutBomb((int)RoomScript.bombs.WIDEBOMS);
        }
    }
 
    ///-------------------------------------------------------------- 
    /// <summary>
    /// 爆弾を追加
    /// </summary>
    /// <param name="id"></param>
    /// <param name="num"></param>
    ///-------------------------------------------------------------- 
    public void AddBomb(int id ,int num)
    {
        //bombUI.GetComponent<BombUI>().AddBomb(id,num);
        hasBomb[id] += num;
    }
 
    ///-------------------------------------------------------------- 
    /// <summary>
    /// 梯子を上る
    /// </summary>
    /// <param name="pos"></param>
    ///-------------------------------------------------------------- 
    public void SetLadder(Vector3 pos)
    {
        this.transform.position = pos;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 梯子から離れる
    /// </summary>
    /// <param name="pos"></param>
    ///-------------------------------------------------------------- 
    public void LeaveLadder(Vector3 pos)
    {
        //梯子に上っていなかったらスルー
        if (!isMoveLadder) return;

        this.transform.position = pos;
        this.direction = Vector3.zero;
        isMoveLadder = false;
        isMoveLadderLastFlame = true;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// プレイヤーキャラクターの死亡処理
    /// </summary>
    ///-------------------------------------------------------------- 
    public void deathPlayerCharacter()
    {
		// 自身の子となるオブジェクトの数を取得
		int length = childObjects.Length;

		// 自身の当たり判定を無効化する
		foreach(BoxCollider x in transform.GetComponentsInChildren<BoxCollider>())
		{
			x.isTrigger = false;
		}

		// フラグを自身が死んだ状態にする
		isDeath = true;

		StartCoroutine("waitResporn");

        deadCount++;

		// 死亡時アニメーションの再生
		deathAction.StartCoroutine(deathAction.FlyAction());
    }
		
	private IEnumerator FollToCamera()
	{
		
		Vector3 cameraPosition = new Vector3();
		while(true)
		{
			Vector3 follForce = Camera.main.transform.position;
			follForce = transform.position -cameraPosition;

			follForce.Normalize();

			yield return null;
		}

	}

	private IEnumerator waitResporn()
	{
		yield return new WaitForSeconds(2f);
		ReSpawnPlayerCharactor();
	}

	public void WhenFinishPlayerCharacterDeadAnimation()
	{
		
	}
		
	///-------------------------------------------------------------- 
	/// <summary>
	/// プレイヤーキャラクターの復活関数
	/// </summary>
	///-------------------------------------------------------------- 
	public void ReSpawnPlayerCharactor()
	{
		Initialize();

		Transform respawnPoint = roomManager.GetSpawnPoint();

		this.transform.position = respawnPoint.position;
		this.transform.rotation = respawnPoint.rotation;
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// ゲームクリア関数
    /// </summary>
    ///-------------------------------------------------------------- 
    public void GameClear()
    {
        point++;
    }

    // 自爆
    private void Destruct()
    {
        if(destractEffectPrefab != null)Instantiate(destractEffectPrefab,transform.position,transform.rotation);

        Transform respawnPoint = roomManager.GetSpawnPoint();
        this.transform.position = respawnPoint.position;
        this.transform.rotation = respawnPoint.rotation;

        deadCount++;
    }

    public int GetID() { return this.id; }
    public void SetID(int id)
    {
        this.id = id;
        
        switch(id)
        {
            case 1:
                usePad = GamePad.Index.One;
                break;
            case 2:
                usePad = GamePad.Index.Two;
                break;
            case 3:
                usePad = GamePad.Index.Three;
                break;
            case 4:
                usePad = GamePad.Index.Four;
                break;
            default:
                Debug.LogError("Caution! This UserID is not Supported!");
                break;
        }

        // TODO プレイヤの色の変更処理
        GetComponentInChildren<Renderer>().material = this.materialList[id-1];
        Debug.Log("Change to " + this.materialList[id-1]);

    }

    public void SetColor(E_PlayerColor color)
    {
        switch(color)
        {
            case E_PlayerColor.RED:
                
                break;
            case E_PlayerColor.GREEN:
                break;
            case E_PlayerColor.BLUE:
                break;
            case E_PlayerColor.YELOW:
                break;
            default:
                Debug.LogError("Caution! This Color is not Supported!");
                break;
        }
    }

    public int GetHasBombs(RoomScript.bombs bomb)
    {
        return hasBomb[(int)bomb];
    }
}
