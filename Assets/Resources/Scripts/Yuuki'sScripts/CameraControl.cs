﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {


    public float cameraDampingTime;

    //プレイヤーのTransform
    private Transform playerTransform;

    //カメラ
    private Camera mainCamera;


	// Use this for initialization
	void Start () 
    {
        //メインカメラをセット
        mainCamera = Camera.main;
      
	}
	

	// Update is called once per frame
	void Update () 
    {
        if (playerTransform != null)
        {
            //プレイヤーへの方向ベクトルを取得
            Vector3 LookAtPlayer = playerTransform.localPosition - this.mainCamera.transform.localPosition;

            //単位ベクトルに変換
            LookAtPlayer.Normalize();



            //カメラをプレイヤーに向けるためのクォータニオンを生成
            Quaternion quate = Quaternion.LookRotation(LookAtPlayer);


            //カメラ回転
            mainCamera.transform.localRotation = Quaternion.Slerp(mainCamera.transform.localRotation,quate,cameraDampingTime*Time.deltaTime);

        }
        else
        {
           if(GameObject.FindGameObjectWithTag("Player") != null)
           {
               playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
           }
        }
	    
	}
}
