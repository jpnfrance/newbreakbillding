﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Score : MonoBehaviour {


    public Sprite[] sprites;
    private GameObject[] valuesSprite;
    
    private const int Value = 8;
    private const int Limit = 100000000;

    private int score;

	// Use this for initialization
	void Start ()
    {
        valuesSprite = gameObject.GetChildren(this);

        score = 0;

	}
	
	// Update is called once per frame
	void Update () 
    {
        Calculation();
	}

    void Calculation()
    {
        int scorer = this.score;
        int values = (int)Mathf.Log10(scorer)+1;
        int count = 0;
    
        values = Mathf.Clamp(values, 1, 8);

        int temp = (int)Mathf.Pow(10, values - 1);

        foreach(GameObject item in valuesSprite)
        {
            Image overRide = item.GetComponent<Image>();
            overRide.sprite = new Sprite();

            if(count >= Value-values)
            {
                overRide.overrideSprite = sprites[scorer / temp];
                scorer %= temp;
                temp/= 10;
            }
            else
            {
                overRide.overrideSprite = sprites[0];
            }
            count++;
        }
        

    }

    public void AddScore(int addScore)
    {
        this.score += addScore;

        if (this.score >= Limit)
        {
            this.score = Limit - 1;
        }
    }

    public void ResetScore()
    {
        this.score = 0;
    }
}
