﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BombUIonScreen : MonoBehaviour {

    enum SELECT__STATE
    {
        CURRENT = 0,
        CENTER,
        LEFT
    }


    //テクスチャリスト
    public Sprite[] uiTextureList;
    
    
    
    private GameObject playerPrefab;
    private GameObject[] childList;
    private int selected;

    


	// Use this for initialization
	void Start () 
    {
        childList = gameObject.GetChildren(this);
        selected = 0;
    }
	
	// Update is called once per frame
	void Update () 
    {
        if(playerPrefab != null)
            UpdateState();    
        else
        {
            playerPrefab = GameObject.FindGameObjectWithTag("Player");

        }
	}

    void UpdateState()
    {
        PlayerControl player = playerPrefab.GetComponent<PlayerControl>();

        selected = player.GetCurrentSelectBombID();

        int count = 0;
        int[] state = new int[childList.Length];
      
        state[0] = selected;
        state[1] = (selected + 1) % 3;
        state[2] = (selected + 2) % 3;

       foreach(GameObject item in childList)
       {
           Image texture = item.GetComponent<Image>();

           texture.sprite = new Sprite();
           texture.overrideSprite = uiTextureList[state[count]];

           count++;
       }


    }
}
