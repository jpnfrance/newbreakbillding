﻿using UnityEngine;
using System.Collections;

public class OverTrigger : MonoBehaviour {

    //親のゲームオブジェクト
    private GameObject parentObject;

	// Use this for initialization
	void Start ()
    {
        //親(プレイヤー)のゲームオブジェクトを取得
	    this.parentObject = this.transform.root.gameObject;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OntriggerEnter(Collider col)
    {
        //梯子を上っている時以外で何かヒットしたらゲームオーバー
        if(!parentObject.GetComponent<PlayerControl>().GetIsMoveLadder())
        {
            parentObject.SendMessage("GameOver");
        }
    }

}
