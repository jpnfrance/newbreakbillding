﻿using UnityEngine;
using System.Collections;
using System.Linq;

public static class GameObjectExtension {

    public static GameObject[] GetChildren(this GameObject self, bool includeInactive = false)
    {
        return self
            .GetComponentsInChildren<Transform>(includeInactive)
            .Where(c => c != self.transform)
            .Select(c => c.gameObject)
            .ToArray();
    }

    public static GameObject[] GetChildren(this Component self, bool includeInactive = false)
    {
        return self
            .GetComponentsInChildren<Transform>(includeInactive)
            .Where(c => c != self.transform)
            .Select(c => c.gameObject)
            .ToArray();
    }
}
