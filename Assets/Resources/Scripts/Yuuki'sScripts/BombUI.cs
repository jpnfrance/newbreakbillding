﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;

public class BombUI : MonoBehaviour {


    //オフセット
    public Vector3 offSet;

    //ボムの所持数
    private int[] hasBomb;

    //プレイヤーのオブジェクト
    private GameObject playerObject;

    //子オブジェクトのリスト
    private GameObject[] childList;


    //自身のRectTransform
    private RectTransform myRectTrans;

    //親のRectTransform
    private RectTransform parentRectTrans;
  
    //時間計測用
    private float timeLeft;


	// Use this for initialization
	void Start ()
    {
        //子オブジェクトをすべて取得
        childList = gameObject.GetChildren(this);

        myRectTrans = GetComponent<RectTransform>();
        parentRectTrans = (RectTransform)myRectTrans.parent;

       
       
        //UIが消滅するまでの時間
        timeLeft = 1.5f;


        hasBomb = new int[3];


       
	}


    // Update is called once per frame
    void Update()
    {
        if (playerObject != null && myRectTrans != null && parentRectTrans != null)
        {
            //ポジションを更新
            UpdateLocalPosFromPlayerPos();
        }
        else if(playerObject == null)
        {
            FindPlayerObject();
        }

        //アクティブ状態なら
        if (this.gameObject.activeSelf)
        {
            timeLeft -= Time.deltaTime;
            
            //時間がきたら
            if (timeLeft <= 0.0f)
            {
                
                timeLeft = 1.5f;
                this.gameObject.SetActive(false);
            }
        }
       
    }


    //ポジション更新
    void UpdateLocalPosFromPlayerPos()
    {
        //プレイヤーのスクリーン上のポジション
        var viewPortPos = Camera.main.WorldToViewportPoint((playerObject.transform.position ));

        //ビューポートの座標をワールド座標に
        var localPos = Camera.main.ViewportToWorldPoint(viewPortPos + offSet);

      
      
        myRectTrans.localPosition = localPos;
    }

    //プレイヤーオブジェクトを探す
    void FindPlayerObject()
    {
        playerObject = GameObject.FindGameObjectWithTag("Player");
    }

    //現在選択しているボムを更新
    public void SetCurrentBomb(int bombID)
    {
        int count = 0;

      
        //子オブジェクト分のループ
        foreach(GameObject item in childList)
        {

          
            if (count <= 2)
            {
                Image rawImage = item.GetComponent<Image>();

                if (count == bombID)
                {
                    rawImage.color = new Color(rawImage.color.r, rawImage.color.g, rawImage.color.b, 1.0f);
                }
                else
                {
                    rawImage.color = new Color(rawImage.color.r, rawImage.color.g, rawImage.color.b, 0.5f);
                }
            }
            else 
            {
                Text text = item.GetComponent<Text>();
                text.text = hasBomb[count - 3].ToString();

            }
            count++;
        }

        //最後の更新から1.5秒後にUIを消す
        timeLeft = 1.5f;

    }


    //ボムの個数をセット
    public void SetHasBomb(int[] has)
    {
        hasBomb = has;
    }

    //ボムの個数を増やす
    public void AddBomb(int id ,int num)
    {
        if (id < 0 || id >= hasBomb.Length) return;

        hasBomb[id] += num;
    }


   

   
}
