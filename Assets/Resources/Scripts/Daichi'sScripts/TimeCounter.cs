﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour {
    [SerializeField]
    Image[] images = new Image[4];

    [SerializeField]
    Sprite[] numberSprites = new Sprite[10];

    public int setTime;

    public float timeCount
    {
        get;
        private set;
    }

	// Use this for initialization
	void Start () 
    {
        SetTime(setTime);
	}
	
    public void SetTime(float time)
    {
        timeCount = time;
        StartCoroutine(TimerStart());
    }

    void SetNumbers(int sec,int val1,int val2)
    {
        string str = String.Format("{0:00}", sec);
        images[val1].sprite = numberSprites[Convert.ToInt32(str.Substring(0, 1))];
        images[val2].sprite = numberSprites[Convert.ToInt32(str.Substring(1, 1))];
    }

    IEnumerator TimerStart()
    {
        while(timeCount >= 0)
        {
            int sec = Mathf.FloorToInt(timeCount % 60);
            SetNumbers(sec, 2, 3);
            int min = Mathf.FloorToInt((timeCount - sec) / 60);
            SetNumbers(min, 0, 1);
            yield return new WaitForSeconds(1.0f);
            timeCount -= 1.0f;
        }
        TimeOver();
    }

    void TimeOver()
    {
        TextMesh text = new GameObject().AddComponent<TextMesh>();
        text.text = "Time Over";
    }
}
