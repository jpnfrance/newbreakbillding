﻿using UnityEngine;
using System.Collections;

public class BombManager : MonoBehaviour {


    ////　変数宣言
    //　タイマーの初期値
    public float startingTime = 3.0f;
    //　タイマーの現在の値
    public float countDownTimer;
    //　現在の爆弾のタイプ
    public int bombType;
    //　爆弾の位置
    public Vector3 position;

    public bool ifDebug = false;

    private GameObject theRoom;

    //　部屋の消滅用に
    private GameObject room;



    public void SetRoom(GameObject gameObject)
    {
        theRoom = gameObject;
    }

    public GameObject GetRoom()
    {
        return this.theRoom;
    }

	// Use this for initialization
	void Start ()
    {
        ////　poitionのそれぞれの座標
        //position.x = 0;
        //position.y = 6;
        //position.z = 10;
        //　positionの反映
        //this.gameObject.transform.position = position;
        //タイマーに初期値を代入
        countDownTimer = startingTime;
        //bombType = this.GetComponent<BombManager>().bombType;
	}
	
	// Update is called once per frame
    void Update()
    {

        //　時間の減少
        countDownTimer -= Time.deltaTime;

        

        //　時間切れになったら
        if (countDownTimer <= 0.0f)
        {
            if (ifDebug) this.DebugBombExprosion();
            else this.BombExprosion();

        }
    }

    public void BombExprosion()
    {
        if (this.theRoom != null)
        {
            RoomScript theRoomScript = this.theRoom.GetComponent<RoomScript>();

            switch(bombType)
            {
                case (int)RoomScript.bombs.NORMAL_BOMB:
                    theRoomScript.Bakkan();
                    break;

                case (int)RoomScript.bombs.WIDEBOMS:
                    theRoomScript.SideBakkan();
                    break;

                case (int)RoomScript.bombs.VERTICALBOMB:
                    theRoomScript.VerticalBakkan();
                    break;
            }

        }
        //　オブジェクトを消滅させる
        Destroy(this.gameObject);


    }

    // debug用
    public void DebugBombExprosion()
    {

        //　オブジェクトを消滅させる
        Destroy(this.gameObject);


    }
}
