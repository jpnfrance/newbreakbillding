﻿using UnityEngine;
using System.Collections;


/*********************************************
*
* エントリー画面に出ているモデル君スクリプト
*
**********************************************/

public class EntryCharacterMovement : MonoBehaviour {

	// メンバ変数
	private bool m_isVisible = false;		// 画面上に見えているかどうか
	private const float SCALE_MAX = 432f;	// サイズの上限
	private float m_scale = 1f;

	// プロパティ
	public bool Visible{
		get{ return m_isVisible; }
		set{ m_isVisible = value; }
	}

	
	/*********************************************
	 * 更新処理
	 *********************************************/
	void Update () {
		if ( m_isVisible ){
			// 回す
			this.transform.Rotate(0f, 0f, -1f);
			// 拡大
			m_scale = Mathf.Clamp(m_scale + Time.deltaTime * SCALE_MAX * 2f, 0f, SCALE_MAX);
			this.transform.localScale = new Vector3( m_scale, m_scale, m_scale );
		}
	}
}
