﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/***********************************
 * 
 * 	オブジェクトを点滅させる際の補助装置
 * 
 * *********************************/


public class Blinker {

	// メンバ変数
	private MaskableGraphic	m_component = null;	// 対象のコンポーネント
	private float 			m_alpha = 1f;		// α値
	private float 			m_angle;			// 角度


	/*********************************************
	 * 点滅コンポーネントの指定処理
	 * 
	 * 引数 : Color属性を持っているコンポーネント
	 *********************************************/
	public void SetBlinkComponent(MaskableGraphic _component){
		m_component = _component;
	}


	/*********************************************
	 * 点滅処理
	 *********************************************/
	public IEnumerator Blink(){
		m_angle = 90f;

		while( true ){
			m_alpha = 0.5f * Mathf.Sin( m_angle * Mathf.Deg2Rad ) + 0.5f;
			m_component.color = 
				new Color( m_component.color.r, m_component.color.g, m_component.color.b, m_alpha );

			yield return null;

			m_angle += Time.deltaTime * 180f;
		}
	}

}
