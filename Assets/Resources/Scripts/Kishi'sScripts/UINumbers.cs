﻿using UnityEngine;
using System.Collections;

public class UINumbers : MonoBehaviour {

	// 数字テクスチャ列挙
	public enum E_NumberTexture{
		ZERO = 0,
		ONE,
		TWO,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		CROSS,
		COLON,
	}


	// メンバ変数
	[SerializeField] private Sprite[] m_numbers;		// 数字のＵＩ


	// インデクサ
	public Sprite this[int _index]{
		get{ 
			return m_numbers[_index]; 
		}
	}
}
