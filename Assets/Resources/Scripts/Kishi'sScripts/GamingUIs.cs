﻿using UnityEngine;
using System.Collections;

public class GamingUIs : MonoBehaviour {

	// メンバ変数
	[SerializeField] private GameObject[]	m_players = null;	// プレイヤー
	private const float 			SCREEN_WIDTH = 1600f;	// スクリーン幅
	private const float 			SCREEN_HEIGHT = 900f;	// スクリーン高さ
	private const int				PLAYER_MAX = 4;			// 最大参加人数
	private const float				SPRITE_OFFSET = 36f;	// オフセット
	private Timer					m_timer;				// タイマー
	private GoalDirection			m_goalDirection;		// ゴール方向
	private CapturedCountUI			m_capturedUI;			// 攻略回数UI


	public float SpriteOffset{
		get{ return SPRITE_OFFSET; }
	}
	public float LimitTime{
		set{ m_timer.LimitTime = value; }
	}
	public GameObject this[int _index]{		// 注意！インデックスはプレイヤー番号(1~4の値)
		get{ return m_players[_index - 1]; }
	}
	public float SCREEN_W{
		get{ return SCREEN_WIDTH; }
	}
	public float SCREEN_H{
		get{ return SCREEN_HEIGHT; }
	}


	/*********************************************
	 * UI表示切り換え処理
	 *********************************************/
	public void ShowUI(bool isShow){
		m_timer.gameObject.SetActive(isShow);
		m_goalDirection.gameObject.SetActive(isShow);
	}


	/*********************************************
	 * タイマー起動処理
	 *********************************************/
	public void StartTimer(){
		m_timer.StartTimer();
	}


	/*********************************************
	 * タイマー停止処理
	 *********************************************/
	public void StopTimer(){
		m_timer.StopTimer();
	}


	/*********************************************
	 * タイマーリセット処理
	 *********************************************/
	public void ResetTimer(){
		m_timer.ResetTimer();
	}


	/*********************************************
	 * プレイヤー情報をマネージャに渡す処理
	 *********************************************/
	IEnumerator SetPlayerData(){
		yield return null;

		// プレイヤーマネージャに参加メンバ情報を与える
		PlayerManager manager = GameObject.Find("Managers").GetComponent<PlayerManager>();
		manager.GeneratePlayerWithJoinPlayerList(EntryPlayerData.Instance.EntryPlayers);

		// プレイヤーを取得する
		StartCoroutine(GetPlayerObjects(manager));
	}


	/*********************************************
	 * キャンパスにカメラを設定する処理
	 *********************************************/
	IEnumerator SetCamera(){
		// キャンパスの取得
		GameObject canvas = this.transform.parent.gameObject;

		while( true ){
			GameObject camera = GameObject.FindWithTag("MainCamera");

			if ( camera != null ) {
				canvas.GetComponent<Canvas>().worldCamera = camera.GetComponent<Camera>();
			}

			yield return null;
		}
	}


	/*********************************************
	 * 生成時処理
	 *********************************************/
	IEnumerator GetPlayerObjects(PlayerManager _manager){
		bool isClear = false;

		while( !isClear ){
			// プレイヤー情報を自分でも保持する
			for( int i = 0; i < PLAYER_MAX; ++i ){
				m_players[i] = _manager.GetPlayerObject(i + 1);

				// １つでもきちんとデータが取得出来たら抜けられる
				if ( m_players[i] != null ) isClear = true;
			}

			yield return null;

			Debug.Log("Player取得待ちです");
		}

		// 攻略回数スプライトをアクティブにする
		m_capturedUI.SetActiveSprite();
	}


	/*********************************************
	 * 生成時処理
	 *********************************************/
	void Start(){
		StartCoroutine("SetPlayerData");

		m_players = new GameObject[PLAYER_MAX];

		m_timer = this.transform.FindChild("Timer").GetComponent<Timer>();
		//m_goalDirection = this.transform.FindChild("GoalDirection").GetComponent<GoalDirection>();
		m_capturedUI = this.transform.FindChild("CapturedCounter").GetComponent<CapturedCountUI>();

		// メンバの初期化
		m_timer.Initialize();
		//m_goalDirection.Initialize();
		StartCoroutine(m_capturedUI.Initialize());	// プレイヤー情報を取得するためコルーチン処理

		// キャンパスにカメラをセット
		StartCoroutine("SetCamera");
	}
}
