﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GoalDirection : MonoBehaviour {

	// メンバ変数

	private GameObject		m_goal 	  = null;	// ゴール
	private RectTransform	m_spriteRectTrans;	// 矢印画像のトランスフォーム
	private GameObject		m_player1 = null;	// プレイヤー１
	private GameObject		m_player2 = null;	// プレイヤー２
	private GameObject		m_player3 = null;	// プレイヤー３
	private GameObject		m_player4 = null;	// プレイヤー４

	private delegate float CalculateProcess();				// 演算処理
	private CalculateProcess CalculateGoalDirection = null;	// ゴール方向計算処理


	/*********************************************
	 * 初期化処理
	 *********************************************/
	public void Initialize(){
//		if ( GameManager.GetInstance.GetPlayerNum() == 2 ){
//			CalculateGoalDirection = CalculateGoalDirection_Duet;
//		}
//		else
		{
			CalculateGoalDirection = CalculateGoalDirection_Solo;
		}

		// オブジェクトを探す
		//StartCoroutine("FindPlayer");
		//StartCoroutine("FindGoal");

		m_spriteRectTrans = this.GetComponent<RectTransform>();

		// はじめは非表示にする
		this.GetComponent<Image>().enabled = false;
	}





	/*********************************************
	 * ゴールをを探す処理
	 *********************************************/
	IEnumerator FindGoal(){
		Debug.Log("Start Find Goal");

		GameObject building = null;
		while(building == null){
			building = GameObject.Find("Billding(Clone)");

			yield return null;
		}

		while(m_goal == null){
			m_goal = building.transform.FindChild("Goal(Clone)").gameObject;

			yield return null;
		}

		Debug.Log("Goal Find!");
	}
	
	/*********************************************
	 * 更新処理
	 *********************************************/
	void Update () {
		if ( m_player1 == null || m_goal == null ){
			return;
		}

		float dir = CalculateGoalDirection();

		// 画面中心からの距離を求める
		m_spriteRectTrans.localPosition = 
			new Vector3( ((Screen.width / 2) * 2f / 3f) * Mathf.Cos(dir * Mathf.Deg2Rad),
				((Screen.height / 2) * 2f / 3f) * Mathf.Sin(dir * Mathf.Deg2Rad),
				0f);

		// 画像の矢印の向きを変更
		m_spriteRectTrans.localRotation = Quaternion.Euler( new Vector3(0f, 0f, dir) );
	}


	/*********************************************
	 * １人プレイ時のゴール方向計算処理
	 *********************************************/
	private float CalculateGoalDirection_Solo(){

		float result = Mathf.Atan2( m_goal.transform.localPosition.y - m_player1.transform.localPosition.y,
			m_goal.transform.localPosition.x - m_player1.transform.localPosition.x ) * Mathf.Rad2Deg;

		Debug.Log(result);

		return result;
	}


	/*********************************************
	 * ２人プレイ時のゴール方向計算処理
	 *********************************************/
	private float CalculateGoalDirection_Duet(){
		// プレイヤーの中点を算出
		Vector3 center = m_player2.transform.localPosition - m_player1.transform.localPosition;

		float result = Mathf.Atan2( m_goal.transform.localPosition.y - center.y, 
			m_goal.transform.localPosition.x - center.x ) * Mathf.Rad2Deg;

		return result;
	}
}
