﻿using UnityEngine;
using System.Collections;

public class EntrySceneStateMachineBehavior : StateMachineBehaviour {


	/*********************************************
	 * ステートに入った瞬間の処理
	 *********************************************/
	void OnStateEnter(Animator _animator, AnimatorStateInfo _stateInfo, int _layerIndex){
		Debug.Log("StateMachine_In");
	}


	/*********************************************
	 * ステートから出る瞬間の処理
	 *********************************************/
	void OnStateExit(Animator _animator, AnimatorStateInfo _stateInfo, int _layerIndex){
		Debug.Log("StateMachine_Out");
	}
		

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
