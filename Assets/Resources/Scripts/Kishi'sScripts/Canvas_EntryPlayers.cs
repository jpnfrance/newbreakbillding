﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Canvas_EntryPlayers : MonoBehaviour {

	// メンバ変数
	private const int 						PLAYER_MAX = 4;	// 最大参加人数
	[SerializeField] private int			PLAYER_MIN;		// 最小参加人数
	[SerializeField] private GameObject[] 	m_players = new GameObject[PLAYER_MAX];	// プレイヤー
	private Scene.EntrySceneState			m_sceneState;	// 現在のシーンステート
	private bool 							m_isChanged = false;	// シーンチェンジしたかどうか


	/*********************************************
	 * 初期化処理
	 *********************************************/
	void Start(){
		// シーンステートを取得
		m_sceneState = this.transform.FindChild("SceneEntry").GetComponent<Scene.EntrySceneState>();

		// キャンバス内のプレイヤーUIを取得
		for( int i = 0; i < PLAYER_MAX; ++i ){
			string objectName = "Player" + (i + 1);

			m_players[i] = this.transform.FindChild(objectName).gameObject;
			m_players[i].GetComponent<EntryPlayerUI>().Initialize(i + 1);
		}

		// 入っている情報を削除
		EntryPlayerData.Instance.RemovePlayers();
	}

	
	/*********************************************
	 * 更新処理
	 *********************************************/
	void Update () {
		if ( m_isChanged ) return;

		// 参加受付
		EntryPlayer();

		// タイトルバック受付
		TurnBackTitleScene();

		// 参加準備完了メンバ数
		List<int> entries = EntryPlayerData.Instance.EntryPlayers;

		// 人数が２人以上
		if ( entries.Count >= PLAYER_MIN ){
			bool isPushedStart = false;
		
			// 参加メンバのStartボタンを検知
			foreach( int value in entries ){
				if ( GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.Start, (GamepadInput.GamePad.Index)value) ){
					isPushedStart = true;
				}
			}
		
			if ( isPushedStart ){
				// 準備が出来たようなので,シーン遷移する
				m_sceneState.ChanegPlayScene();
				m_isChanged = true;

				// ゲムマネージャにプレイヤー情報を渡す
				GameManager.GetInstance.EntryPlayers = EntryPlayerData.Instance.EntryPlayers;

				// プイヤーのキーを受け付けなくする
				SetLockKey();
			}
		}
	}


	/*********************************************
	 * プレイヤーキーロック処理
	 *********************************************/
	void SetLockKey(){
		foreach( GameObject player in m_players ){
			player.GetComponent<EntryPlayerUI>().LockKey = true;
		}
	}


	/*********************************************
	 * エントリー受付処理
	 *********************************************/
	void EntryPlayer(){

		Debug.Log("Ready Entry...");

		// エントリー
		if ( GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.Start, GamepadInput.GamePad.Index.One) ){
			m_players[(int)GamepadInput.GamePad.Index.One - 1].GetComponent<EntryPlayerUI>().Entry();
			SeManager.GetInstance.Play((int)E_SeType.CURSOR_OK);
		}
		if ( GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.Start, GamepadInput.GamePad.Index.Two) ){
			m_players[(int)GamepadInput.GamePad.Index.Two - 1].GetComponent<EntryPlayerUI>().Entry();
			SeManager.GetInstance.Play((int)E_SeType.CURSOR_OK);
		}
		if ( GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.Start, GamepadInput.GamePad.Index.Three) ){
			m_players[(int)GamepadInput.GamePad.Index.Three - 1].GetComponent<EntryPlayerUI>().Entry();
			SeManager.GetInstance.Play((int)E_SeType.CURSOR_OK);
		}
		if ( GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.Start, GamepadInput.GamePad.Index.Four) ){
			m_players[(int)GamepadInput.GamePad.Index.Four - 1].GetComponent<EntryPlayerUI>().Entry();
			SeManager.GetInstance.Play((int)E_SeType.CURSOR_OK);
		}
	}


	/*********************************************
	 * エントリー受付処理
	 *********************************************/
	void TurnBackTitleScene(){
		bool result = false;
		if ( GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.B, GamepadInput.GamePad.Index.One) ){
			if ( !m_players[(int)GamepadInput.GamePad.Index.One - 1].GetComponent<EntryPlayerUI>().IsReady ){
				result = true;
			}
		}

		if ( result ){
			// タイトルシーンに切り換え
			m_sceneState.ChangeTitleScene();
		}
	}


}
