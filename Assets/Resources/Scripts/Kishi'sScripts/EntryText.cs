﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EntryText : MonoBehaviour {

	// メンバ変数
	private Blinker	m_blinker;		// 点滅装置


	/*********************************************
	 * 生成時処理
	 *********************************************/
	void Start () {
		m_blinker = new Blinker();
		m_blinker.SetBlinkComponent( this.GetComponent<Text>() );

		// 点滅処理を行う
		StartCoroutine(m_blinker.Blink());
	}
}
