﻿using UnityEngine;
using System.Collections;


/*********************************************
 * エントリー画面のプレイヤーUIのスクリプト
 *********************************************/

public class EntryPlayerUI : MonoBehaviour {

	// メンバ変数
	public static float m_standardPositionX = 320f;		// 表示するX座標の基準
	public static float m_standardInvisiblePosX = 1280f;// 見えないときのX座標の基準
	public static float m_standardPositionY = 64f;		// 表示するY座標の基準
	public static float m_uiSpaceY	= 320f;				// UI毎の空ける間隔(Y)

	private GameObject 	m_readySprite;							// 準備完了スプライト
	private GameObject	m_waitEntryText;						// エントリー待ちテキスト
	private EntryCharacterMovement	m_characterMovement;		// プレイヤーモデルの挙動

	private int 						m_playerNumber;			// プレイヤー番号(1~4)
	[SerializeField]private Vector2 	m_uiVisiblePosition;	// UIが見える位置
	[SerializeField]private Vector2		m_uiInvisiblePosition;	// UIが見えない位置
	private bool 						m_isEntry = false;		// エントリーしたかどうか
	private bool						m_isReady = false;		// 決定したかどうか
	private bool						m_isLockKey = false;	// キーロックフラグ

	// プロパティ
	public bool IsReady{
		get{ return m_isReady; }
	}
	public bool LockKey{
		set{ m_isLockKey = value; }
	}


	/*********************************************
	 * エントリー処理
	 *********************************************/
	public void Entry(){
		if ( m_isEntry ) return;	// 既にエントリーしてる場合は無視
		m_isEntry = true;
		m_characterMovement.Visible = true;

		m_waitEntryText.SetActive(false);
		StartCoroutine("FrameIn");
	}


	/*********************************************
	 * 初期化処理
	 *********************************************/
	public void Initialize(int _playerNumber){
		m_playerNumber = _playerNumber;

		// 準備完了スプライトの取得
		m_readySprite = this.transform.FindChild("Ready").gameObject;
		m_readySprite.SetActive(false);

		// 座標調整
		float posX = m_standardPositionX;
		float posY = m_standardPositionY;

		// 奇数番目のプレイヤーのUIは←
		posX *= Mathf.Pow(-1f, m_playerNumber );

		if ( m_playerNumber > 2 ){
			posY -= m_uiSpaceY;
		}
		m_uiVisiblePosition = new Vector2( posX, posY );
		m_uiInvisiblePosition = new Vector2( m_standardInvisiblePosX * Mathf.Pow(-1, m_playerNumber), posY );

		this.transform.localPosition = m_uiInvisiblePosition;

		// エントリー待ちテキストの取得
		string entryTextName = "WaitEntryText" + m_playerNumber.ToString();
		m_waitEntryText = this.transform.parent.FindChild(entryTextName).gameObject;
		m_waitEntryText.transform.localPosition = m_uiVisiblePosition;

		string characterName = "Character_";
		switch( m_playerNumber ){
		case 1:
			characterName += "Red";
			break;

		case 2:
			characterName += "Green";
			break;

		case 3:
			characterName += "Blue";
			break;

		case 4:
			characterName += "Yellow";
			break;

		default:
			Debug.LogError("Not define Player!");
			break;
		}

		// キャラクターについているアニメータを取得
		m_characterMovement = this.transform.FindChild(characterName).GetComponent<EntryCharacterMovement>();
	}


	/*********************************************
	 * 更新処理
	 *********************************************/
	public void Update () {
		// エントリー前もしくはスタートするフェードが始まったら受け付けない
		if ( !m_isEntry || m_isLockKey ) return;

		if ( !m_isReady ){
			if ( GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.A, (GamepadInput.GamePad.Index)m_playerNumber) ){
				// 準備完了にする
				StartCoroutine( SetReady(true) );
				EntryPlayerData.Instance.AddPlayer(m_playerNumber);
				SeManager.GetInstance.Play((int)E_SeType.BURST_NORMAL);
			}
		}
		else{
			if ( GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.B, (GamepadInput.GamePad.Index)m_playerNumber) ){
				// 準備状態を解除
				StartCoroutine( SetReady(false) );
				EntryPlayerData.Instance.RemovePlayer(m_playerNumber);
			}
		}
	}


	/*********************************************
	 * 準備状態設定処理
	 *********************************************/
	private IEnumerator SetReady(bool _isReady){
		yield return null;

		m_isReady = _isReady;
		m_readySprite.SetActive(m_isReady);
	}


	/*********************************************
	 * フレームイン処理
	 *********************************************/
	private IEnumerator FrameIn(){
		this.transform.localPosition = m_uiVisiblePosition;
		yield return null;
	}
}
