﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EntryPlayerData : SingletonMonoBehaviour<EntryPlayerData> {

	// メンバ変数
	private int 		PLAYER_MAX = 4;	// プレイヤー上限
	[SerializeField] private List<int>	m_entryPlayer;	// エントリーしたプレイヤー


	// プロパティ
	public List<int> EntryPlayers{		// 参加するプレイヤー番号
		get{ return m_entryPlayer; }
	}


	/*********************************************
	 * プレイヤー追加処理
	 *********************************************/
	public void AddPlayer(int _id){
		if ( !m_entryPlayer.Contains(_id) ){
			m_entryPlayer.Add(_id);
		}
	}


	/*********************************************
	 * プレイヤー削除処理
	 *********************************************/
	public void RemovePlayer(int _id){
		m_entryPlayer.Remove(_id);
	}


	/*********************************************
	 * プレイヤー削除処理
	 *********************************************/
	public void RemovePlayers(){
		m_entryPlayer.Clear();
	}



	/*********************************************
	 * 生成時処理
	 *********************************************/
	void Awake(){
		// 複数生成を防ぐ
		if ( Instance != this ){
			Destroy(this.gameObject);
			return;
		}

		DontDestroyOnLoad(this.gameObject);

		// リストの初期化
		m_entryPlayer = new List<int>();
	}

}
