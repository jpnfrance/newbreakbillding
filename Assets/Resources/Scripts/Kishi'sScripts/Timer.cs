﻿using UnityEngine;
using UnityEngine.UI;		// UIを使うよ
using System.Collections;
using System.Collections.Generic;	// List<>を使うよ

/******************************************
 * タイマークラス
 * 
 * 対戦モードにて使用します
 * Limitで制限時間を設定してください
 * (ただし,タイマー作動中は設定できません)
 * 
 * StartTimer()呼び出しでタイマーを作動させられます
 * 
 * StopTimer()呼び出しでタイマーを停止できます
 * (別にTimeScaleを0にしても構いません)
 * 
 * ResetTimer()呼び出しでタイマーをリセットできます
 * (ただしタイマー作動中はリセットできません)
 * 
 ******************************************/
public class Timer : MonoBehaviour {

	// メンバ変数
	private const int		TIME_SPRITE_AMOUNT = 8;			// 表示桁数(うち２つはコロン)
	private float			LIMIT_TIME = 30f;				// タイムアップまでの時間
	private const float		TIMER_POSITION_OFFSET_Y = 48f;	// タイマーのY座標オフセット
	[SerializeField] private Vector2	m_spriteSize;		// スプライトのサイズ
	[SerializeField] private float 		m_spriteSpace;		// スプライトの間隔
	private GameObject[]	m_timeSprites;			// 時間表示のスプライト
	private UINumbers		m_uiNumbers;			// 数字スプライト
	private float 			m_timerPositionY;		// タイマーの表示位置
	private float 			m_timer = 0f;			// タイマー
	private bool			m_isLock = false;		// 設定のロック(falseの場合設定可)

    private GameTimeManager m_timeManager;

	// プロパティ
	public float LimitTime{
		set{
			if ( !m_isLock ) LIMIT_TIME = value; 
		}
	}



	/*********************************************
	 * 初期化処理
	 *********************************************/
	public void Initialize(){
		// 数字スプライト
		m_uiNumbers = this.transform.FindChild("UINumbers").GetComponent<UINumbers>();

		m_timerPositionY = this.transform.parent.GetComponent<GamingUIs>().SCREEN_H / 2f - TIMER_POSITION_OFFSET_Y;

		// バックスプライトの初期化
		Transform backSprite = this.transform.FindChild("BackBoard");
		backSprite.localPosition = new Vector3( 0f, m_timerPositionY, 0f );

		// 時間スプライトの初期化
		m_timeSprites = new GameObject[TIME_SPRITE_AMOUNT];
		for( int i = 0; i < TIME_SPRITE_AMOUNT; ++i ){
			// プレハブから作成
			m_timeSprites[i] = Instantiate(Resources.Load("Prefabs/Kishi'sPrefabs/NumberSprite")) as GameObject;
			m_timeSprites[i].transform.SetParent( this.transform, false );
			// スプライトのサイズ設定
			m_timeSprites[i].GetComponent<RectTransform>().sizeDelta = m_spriteSize;
			// 位置調整
			m_timeSprites[i].transform.localPosition = 
				new Vector2( m_spriteSpace * (i - TIME_SPRITE_AMOUNT / 2), m_timerPositionY );
		}

        m_timeManager = GameObject.FindWithTag("Manager").GetComponent<GameTimeManager>();

	}


	/*********************************************
	 * タイマースタート処理
	 *********************************************/
	public void StartTimer(){
		// 設定をロックする
		m_isLock = true;
	}


	/*********************************************
	 * タイマーストップ処理
	 *********************************************/
	public void StopTimer(){
		m_isLock = false;
	}


	/*********************************************
	 * リセット処理
	 *********************************************/
	public void ResetTimer(){
		if ( !m_isLock ){
			m_timer = 0f;
		}
	}


	/*********************************************
	 * 更新処理
	 *********************************************/
	void Update(){
		// 残り時間の計算
		//if ( LIMIT_TIME - m_timer > 0f ){
        if (m_timeManager.GetTimeLimit() > 0) {
            //CalculateRemainingMinute(LIMIT_TIME - m_timer);
            CalculateRemainingMinute(m_timeManager.GetTimeLimit());
		}
		else{
			CalculateRemainingMinute( 0f );
		}

		// 設定可能状態ならば動かさない
		if ( !m_isLock ) return;

		m_timer += Time.deltaTime;
	}


	/*********************************************
	 * 残り分計算処理
	 * 
	 * 引数 : 残り時間
	 *********************************************/
	private void CalculateRemainingMinute(float _remainingTime){

		float dummy = _remainingTime;
		// 分の表示
		int minute = (int)(dummy / 60);
		m_timeSprites[0].GetComponent<Image>().sprite = m_uiNumbers[minute / 10];
		if (minute / 10 == 0){
			m_timeSprites[0].SetActive(false);
		}
		m_timeSprites[1].GetComponent<Image>().sprite = m_uiNumbers[minute % 10];

		// コロン
		m_timeSprites[2].GetComponent<Image>().sprite = m_uiNumbers[(int)UINumbers.E_NumberTexture.COLON];

		// 秒の表示
		int second = (int)(dummy % 60);	
		m_timeSprites[3].GetComponent<Image>().sprite = m_uiNumbers[second / 10];
		m_timeSprites[4].GetComponent<Image>().sprite = m_uiNumbers[second % 10];

		// コロン
		m_timeSprites[5].GetComponent<Image>().sprite = m_uiNumbers[(int)UINumbers.E_NumberTexture.COLON];

		// 小数点部分の表示
		int comma = (int)( (dummy - Mathf.Floor(dummy)) * 100 );
		m_timeSprites[6].GetComponent<Image>().sprite = m_uiNumbers[comma / 10];
		m_timeSprites[7].GetComponent<Image>().sprite = m_uiNumbers[comma % 10];
	}
}
