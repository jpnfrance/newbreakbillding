﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CapturedCountUI : MonoBehaviour {

	// メンバ変数
	private const int 					PLAYER_MAX = 4;			// プレイヤーの上限
	private const float 				SPRITE_SIZE = 96f;		// スプライトサイズ
	private GamingUIs					m_gamingUI = null;		// ゲームUI
	[SerializeField] private UINumbers	m_uiNumbers = null;		// 数字UI
	[SerializeField] private Image[]	m_counterSprites;		// 攻略カウンタースプライト
	[SerializeField] private Image[]	m_numberSprites;		// 数字スプライト
	private bool 						m_isActive = false;		// スプライトがアクティブかどうか


	/*********************************************
	 * 初期化処理
	 *********************************************/
	public IEnumerator Initialize(){
		// 親の取得
		m_gamingUI = this.transform.parent.GetComponent<GamingUIs>();
		// 数字UIの取得
		m_uiNumbers = m_gamingUI.transform.FindChild("Timer").FindChild("UINumbers").GetComponent<UINumbers>();

		// １フレーム置く
		yield return null;

		// スプライトの取得
		m_counterSprites = new Image[PLAYER_MAX];
		for( int i = 0; i < PLAYER_MAX; ++i ){
			// オブジェクトの取得
			string imageName = "Player";
			imageName += (i + 1).ToString();

			m_counterSprites[i] = this.transform.FindChild(imageName).GetComponent<Image>();

			// 1,2Pは上にUIを表示
			float posY = -1f;
			if ( i < 2 ) {
				posY *= -1f;
			}

			// 座標調整
			Vector3 position = new Vector3( ( m_gamingUI.SCREEN_W / 2 - SPRITE_SIZE / 2 - m_gamingUI.SpriteOffset) * Mathf.Pow(-1, i + 1),
											( m_gamingUI.SCREEN_H / 2 - SPRITE_SIZE / 2 - m_gamingUI.SpriteOffset) * posY,
											0f );
			m_counterSprites[i].transform.localPosition = position; 
		}

		// 全てのプレイヤーの攻略回数スプライトを非アクティブにする
		foreach( Image value in m_counterSprites ){
			value.gameObject.SetActive(false);
		}
	}


	/*********************************************
	 * 攻略スプライトをアクティブにする処理
	 *********************************************/
	public void SetActiveSprite(){
		// 参加プレイヤー情報取得
		List<int> playerList = EntryPlayerData.Instance.EntryPlayers;

		foreach( int number in playerList ){
			m_counterSprites[number - 1].gameObject.SetActive(true);
		}

		m_isActive = true;

		// 数字スプライトを生成する
		CreateNumberSprite();
	}


	/*********************************************
	 * 数字スプライト生成処理
	 *********************************************/
	public void CreateNumberSprite(){
		m_numberSprites = new Image[PLAYER_MAX];

		// 参加しているプレイヤーだけ数字を生成
		for( int i = 0; i < PLAYER_MAX; ++i ){
			if ( m_gamingUI[i + 1] != null ){
				// インスタンス生成
				GameObject numberSprite = Instantiate(Resources.Load("Prefabs/Kishi'sPrefabs/NumberSprite"))as GameObject;
				// コンポーネント取得
				m_numberSprites[i] = numberSprite.GetComponent<Image>();
				// 親の設定
				numberSprite.transform.SetParent(m_counterSprites[i].transform, false);
			}
			else{
				m_numberSprites[i] = null;
			}
		}
	}


	/*********************************************
	 * 更新処理
	 *********************************************/
	void Update(){
		// スプライトが表示されていないなら戻る
		if ( !m_isActive ) return;

		// プレイヤーのステージ攻略回数の描画
		for( int i = 0; i < PLAYER_MAX; ++i ){
			if ( m_numberSprites[i] != null ){
				// 数字の更新
				m_numberSprites[i].sprite = m_uiNumbers[m_gamingUI[i + 1].GetComponent<PlayerControl>().point];
				// 数字を黒で表示
				m_numberSprites[i].color = Color.black;
			}
		}
	}
}
