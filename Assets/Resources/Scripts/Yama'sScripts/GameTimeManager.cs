﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameTimeManager : MonoBehaviour {

    public float timeLimit;     // ゲームの制限時間(初期値、実行中は変動しない
    private float _timeLimit;   // ゲームの制限時間(計算値、外部から変更できない

    private bool isGameOver = false;

    private PlayerManager playerManager;    // プレイヤマネージャ(シーン遷移時値渡し用

	// Use this for initialization
	void Start () {
        _timeLimit = timeLimit;

        playerManager = transform.GetComponent<PlayerManager>();
        if (playerManager == null) Debug.LogError("Caution! Cant Get PlayerManager!");
	}
	
	// Update is called once per frame
	void Update () {
        if(!isGameOver)_timeLimit -= Time.deltaTime;

        if (_timeLimit <= 0f && !isGameOver)
        {
            isGameOver = true;
            WhenGameOver();
        }
	}

    void WhenGameOver()
    {
        SeManager seManager = SeManager.GetInstance;

        seManager.Play((int)E_SeType.GAME_FINISH);

        // リストに格納したパラメータを取得する
        List<int> playerIDList = playerManager.GetPlayerIDList();

        // パラメータを入れる配列を宣言する
        int[,] para = new int[(int)E_ControlPlayerType.MAX, (int)E_ResultParameterType.MAX];

        // プレイヤーの数だけ回す
        for ( int i = 0; i < (int)E_ControlPlayerType.MAX; ++i )
        {
            // intの要素数が有るか判断
            if (playerIDList.Contains(i))
            {
                // ゴール回数を取得する
                para[i, (int)E_ResultParameterType.PLAYER_GOAL] = playerManager.GetPlayerPoint(i);
                para[i, (int)E_ResultParameterType.PLAYER_DEATH] = playerManager.GetPlayerDeadCount(i);
                para[i, (int)E_ResultParameterType.PLAYER_PUT_BOMB] = playerManager.GetPlayerUseBombCount(i)
                                                                    + playerManager.GetPlayerUseWideBombCount(i)
                                                                    + playerManager.GetPlayerUseVerticalBombCount(i);

            }
        }
        // パラメータをゲームマネージャに渡す
        GameManager.GetInstance.ResultParameter = para;

        SceneControlManager.GetInstance.AddPause<Pause.FinishcallPauseState>();

        Destroy(playerManager.gameObject);
    }

    // Accessor
    public float GetTimeLimit(){return _timeLimit;}
}
