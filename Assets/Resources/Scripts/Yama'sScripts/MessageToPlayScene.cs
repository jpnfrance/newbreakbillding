﻿using UnityEngine;
using System.Collections;
using GamepadInput;
//

public class MessageToPlayScene {

    private static MessageToPlayScene instance = null;

    private GamepadInput.GamePad.Index pressPad = GamePad.Index.None;

    public static MessageToPlayScene GetInstance()
    {
        if (MessageToPlayScene.instance == null)
        {
            MessageToPlayScene.instance = new MessageToPlayScene();
        }

        return MessageToPlayScene.instance;
    }

    public static bool isExist()
    {
        return instance != null;
    }

    public static void InitializeMessage()
    {
        MessageToPlayScene.instance = null;
        //this.pressPad = GamePad.Index.None;
    }

    public GamePad.Index GetPressPad()
    {
        return pressPad;
    }

    public void SetPressPad(GamePad.Index pressPad)
    {
        this.pressPad = pressPad;
    }
}
