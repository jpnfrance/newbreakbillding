﻿using UnityEngine;
using System.Collections;

public class GameUIController : MonoBehaviour {

    public int playerID;
    private PlayerControl playerControl;

    public GameObject pointDisplayerObject;
    public GameObject wideBombsCountDisplayerObject;
    public GameObject verticalBombsCountDisplayerObject;
    private UINumSpriteController pointDisplayer;
    private UINumSpriteController wideBombsCountDisplayer;
    private UINumSpriteController verticalBombsCountDisplayer;

	// Use this for initialization
	void Start () {
        GameObject manager = GameObject.Find("Managers");
        PlayerManager playerManager = manager.GetComponent<PlayerManager>();
        GameObject playerObject = playerManager.GetPlayerObject(playerID);
        playerControl = playerObject.GetComponent<PlayerControl>();

        pointDisplayer = pointDisplayerObject.GetComponent<UINumSpriteController>();
        wideBombsCountDisplayer = wideBombsCountDisplayerObject.GetComponent<UINumSpriteController>();
        verticalBombsCountDisplayer = verticalBombsCountDisplayerObject.GetComponent<UINumSpriteController>();

	}
	
	// Update is called once per frame
	void Update () {
        pointDisplayer.displayNumber = playerControl.point;
        wideBombsCountDisplayer.displayNumber = playerControl.GetHasBombs(RoomScript.bombs.WIDEBOMS);
        verticalBombsCountDisplayer.displayNumber = playerControl.GetHasBombs(RoomScript.bombs.VERTICALBOMB);
	}
}
