﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class UINumSpriteController : MonoBehaviour {

    public int displayNumber;

    public Sprite[] numbers;
    private Image image;

	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        image.sprite = numbers[displayNumber];
	}
}
