﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/04

＜内容＞
・選択中の項目に合わせてイメージを表示させる処理。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 選択中の項目のイメージを表示するクラス
/// </summary>
///-------------------------------------------------------------- 
public class SelectedImage : MonoBehaviour 
{
    /*** public変数 ***/
    public Sprite[] m_selectedImageArray;

    /*** private変数 ***/
    private Image m_image;
    private bool m_isFlash;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
    void Awake()
    {
        // イメージ情報を取得する
        m_image = this.GetComponent<Image>();
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 選択したイメージを表示させる
    /// </summary>
    /// <param name="num"></param>
    ///-------------------------------------------------------------- 
    public void DrawingImage( int num )
    {
        m_image.sprite = m_selectedImageArray[num];
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 点滅を開始する
    /// </summary>
    ///-------------------------------------------------------------- 
    public void StartFlash()
    {
        // 点滅中だったら処理を行わない
        if( m_isFlash ) return;

        // 点滅した状態にする
        m_isFlash = true;

        // 点滅を開始する
        StartCoroutine(Flash());
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 点滅の処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator Flash()
    {
        // 点滅するための初期化
        int flashCount = 0;
        int flashTime = 3;
        float minFlash = 0.0f;
        float maxFlash = 1.0f;
        Color color = m_image.color;

        // ずっと点滅し続ける
        while( true )
        {
            // 点滅のカウントを増やす
            flashCount++;

            // 点滅処理
            color.a = GlobalProcess.SinWave(flashCount, flashTime, minFlash, maxFlash);
            m_image.color = color;
            yield return null;
        }
    }
}
