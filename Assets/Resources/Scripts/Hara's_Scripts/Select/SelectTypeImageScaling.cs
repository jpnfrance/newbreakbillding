﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/03

＜内容＞
・

---------------------------------------------------------------------------------------*/
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 選択したイメージを拡縮するクラス
/// </summary>
///-------------------------------------------------------------- 
public class SelectTypeImageScaling : MonoBehaviour 
{
    /*** public変数 ***/ 
    public SinScale[] m_selectImageArray;

    /*** private変数 ***/ 
    private int m_num;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 最初の数値をセットする
    /// </summary>
    /// <param name="num"></param>
    ///--------------------------------------------------------------
    public void StartCount( int num )
    {
        // 新しく選択された項目のイメージを拡縮する
        m_selectImageArray[num].StartWave();
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 拡縮を操作する
    /// </summary>
    /// <param name="num"></param>
    ///-------------------------------------------------------------- 
    public void ScaleControl( int num )
    {
        // 選択中の番号が前の番号と違ったら拡縮するイメージを変える
        if( m_num != num )
        {
            // 前まで拡縮していたイメージを止める
            m_selectImageArray[m_num].StopWave();

            // 新しく選択された項目のイメージを拡縮する
            m_selectImageArray[num].StartWave();

            // 新しい番号を登録
            m_num = num;
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 指定したイメージを削除する
    /// </summary>
    /// <param name="num"></param>
    ///-------------------------------------------------------------- 
    public void DestroySelectedImage( int num )
    {
        // 選択したものがすでになかったら処理を行わない
        if (!m_selectImageArray[num]) return;

        // 指定したイメージを削除する
        Destroy( m_selectImageArray[num].gameObject );
    }
}
