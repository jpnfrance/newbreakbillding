﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/03

＜内容＞
・選択に合わせてキャラクターを動かす処理。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 選択に合わせてキャラクターを動かすクラス
/// </summary>
///-------------------------------------------------------------- 
public class SelectCharacterMove : MonoBehaviour 
{
    /*** public変数 ***/ 
    public Vector3[] m_pointArray;
    public float m_moveTime = 1.0f;

    /*** private変数 ***/
    private bool m_isMove;
    private Vector3 m_startPosition;
    private Vector3 m_endPosition;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        // 動いていない状態で初期化
        m_isMove = false;
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 移動状態のゲッタ－
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    public bool GetIsMove()
    {
        return m_isMove;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// プレイヤーを削除する
    /// </summary>
    ///-------------------------------------------------------------- 
    public void DestroyPlayer()
    {
        Destroy( gameObject );
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 移動処理を始める
    /// </summary>
    /// <param name="pointID"></param>
    ///-------------------------------------------------------------- 
    public void StartMove( int pointID )
    {
        // 移動していたら処理を行わない
        if( m_isMove ) return;

        // カーソルの移動音
        SeManager.GetInstance.Play((int)E_SeType.CURSOR_MOVE);

        // フラグを移動状態にする
        m_isMove = true;

        // 移動処理を始める
        StartCoroutine(Move( pointID ));
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 移動処理
    /// </summary>
    /// <param name="pointID"></param>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator Move( int pointID )
    {
        // 移動に使用するカウントの初期化
        float moveCount = 0.0f;

        // スタートとゴールポイントを設定
        m_startPosition = this.transform.localPosition;
        m_endPosition = m_pointArray[pointID];

        // カウントが時間になるまで続ける
        while( moveCount < m_moveTime )
        {
            // カウントを加算ん
            moveCount += 0.007f;

            // 補間で使用するための数値を計算
            float lerpTime = moveCount / m_moveTime;

            // 補間を使って移動処理を行う
            this.transform.localPosition = Vector3.Lerp( m_startPosition, m_endPosition, lerpTime );
            yield return null;
        }

        // フラグを移動していない状態にする
        m_isMove = false;
    }
}
