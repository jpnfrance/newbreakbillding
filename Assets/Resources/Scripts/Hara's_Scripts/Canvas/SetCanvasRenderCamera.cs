﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/27

＜内容＞
・キャンバスにカメラを適応させる処理。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// キャンバスにカメラを適応させるクラス
/// </summary>
///-------------------------------------------------------------- 
public class SetCanvasRenderCamera : MonoBehaviour 
{
    /*** public変数 ***/ 
    public int m_distance = 1;

    /*** private変数 ***/ 
    private Canvas m_canvas;
    private Camera m_camera;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        // キャンバスのレンダーモードを設定する
        m_canvas = this.GetComponent<Canvas>();
        m_canvas.renderMode = RenderMode.ScreenSpaceCamera;
	}
	
    ///-------------------------------------------------------------- 
    /// <summary>
    /// 更新処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Update () 
    {
        // カメラを適応させる
        this.AdaptationCamera();
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// カメラを適応させる処理
    /// </summary>
    ///-------------------------------------------------------------- 
    private void AdaptationCamera()
    {
        // カメラが存在していたら処理を行わない
        if( m_camera ) return;

        // タグを使ってカメラを検索し、キャンバスに適応させる
        m_camera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
        m_canvas.worldCamera = m_camera;

        // 深度を設定（カメラとの距離で決まる）
        m_canvas.planeDistance = m_distance;
    }
}
