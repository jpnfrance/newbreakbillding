﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/16

＜内容＞
・ゲーム全体を管理と設定を行う。
 管理しているもの。
→ セットアップ状態。
→ ゲームのルールの種類。
→ リザルトで必要なパラメータ。
→ 参加するプレイヤーの人数。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

///-------------------------------------------------------------- 
/// <summary>
/// ゲームマネージャのクラス
/// </summary>
///-------------------------------------------------------------- 
public class GameManager : MonoBehaviour 
{
    /*** private変数 ***/
    private int m_gameRuleType;
    private int m_playerNum;
    private bool m_isSetup;
    private bool m_isManual;
    private bool m_isPause;
    private bool m_isStartCall;
    private int[,] m_resultParameterArray;
    private List<int> m_entryPlayers;

    /*** static変数 ***/
    private static GameManager m_instance;

    ///--------------------------------------------------------------
    /// <summary>
    /// ゲットインスタンス
    /// </summary>
    ///--------------------------------------------------------------
    public static GameManager GetInstance
    {
        get
        {
            if (m_instance == null)
            {
                GameObject go = new GameObject("GameManager");
                m_instance = go.AddComponent<GameManager>();
            }

            return m_instance;
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        DontDestroyOnLoad(this);
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// ゲームをセットアップした状態にする
    /// </summary>
    ///-------------------------------------------------------------- 
    public void GameSetUp()
    {
        m_isSetup = true;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// セットアップ状態のフラグのゲッター
    /// </summary>
    ///-------------------------------------------------------------- 
    public bool GetIsSetup()
    {
        return m_isSetup;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// ゲームルールの種類のプロパティ
    /// </summary>
    ///-------------------------------------------------------------- 
    public int GameRuleType
    {
        get { return m_gameRuleType; }
        set { m_gameRuleType = value; }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// リザルトで必要なパラメータのプロパティ
    /// </summary>
    /// ・第一要素数；プレイヤーの種類
    /// ・第二要素数：パラメータ
    ///-------------------------------------------------------------- 
    public int[,] ResultParameter
    {
        get { return m_resultParameterArray; }
        set { m_resultParameterArray = value; }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// プレイヤーの人数のプロパティ
    /// </summary>
    ///-------------------------------------------------------------- 
    public int PlayerNum
    {
        get { return m_playerNum; }
        set { m_playerNum = value; }
    }

    public bool ManualState
    {
        get { return m_isManual; }
        set { m_isManual = value; }
    }

    public bool PauseState
    {
        get { return m_isPause; }
        set { m_isPause = value; }
    }

    public bool StartCull
    {
        get { return m_isStartCall; }
        set { m_isStartCall = value; }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 参加したプレイヤーのプロパティ
    /// </summary>
    ///-------------------------------------------------------------- 
    public List<int> EntryPlayers
    {
        get{ return m_entryPlayers; }
        set { m_entryPlayers = value; }
    }
}
