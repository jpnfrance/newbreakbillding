﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/15

＜内容＞
・ゲームを始める前に必要な情報を準備する。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// ゲームのセットアップ
/// </summary>
///-------------------------------------------------------------- 
public class GameSetup : MonoBehaviour 
{
    /*** public変数 ***/
    public GameObject[] m_prefabFadeArray;
    public GameObject[] m_prefabEffect;
    public AudioClip[] m_seAudioArray;
    public AudioClip[] m_bgmAudioArray;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
    public void Awake()
    {
        Debug.Log(m_prefabEffect);

        // セットアップする
        FadeManager.GetInstance.SetFadeArray(m_prefabFadeArray);    // フェードのプレハブをセット
        EffectManager.GetInstance.SetEffectArray( m_prefabEffect ); // エフェクトのプレハブをセット
        SeManager.GetInstance.SetAudioArray( m_seAudioArray );      // 効果音のサウンドをセット
        BgmManager.GetInstance.SetAudioArray(m_bgmAudioArray);      // BGMのサウンドをセット
    }
}
