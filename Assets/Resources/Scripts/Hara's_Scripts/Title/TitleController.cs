﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/07

＜内容＞
・

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;
using GamepadInput;

///-------------------------------------------------------------- 
/// <summary>
/// タイトルをコントロールするクラス
/// </summary>
///-------------------------------------------------------------- 
public class TitleController : MonoBehaviour 
{
    /*** public変数 ***/
    public LimitCounter m_limitCounter;
    public Scene.TitleSceneState m_titleSceneState;
    public SelectTypeImageScaling m_selectTypeImageScaling;
    public SelectedRoom m_selectedRoom;
    public SelectCharacterMove m_selectCharacterMove;

    /*** private変数 ***/ 

    /*** 定数 ***/
    private const int SCENE_SOLO_MODE = 0;
    private const int SCENE_BATTLE_MODE = 1;
    private const int SCENE_MANUAL = 2;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        m_selectTypeImageScaling.StartCount( m_limitCounter.GetCount() );
	}
	
    ///-------------------------------------------------------------- 
    /// <summary>
    /// 更新処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Update () 
    {
        // マニュアルが出ていたら処理を行わない
        if( GameManager.GetInstance.ManualState ) return;

        // 選択されている項目に合わせてイメージを拡縮する
        m_selectTypeImageScaling.ScaleControl( m_limitCounter.GetCount() );

        // ゲームパッドのスティックと十字ボタンの傾けた値を取得する
        Vector2 leftStickAxis = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any);
        Vector2 rightStickAxis = GamePad.GetAxis(GamePad.Axis.RightStick, GamePad.Index.Any);
        Vector2 dpadAxis = GamePad.GetAxis(GamePad.Axis.Dpad, GamePad.Index.Any);

        // ゲームパッドのスティックを傾けた量に合わせて選択を決める処理
        this.CheckTilt(leftStickAxis.y);
        this.CheckTilt(rightStickAxis.y);
        this.CheckTilt(dpadAxis.y);

        // キーボードによって移動を行う
        this.KeyControlMove();

        if( !m_selectCharacterMove.GetIsMove() )
        {
            // ゲームパッドのAボタンが押されたらシーンを変える
            if (GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.One))
            {
                ChangeScene(GamePad.Index.One);
            }

            if (GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Two))
            {
                ChangeScene(GamePad.Index.Two);
            }

            if (GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Three))
            {
                ChangeScene(GamePad.Index.Three);
            }

            if (GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Four))
            {
                ChangeScene(GamePad.Index.Four);
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                ChangeScene(GamePad.Index.None);
            }
        }
	}

    private void ChangeScene(GamePad.Index pressPad)
    {
        if (m_limitCounter.GetCount() == SCENE_MANUAL)
        {
            SeManager.GetInstance.Play((int)E_SeType.CURSOR_OK);

            m_titleSceneState.PopManual();
        }
        else
        {
            // 決定音
            SeManager.GetInstance.Play((int)E_SeType.BURST_NORMAL);

            // 選択した所に爆発の演出を出す
            EffectManager.GetInstance.StartEffect(
                (int)E_EffectType.NORMAL_BURST,
                m_selectCharacterMove.gameObject.transform.position,
                Quaternion.identity);

            // 次に進むシーンが決まったら削除するもの
            m_selectTypeImageScaling.DestroySelectedImage(m_limitCounter.GetCount());
            m_selectedRoom.DestroySelectedRoom(m_limitCounter.GetCount());
            m_selectCharacterMove.DestroyPlayer();

            // 決定した時に選択している番号によって処理を決める
            switch (m_limitCounter.GetCount())
            {
                // ソロプレイなのでプレイシーンに飛ぶ
                case SCENE_SOLO_MODE:
                    m_titleSceneState.ChangeSoloPlay();
                    MessageToPlayScene.GetInstance().SetPressPad(pressPad);
                    break;

                // バトルプレイなのでエントリーシーンに飛ぶ
                case SCENE_BATTLE_MODE:
                    m_titleSceneState.ChangeEntryScene();
                    break;
            }
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// キーボードによって移動をする
    /// </summary>
    ///-------------------------------------------------------------- 
    private void KeyControlMove()
    {
        // 移動中だったら処理を行わない
        if (m_selectCharacterMove.GetIsMove()) return;

        // 上に移動する
        if (Input.GetKey(KeyCode.UpArrow))
        {
            m_limitCounter.CountDown();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
        // 下に移動する
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            m_limitCounter.CountUp();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 傾けた量をチェックする処理
    /// </summary>
    /// <param name="x"></param>
    ///-------------------------------------------------------------- 
    private void CheckTilt(float x)
    {
        // 選択用のキャラクターが動いていたら処理を行わない
        if (m_selectCharacterMove.GetIsMove()) return;

        // 傾けた数値をチェックする数値
        float checkTiltUp = -0.5f;
        float checkTiltLow = 0.5f;

        // 上に傾けた時
        if (x < checkTiltUp)
        {
            m_limitCounter.CountUp();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
        // 下に傾けた時
        else if (x > checkTiltLow)
        {
            m_limitCounter.CountDown();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
    }
}
