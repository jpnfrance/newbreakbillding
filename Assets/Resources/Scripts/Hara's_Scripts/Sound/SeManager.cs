﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/17

＜内容＞
・効果音を管理する。



---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 効果音のマネージャクラス
/// </summary>
///-------------------------------------------------------------- 
public class SeManager : MonoBehaviour
{
    /*** private変数 ***/
    private int m_audioNumber;
    private AudioSource m_audioSorce;
    private AudioClip[] m_audioClipArray;

    /*** static変数 ***/
    private static SeManager m_instance;

    ///--------------------------------------------------------------
    /// <summary>
    /// ゲットインスタンス
    /// </summary>
    ///--------------------------------------------------------------
    public static SeManager GetInstance
    {
        get
        {
            if (m_instance == null)
            {
                GameObject go = new GameObject("SeManager");
                m_instance = go.AddComponent<SeManager>();
            }

            return m_instance;
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        // オブジェクトが削除されないようにする
        DontDestroyOnLoad(this);

        // オーディオが作られていない時
        if( m_audioSorce == null )
        {
            // // オーディオの環境を設定
            m_audioSorce = gameObject.AddComponent<AudioSource>();
        }

        // 初めにオーディオ番号を不特定数にする
        m_audioNumber = int.MinValue; ;
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// オーディオをセットする
    /// </summary>
    /// <param name="audioClipArray"></param>
    ///-------------------------------------------------------------- 
    public void SetAudioArray( AudioClip[] audioClipArray )
    {
        // オーディオをセット
        m_audioClipArray = audioClipArray;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 音を出す
    /// </summary>
    /// <param name="id"></param>
    ///-------------------------------------------------------------- 
    public void Play( int id )
    {
        // もし音がセットされていなかった音を出さない
        if (m_audioClipArray == null)
        {
            Debug.Log("効果音がセットされていません");
            return;
        }

        // 既にセットされているオーディオなら変更を行わない
        if (m_audioNumber != id)
        {
            // オーディオのIDをセットする
            m_audioNumber = id;
        }

        // 音を出す処理
        m_audioSorce.PlayOneShot(m_audioClipArray[m_audioNumber]);
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// 音を止める
    /// </summary>
    ///--------------------------------------------------------------
    public void Stop()
    {
        // 音を止める処理
        m_audioSorce.Stop();
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 音のボリュームを変更する（０～１）
    /// </summary>
    /// <param name="volumeNum"></param>
    ///-------------------------------------------------------------- 
    public void Volume( float volumeNum )
    {
        // ボリュームを指定した大きさに変更する
        m_audioSorce.volume = volumeNum;
    }
}