﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/07

＜内容＞
・レディーゴーの処理。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// レディゴーのクラス
/// </summary>
///-------------------------------------------------------------- 
public class StartCall : MonoBehaviour 
{
    /*** public変数 ***/ 
    public float m_readyTime = 1.0f;
    public float m_goTime = 1.0f;
    public Sprite m_readyImage;
    public Sprite m_goImage;

    /*** private変数 ***/
    private bool m_isEnd;
    private bool m_isCull;
    private Image m_image;

    /*** 定数 ***/
    private const float UP_TIME = 0.007f;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        // 自身のイメージ情報を取得する
        m_image = this.GetComponent<Image>();

        // フラグを呼び出していない状態にする
        m_isCull = false;

        // フラグを終了していない状態にする
        m_isEnd = false;
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 終了状態のゲッター
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    public bool GetIsEnd()
    {
        return m_isEnd;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// Ready・Go処理を開始する
    /// </summary>
    ///-------------------------------------------------------------- 
    public void StartCullReady()
    {
        // 呼び出し中だったら処理を行わない
        if( m_isCull ) return;

        // フラグを呼び出した状態にする
        m_isCull = true;

        // レディーゴー処理を行う
        StartCoroutine(CallReady());
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// ゲーム開始コール"Ready"の処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator CallReady()
    {
        // 初期化
        float count = 0.0f;
        float lerpCount = 0.0f;
        m_image = this.GetComponent<Image>();
        Color color = m_image.color;
        Vector3 startScale = new Vector3( 3.0f, 3.0f, 1.0f );
        Vector3 endScale = new Vector3( 1.0f, 1.0f, 1.0f );
        float startAlpha = 1.0f;
        float endAlpha = 0.5f;
        m_image.sprite = m_readyImage;

        // カウントが時間を上回るまで処理を行う
        while( count < m_readyTime )
        {
            // 補間の時間を出す
            lerpCount = count / m_readyTime;

            // 補間によってアルファ値を操作する
            color.a = Mathf.Lerp( startAlpha, endAlpha, lerpCount );
            m_image.color = color;

            // 補間によってスケールを操作する
            transform.localScale = Vector3.Lerp( startScale, endScale, lerpCount );

            // カウントを上げる
            count += UP_TIME;
            yield return null;
        }

        // ゲーム開始コール"Go"の処理
        StartCoroutine(CullGo());
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// ゲーム開始コール"Go"の処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator CullGo()
    {
        // 初期化
        float count = 0.0f;
        float lerpCount = 0.0f;
        Color color = m_image.color;
        Vector3 startScale = new Vector3(3.0f, 3.0f, 1.0f);
        Vector3 endScale = new Vector3(8.0f, 8.0f, 1.0f);
        float startAlpha = 0.5f;
        float endAlpha = 1.0f;
        m_image.sprite = m_goImage;

        // カウントが時間を上回るまで処理を行う
        while (count < m_readyTime)
        {
            // 補間の時間を出す
            lerpCount = count / m_goTime;

            // 補間によってアルファ値を操作する
            color.a = Mathf.Lerp(startAlpha, endAlpha, lerpCount);
            m_image.color = color;

            // 補間によってスケールを操作する
            transform.localScale = Vector3.Lerp( startScale, endScale, lerpCount );

            // カウントを上げる
            count += UP_TIME;
            yield return null;
        }

        // フラグを終了状態する
        m_isEnd = true;
    }
}
