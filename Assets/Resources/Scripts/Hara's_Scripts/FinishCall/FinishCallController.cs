﻿using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 終了コールをコントロールするクラス
/// </summary>
///-------------------------------------------------------------- 
public class FinishCallController : MonoBehaviour 
{
    /*** public変数 ***/
    public FinishCall m_finishCall;
    public Pause.FinishcallPauseState m_finishCallpauseState;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake ()
    {
        m_finishCall.StartCall();
	}
	
    ///-------------------------------------------------------------- 
    /// <summary>
    /// 更新処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Update () 
    {
	    if( m_finishCall.GetIsEnd() )
        {
            m_finishCallpauseState.ClosePause();
        }
	}
}
