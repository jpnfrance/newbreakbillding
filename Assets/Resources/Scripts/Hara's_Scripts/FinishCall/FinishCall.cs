﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/08

＜内容＞
・

---------------------------------------------------------------------------------------*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 終了コールのクラス
/// </summary>
///-------------------------------------------------------------- 
public class FinishCall : MonoBehaviour 
{
    /*** public変数 ***/
    public float m_callTime = 1.0f;

    /*** private変数 ***/
    private bool m_isCall;
    private bool m_isEnd;
    private Image m_image;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        m_image = this.GetComponent<Image>();

        m_isEnd = false;
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 終了フラグのゲッター
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    public bool GetIsEnd()
    {
        return m_isEnd;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 終了コールを開始する
    /// </summary>
    ///-------------------------------------------------------------- 
    public void StartCall()
    {
        if( m_isCall ) return;

        m_isCall = true;

        StartCoroutine( Call() );
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 終了コールの処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator Call()
    {
        float count = 0.0f;
        float lerpCount = 0.0f;
        m_image = this.GetComponent<Image>();
        Color color = m_image.color;
        Vector3 startScale = new Vector3( 1.0f, 1.0f, 1.0f );
        Vector3 endScale = new Vector3( 5.0f, 5.0f, 5.0f );
        float startAlpha = 0.5f;
        float endAlpha = 1.0f;

        while( count < m_callTime )
        {
            lerpCount = count / m_callTime;

            color.a = Mathf.Lerp( startAlpha, endAlpha, lerpCount );
            m_image.color = color;

            transform.localScale = Vector3.Lerp( startScale, endScale, lerpCount );

            count += 0.007f;
            yield return null;
        }

        m_isEnd = true;
    }
}
