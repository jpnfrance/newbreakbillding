﻿using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 揺れる動き処理のクラス
/// </summary>
   ///-------------------------------------------------------------- 
public class SwayMotion : MonoBehaviour
{
    /*** public変数 ***/
    public float m_swayTime = 1.0f;
    public float m_positionFrequency = 6.0f;
    public float m_positionAmount = 0.8f;
    public Vector3 m_positionComponents = Vector3.one;
    public int m_positionOctave = 3;

    /*** private変数 ***/
    private bool m_isSway;
    float m_timePosition;
    Vector2[] m_noiseVectors;
    Vector3 m_initialPosition;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
    void Awake()
    {
        m_timePosition = Random.value * 10;
        m_noiseVectors = new Vector2[6];

        for (var i = 0; i < 6; i++)
        {
            var theta = Random.value * Mathf.PI * 2;
            m_noiseVectors[i].Set(Mathf.Cos(theta), Mathf.Sin(theta));
        }

        m_initialPosition = transform.localPosition;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 揺れる動きを始める
    /// </summary>
    ///-------------------------------------------------------------- 
    private void StartSway()
    {
        // 揺れていたら行わない
        if (m_isSway) return;

        // 揺れる処理を実行する
        StartCoroutine(Sway());
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 揺れる処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator Sway()
    {
        // 揺れている状態にする
        m_isSway = true;

        // 止まる時間
        float stopTime = 0.0f;


        while( m_swayTime > stopTime )
        {
            stopTime += Time.deltaTime;

            m_timePosition += Time.deltaTime * m_positionFrequency;

            if (m_positionAmount != 0.0f)
            {
                var p = new Vector3(
                    Fbm(m_noiseVectors[0] * m_timePosition, m_positionOctave),
                    Fbm(m_noiseVectors[1] * m_timePosition, m_positionOctave),
                    Fbm(m_noiseVectors[2] * m_timePosition, m_positionOctave)
                );
                p = Vector3.Scale(p, m_positionComponents) * m_positionAmount * 2;
                transform.localPosition = m_initialPosition + p;
            }

            yield return null;
        }

        // 揺れていない状態にする
        m_isSway = false;

        yield return null;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 
    /// </summary>
    /// <param name="coord"></param>
    /// <param name="octave"></param>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    static float Fbm(Vector2 coord, int octave)
    {
        var f = 0.0f;
        var w = 1.0f;
        for (var i = 0; i < octave; i++)
        {
            f += w * Mathf.PerlinNoise(coord.x, coord.y) * 0.5f;
            coord *= 2;
            w *= 0.5f;
        }
        return f;
    }
}
