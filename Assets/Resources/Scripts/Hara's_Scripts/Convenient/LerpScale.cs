﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/03

＜内容＞
・補間を使用したスケールの変換処理。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 補間を使ってスケールを変えるクラス
/// </summary>
///-------------------------------------------------------------- 
public class LerpScale : MonoBehaviour 
{
    /*** public変数 ***/
    public float m_scaleTime = 1.0f;    // 変換する時間
    public bool m_isNowStart;           // 現在の値から始めるかのフラグ
    public Vector3 m_startScale;        // 開始時のスケール
    public Vector3 m_endScale;          // 終了時のスケール

    /*** private変数 ***/
    private bool m_isScale;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 拡縮している状態のゲッター
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    public bool GetIsScale()
    {
        return m_isScale;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        // 今のスケールを開始値にするか判断
	    if( m_isNowStart )
        {
            // 現在のスケールを開始値にする
            m_startScale = this.transform.localScale;
        }

        // フラグを拡縮していない状態にする
        m_isScale = false;
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 拡縮処理を開始する
    /// </summary>
    ///-------------------------------------------------------------- 
    public void StartScale()
    {
        // 拡縮中は処理を行わない
        if( m_isScale ) return;

        // フラグを拡縮中にする
        m_isScale = true;

        // 拡縮処理
        this.StartCoroutine(Scale());
    }


    ///-------------------------------------------------------------- 
    /// <summary>
    /// 拡縮処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator Scale()
    {
        // スケールのカウントを初期化
        float scaleCount = 0.0f;

        // カウントが時間になるまでスケールを変動させる
        while( scaleCount < m_scaleTime )
        {
            // 補間によって拡縮する
            scaleCount += Time.deltaTime;
            float lerpTime = scaleCount / m_scaleTime;
            this.transform.localScale = Vector3.Lerp( m_startScale, m_endScale, lerpTime );
            yield return null;
        }

        // フラグを拡縮しない状態にする
        m_isScale = false;
    }
}
