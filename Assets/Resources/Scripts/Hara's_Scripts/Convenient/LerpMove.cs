﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/02

＜内容＞
・補間を使用した移動処理。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 補間を使って座標を動かすクラス
/// </summary>
///-------------------------------------------------------------- 
public class LerpMove : MonoBehaviour 
{
    /*** public変数 ***/
    public float m_moveTime = 1.0f;
    public bool m_isNowStart;
    public Vector3 m_startPosition;
    public Vector3 m_endPosition;

    /*** private変数 ***/
    private bool m_isMove;

    ///--------------------------------------------------------------
    /// <summary>
    /// 動いている状態のゲッター
    /// </summary>
    /// <returns></returns>
    ///--------------------------------------------------------------
    public bool GetIsMove()
    {
        return m_isMove;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        // 今の場所をスタート地点にするか判断
	    if( m_isNowStart )
        {
            // スタート地点を今の座標からにする
            m_startPosition = this.transform.position;
        }

        // フラグを動かない状態で初期化
        m_isMove = false;
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 移動処理を開始する
    /// </summary>
    ///-------------------------------------------------------------- 
    public void StartMove()
    {
        // 移動中は移動しないようにする
        if( m_isMove ) return;

        // フラグを移動中にする
        m_isMove = true;

        // 移動処理
        this.StartCoroutine(MovePosition());
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 移動処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator MovePosition()
    {
        // 移動のカウントの初期化
        float moveCount = 0.0f;

        // カウントが時間になるまで処理を行う
        while( moveCount < m_moveTime )
        {
            // 補間による処理
            moveCount += Time.deltaTime;
            float lerpTime = moveCount / m_moveTime;
            this.transform.position = Vector3.Lerp( m_startPosition, m_endPosition, lerpTime );
            yield return null;
        }

        // フラグを動かない状態にする
        m_isMove = false;
    }
}
