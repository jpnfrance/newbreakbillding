﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/03

＜内容＞
・最大値と最低値の間でスケールを変える処理。

---------------------------------------------------------------------------------------*/
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// Sin波を使ったスケールに変化を与えるクラス
/// </summary>
///-------------------------------------------------------------- 
public class SinScale : MonoBehaviour 
{
    /*** public変数 ***/
    public float m_scaleTime = 1.0f;
    public Vector3 m_minScale = new Vector3(0.0f, 0.0f, 0.0f);
    public Vector3 m_maxScale = new Vector3(1.0f, 1.0f, 1.0f);

    /*** private変数 ***/
    private bool m_isScale;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 拡縮している状態のゲッター
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    public bool GetIsScale()
    {
        return m_isScale;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        // 拡縮していない状態で初期化
        m_isScale = false;
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 拡縮を止める
    /// </summary>
    ///-------------------------------------------------------------- 
    public void StopWave()
    {
        // フラグを拡縮していない状態にする
        m_isScale = false;

        // スケールを初期値に戻す
        this.transform.localScale = Vector3.one;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 拡縮を開始する
    /// </summary>
    ///-------------------------------------------------------------- 
    public void StartWave()
    {
        // 拡縮していたら処理を行わない
        if( m_isScale ) return;

        // 拡縮している状態にする
        m_isScale = true;

        // 拡縮処理を行う
        this.StartCoroutine( WaveScaling() );
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 最大値と最低値の間で拡縮処理する
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    public IEnumerator WaveScaling()
    {
        // カウントを初期化する
        float sinCount = 0.0f;

        // 拡縮を止める指示が来るまで処理を行う
        while( m_isScale )
        {
            // 拡縮処理
            Vector3 scale;
            sinCount += 0.007f;
            scale.x = this.Wave( sinCount, m_scaleTime, m_minScale.x, m_maxScale.x );
            scale.y = this.Wave( sinCount, m_scaleTime, m_minScale.y, m_maxScale.y );
            scale.z = this.Wave( sinCount, m_scaleTime, m_minScale.z, m_maxScale.z );

            // 変動した値を反映させる
            this.transform.localScale = scale;
            yield return null;
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// Sinを使った波の処理
    /// </summary>
    /// <param name="count"></param>
    /// <param name="time"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private float Wave( float count, float time, float min, float max )
    {
        // 拡縮処理
        return (min + (Mathf.Sin(Mathf.PI / 2 + Mathf.PI / time * count) + 2) / 2 * (max - min ));
    }
}
