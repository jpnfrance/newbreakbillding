﻿using UnityEngine;
using System.Collections;

///-------------------------------------------------------------------
/// <summary>
/// グローバル処理
/// </summary>
///-------------------------------------------------------------------
public class GlobalProcess
{
	///-------------------------------------------------------------------
	/// <summary>
	/// コンストラクタ
	/// </summary>
	///-------------------------------------------------------------------
	private GlobalProcess()
	{
	}

	///-------------------------------------------------------------------
	/// <summary>
	/// 処理
	/// </summary>
	/// <param name="scale">Scale.</param>
	///-------------------------------------------------------------------
	public static float SinWave( int count, int time, float minScale, float maxScale )
	{
		return ( minScale + ( Mathf.Sin( Mathf.PI / 2 + Mathf.PI / time * count ) + 2 ) / 2 * ( maxScale - minScale ) );
	}
}
