﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/13

＜内容＞
・キャラクターが死んだ時のアクション。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// キャラクターが死んだ時の行動のクラス
/// </summary>
///-------------------------------------------------------------- 
public class DeathAction : MonoBehaviour
{
    /*** public変数 ***/
    public Camera m_camera;
    public float m_hitTime = 1.0f;
    public float m_distance = 1.0f;

    /*** private変数 ***/
    private float m_hitCount;
    private PlayerControl m_playerControl;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake ()
    {
        // 当たるまでの時間のカウント
        m_hitCount = 0.0f;

        // プレイヤーの状態を取得する
        m_playerControl = this.GetComponent<PlayerControl>();

        // インスペクターでカメラがセットされていなかったらタグを使って取得する
        if( m_camera == null ) {
            m_camera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
        }

        // コルーチン開始
        //StartCoroutine(FlyAction());        // プレイヤーが死んで吹っ飛ぶ処理
	}

    void Update()
    {
		// デバッグ用
        // m_playerControl.Death();

    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// プレイヤーが飛んで行く処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
	public IEnumerator FlyAction()
    {
        // プレイヤーが死ぬ状態になるまでここでキープする(デバッグ用
        //while(!m_playerControl.GetIsDeath() )
        //{
        //    yield return null;
        //}

		m_hitCount = 0;
		//m_camera = Camera.current;
        // プレイヤーが吹き飛ぶための設定を行う
        m_playerControl.gravity = 0.0f;                                         // 重力をなくす
        Vector3 hitInterval = new Vector3( 0.5f, -1.0f, m_distance );            // カメラとプレイヤーとの衝突のインターバル
        Vector3 hitPosition = m_camera.transform.position + hitInterval;        // プレイヤーがカメラに衝突する時にしっかり映ることを考えてインターバル開ける
        Vector3 startPlayerPosition = transform.position;                       // プレイヤーのスタート位置を決める
        
        // プレイヤーからカメラまでの法線
        Vector3 playerToCameraVector = m_camera.transform.position - transform.position;
        transform.LookAt(playerToCameraVector);

        transform.eulerAngles += new Vector3(0.0f, 0.0f, -20.0f);
       
        // プレイヤーがカメラに衝突するまで行う
        while( m_hitCount < m_hitTime )
        {
            // 補間を使ってプレイヤーの座標をカメラの前まで移動させる
            m_hitCount += Time.deltaTime;                                                           // カウントを増やす
            float lerpTime = m_hitCount / m_hitTime;                                                // 補間の時間を出す
            transform.position = Vector3.Lerp(startPlayerPosition, hitPosition, lerpTime);          // 補間に合わせて座標を設定する
            yield return null;
        }

        this.GetComponent<Animator>().SetBool("IsDeath", true);

		//StartCoroutine(FlyAction());

        yield return null;
    }
}
