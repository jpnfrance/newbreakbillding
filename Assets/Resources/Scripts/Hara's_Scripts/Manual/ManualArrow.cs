﻿using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// マニュアルの矢印の処理
/// </summary>
///-------------------------------------------------------------- 
public class ManualArrow : MonoBehaviour 
{
    /*** public変数 ***/ 
    public GameObject m_leftArrow;
    public GameObject m_rightArrow;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake ()   
    {
	
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// すべての矢印を消す
    /// </summary>
    ///-------------------------------------------------------------- 
    public void AllArrowLost()
    {
        m_leftArrow.SetActive(false);
        m_rightArrow.SetActive(false);
    }

    public void AllArrowAppearance()
    {
        m_leftArrow.SetActive(true);
        m_rightArrow.SetActive(true);
    }

    public void LeftArrowLost()
    {
        m_leftArrow.SetActive(false);
        m_rightArrow.SetActive(true);
    }

    public void RightArrowLost()
    {
        m_leftArrow.SetActive(true);
        m_rightArrow.SetActive(false);
    }
	

}
