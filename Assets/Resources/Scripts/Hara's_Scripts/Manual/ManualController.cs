﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/08

＜内容＞
・

---------------------------------------------------------------------------------------*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GamepadInput;

///-------------------------------------------------------------- 
/// <summary>
/// マニュアルをコントロールするクラス
/// </summary>
///-------------------------------------------------------------- 
public class ManualController : MonoBehaviour 
{
    /*** public変数 ***/
    public float m_moveTime = 1.0f;
    public GameObject m_prefabManual;
    public Pause.ManualPauseState m_manualPauseState;
    public Sprite[] m_manualImageArray;

    /*** private変数 ***/
    private int m_manualCount;
    private bool m_isMove;
    private GameObject[] m_childImageArray;
    private ManualArrow m_manualArrow;

    /*** 定数 ***/
    private const float UP_TIME = 0.007f;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        // マニュアルを作る
        this.CreateManual();

        // マニュアルの番号
        m_manualCount = 0;

        // フラグを移動していない状態で初期化
        m_isMove = false;

        m_manualArrow = this.GetComponent<ManualArrow>();

        // 矢印を表示する
        this.DrawingArrow();
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 更新処理
    /// </summary>
    ///-------------------------------------------------------------- 
    void Update()
    {
        if (m_isMove) return;

        // ゲームパッドのスティックと十字ボタンの傾けた値を取得する
        Vector2 leftStickAxis = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any);
        Vector2 rightStickAxis = GamePad.GetAxis(GamePad.Axis.RightStick, GamePad.Index.Any);
        Vector2 dpadAxis = GamePad.GetAxis(GamePad.Axis.Dpad, GamePad.Index.Any);

        // ゲームパッドのスティックを傾けた量に合わせて選択を決める処理
        this.CheckTilt(leftStickAxis.y);
        this.CheckTilt(rightStickAxis.y);
        this.CheckTilt(dpadAxis.y);

        // キーボードによって移動する
        this.KeyControlMove();

        // ゲームパッドのBボタンが押されたらマニュアルを閉じる
        if( ( GamePad.GetButtonDown( GamePad.Button.B, GamePad.Index.Any ) )
            || ( Input.GetKeyDown( KeyCode.B ) ) )
        {
            // キャンセル音
            SeManager.GetInstance.Play((int)E_SeType.CURSOR_CANCEL);

            // マニュアルを閉じる
            m_manualPauseState.ClosePause();
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 傾けた量をチェックする処理
    /// </summary>
    /// <param name="x"></param>
    ///-------------------------------------------------------------- 
    private void CheckTilt(float x)
    {
        // 移動していたら処理を行わない
        if( m_isMove ) return;

        // 傾けた数値をチェックする数値
        float checkTiltUp = -0.5f;
        float checkTiltLow = 0.5f;

        // 左に傾けた時
        if (x < checkTiltUp)
        {
            if (m_manualCount > 0)
            {
                m_manualCount--;
                StartMoveLeft();
            }
        }
        // 右に傾けた時
        else if (x > checkTiltLow)
        {
            if (m_manualCount < m_manualImageArray.Length - 1)
            {
                m_manualCount++;
                StartMoveRight();
            }
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// キーボードによって移動する
    /// </summary>
    ///-------------------------------------------------------------- 
    private void KeyControlMove()
    {
        if( m_isMove ) return;

        if( Input.GetKey( KeyCode.LeftArrow ) )
        {
            if (m_manualCount > 0)
            {
                m_manualCount--;
                StartMoveLeft();
            }
        }
        else if( Input.GetKey( KeyCode.RightArrow ) )
        {
            if (m_manualCount < m_manualImageArray.Length - 1)
            {
                m_manualCount++;
                StartMoveRight();
            }
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// マニュアルを作る処理
    /// </summary>
    ///-------------------------------------------------------------- 
    private void CreateManual()
    {
        // マニュアル用のイメージの配列にデータが無かったら処理を行わないようにする
        if( m_manualImageArray.Length == 0  ) return;

        // マニュアルの数をイメージの数に合わせる
        m_childImageArray = new GameObject[m_manualImageArray.Length];

        // 配列用のカウント
        int arrayCount = 0;

        // 生成させる座標の宣言
        Vector3 createPoint = Vector3.zero;

        // 配列カウントがマニュアルの数を上回るまで続ける
        while( arrayCount < m_childImageArray.Length )
        {
            // マニュアルを生成
            createPoint = new Vector3( ( arrayCount * 300.0f * 4 ), 0.0f, 0.0f ); 
            m_childImageArray[arrayCount] = Instantiate( m_prefabManual, createPoint, Quaternion.identity ) as GameObject;

            // 生成したマニュアルと親子関係を構築
            m_childImageArray[arrayCount].transform.SetParent( this.transform );
            m_childImageArray[arrayCount].transform.localScale = Vector3.one;

            // マニュアルにイメージを適応
            m_childImageArray[arrayCount].GetComponent<Image>().sprite = m_manualImageArray[arrayCount];

            // 配列カウントを増やす
            arrayCount++;
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// マニュアルを左に移動を開始させる
    /// </summary>
    ///-------------------------------------------------------------- 
    public void StartMoveLeft()
    {
        // 移動中だったら処理を行わない
        if( m_isMove ) return;

        // カーソルの移動音
        SeManager.GetInstance.Play((int)E_SeType.CURSOR_MOVE);

        // フラグを移動中にする
        m_isMove = true;

        // マニュアルを左に移動させる
        StartCoroutine( MoveLeft() );
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// マニュアルを右に移動を開始させる
    /// </summary>
    ///-------------------------------------------------------------- 
    public void StartMoveRight()
    {
        // 移動中だったら処理を行わない
        if (m_isMove) return;

        // カーソルの移動音
        SeManager.GetInstance.Play((int)E_SeType.CURSOR_MOVE);

        // フラグを移動中にする
        m_isMove = true;

        // マニュアルを左に移動させる
        StartCoroutine(MoveRight());
    }
    
    ///-------------------------------------------------------------- 
    /// <summary>
    /// 左に移動させる処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator MoveLeft()
    {
        // 初期化
        float moveCount = 0.0f;
        float lerpTime = 0.0f;
        Vector3[] startPointArray = new Vector3[m_childImageArray.Length];
        Vector3[] endPointArray = new Vector3[m_childImageArray.Length];

        for (int i = 0; i < m_childImageArray.Length; ++i )
        {
            startPointArray[i] = m_childImageArray[i].transform.position;
            endPointArray[i] = m_childImageArray[i].transform.position + new Vector3( ( 300.0f * 4 ), 0.0f, 0.0f );
        }

        // カウントが時間を上回るまで行う
        while( moveCount < m_moveTime )
        {
            //移動中はすべての矢印を消す
            m_manualArrow.AllArrowLost();

            lerpTime = moveCount / m_moveTime;

            for (int i = 0; i < m_childImageArray.Length; ++i )
            {
                m_childImageArray[i].transform.position = Vector3.Lerp(startPointArray[i], endPointArray[i], lerpTime);
            }

            // カウントを増やす
            moveCount += UP_TIME;
            yield return null;
        }

        // 矢印を表示する
        this.DrawingArrow();

        // フラグを移動していない状態にする
        m_isMove = false;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 右に移動させる処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator MoveRight()
    {
        // 初期化
        float moveCount = 0.0f;
        float lerpTime = 0.0f;
        Vector3[] startPointArray = new Vector3[m_childImageArray.Length];
        Vector3[] endPointArray = new Vector3[m_childImageArray.Length];

        for (int i = 0; i < m_childImageArray.Length; ++i)
        {
            startPointArray[i] = m_childImageArray[i].transform.position;
            endPointArray[i] = m_childImageArray[i].transform.position - new Vector3((300.0f * 4), 0.0f, 0.0f);
        }

        // カウントが時間を上回るまで行う
        while (moveCount < m_moveTime)
        {
            //移動中はすべての矢印を消す
            m_manualArrow.AllArrowLost();

            lerpTime = moveCount / m_moveTime;

            for (int i = 0; i < m_childImageArray.Length; ++i)
            {
                m_childImageArray[i].transform.position = Vector3.Lerp(startPointArray[i], endPointArray[i], lerpTime);
            }

            // カウントを増やす
            moveCount += UP_TIME;
            yield return null;
        }

        // 矢印を表示する
        this.DrawingArrow();

        // フラグを移動していない状態にする
        m_isMove = false;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 矢印を表示する
    /// </summary>
    ///-------------------------------------------------------------- 
    private void DrawingArrow()
    {
        // 左に移動が不可能だったら右の矢印を消す
        if (m_manualCount <= 0)
        {
            m_manualArrow.LeftArrowLost();
        }
        // 右に移動が不可能だったら右の矢印を消す
        else if (m_manualCount >= m_manualImageArray.Length - 1)
        {
            m_manualArrow.RightArrowLost();
        }
        // 移動が可能だったら両方の矢印を出す
        else
        {
            m_manualArrow.AllArrowAppearance();
        }
    }
}
