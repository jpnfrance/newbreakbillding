﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/07

＜内容＞
・

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// エフェクトのマネージャクラス
/// </summary>
///-------------------------------------------------------------- 
public class EffectManager : MonoBehaviour 
{
    public GameObject[] m_prefabEffectArray;

    /*** static変数 ***/ 
    private static EffectManager m_instance;

    ///--------------------------------------------------------------
    /// <summary>
    /// ゲットインスタンス
    /// </summary>
    ///--------------------------------------------------------------
    public static EffectManager GetInstance
    {
        get
        {
            if (m_instance == null)
            {
                GameObject go = new GameObject("EffectManager");
                m_instance = go.AddComponent<EffectManager>();
            }

            return m_instance;
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        DontDestroyOnLoad(this);
	}
	
    ///-------------------------------------------------------------- 
    /// <summary>
    /// 更新処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Update () 
    {
	
	}

    public void SetEffectArray( GameObject[] array )
    {
        m_prefabEffectArray = array;
    }

    public void StartEffect( int id, Vector3 position, Quaternion rotation )
    {
        if (!m_prefabEffectArray[id]) return;

        Instantiate( m_prefabEffectArray[id], position, rotation );
    }
}
