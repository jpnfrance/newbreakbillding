﻿// 
// 

/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/17

＜内容＞
・フェードを管理する。

・フェードイン：だんだん現れる
・フェードアウト：だんだん消える

---------------------------------------------------------------------------------------*/
using UnityEngine;
using System.Collections;

///--------------------------------------------------------------
/// <summary>
/// フェードマネージャ
/// </summary>
///--------------------------------------------------------------
public class FadeManager : MonoBehaviour 
{
    /*** private変数 ***/
    private float m_waitTime;
    private float m_fadeTime;
    private bool m_isFade;
    private GameObject[] m_prefabFadeArray;

    /*** 静的変数 ***/
    private static FadeLife m_fadeLife;
    private static FadeManager m_instance;

    /*** イベント駆動型 ***/
    public delegate void FadeExcute();
    public event FadeExcute m_eventFadeExcute;

    ///--------------------------------------------------------------
    /// <summary>
    /// ゲットインスタンス
    /// </summary>
    ///--------------------------------------------------------------
    public static FadeManager GetInstance
    {
        get
        {
            if( m_instance == null )
            {
                GameObject go = new GameObject("FadeManager");
                m_instance = go.AddComponent<FadeManager>();
            }

            return m_instance;
        }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードしている状態のゲッター
    /// </summary>
    ///--------------------------------------------------------------
    public bool GetIsFade
    {
        get { return m_isFade; }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードのプレハブをセットする
    /// </summary>
    /// <param name="array"></param>
    ///--------------------------------------------------------------
    public void SetFadeArray(GameObject[] array)
    {
        if (array == null) return;
        m_prefabFadeArray = array;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// 開始処理
    /// </summary>
    ///--------------------------------------------------------------
	void Awake () 
    {
        // パラメータの初期化
        m_fadeTime = 0.0f;              // フェード時間のカウント
        m_isFade = false;               // フェードしている状態
        m_eventFadeExcute = null;       // フェードアウト終了時のイベント

        // シーン変更しても削除させない
        DontDestroyOnLoad(gameObject);
	}

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードイン開始（IDセット型）
    /// </summary>
    /// <param name="id"></param>
    ///--------------------------------------------------------------
    public void StartFadeIn(int id)
    {
        // 変更してフェードインを行う
        this.ChangeFade(id);            // 指定されたフェードに変更
        this.StartFadeIn();             // フェードインを開始する
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードイン開始
    /// </summary>
    ///--------------------------------------------------------------
    public void StartFadeIn()
    {
        // フェードが行われていなかったらフェードインを行う
        if (m_isFade == false)
        {
            // フェードしている状態に変更
            m_isFade = true;

            // フェード・イン処理を開始する
            StartCoroutine(FadeInProcess());
        }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウト開始（IDセット型）
    /// </summary>
    /// <param name="id"></param>
    ///--------------------------------------------------------------
    public void StartFadeOut( int id )
    {
        // 変更してフェードアウトを行う
        this.ChangeFade(id);                // 指定したフェードに変更
        this.StartFadeOut();                // フェードアウトを開始
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウト開始
    /// </summary>
    ///--------------------------------------------------------------
    public void StartFadeOut()
    {
        // フェードが実行している時
        if( m_fadeLife != null )
        {
            // フェードが行われていなかったらフェードアウトを行う
            if( m_isFade == false ) 
            {
                // フェードしている状態
                m_isFade = true;

                // フェードアウト処理を開始する
                StartCoroutine(FadeOutProcess());
            }
        }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードのステートの変更
    /// </summary>
    ///--------------------------------------------------------------
    private void ChangeFade(int id)
    {
        // フェードを変える前に既に設定されていたら削除処理を行う
        if (m_fadeLife != null)
        {
            Destroy(m_fadeLife.gameObject);
        }

        // 指定されたフェードを生成し、このクラスと親子関係を結ぶ
        m_fadeLife = Instantiate(m_prefabFadeArray[id]).GetComponent<FadeLife>();
        m_fadeLife.gameObject.transform.SetParent(gameObject.transform);
        m_waitTime = m_fadeLife.GetWaitTime;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードインの処理
    /// </summary>
    /// <returns></returns>
    ///--------------------------------------------------------------
    private IEnumerator FadeInProcess()
    {
        // カウントがフェード終了の時間になるまで処理を継続
        while(m_fadeTime < m_fadeLife.GetFadeInLife )
        {
            m_fadeTime += 0.02f;                                                    // カウントを加算
            float fadeRatio = m_fadeTime / m_fadeLife.GetFadeInLife;                // フェード時間の比率を算出
            m_fadeLife.FadeIn(m_fadeTime, m_fadeLife.GetFadeInLife, fadeRatio);     // カーブを使ったフェードインの処理
            yield return null;
        }

        // フェードの処理が終了したのでパラメータの初期化
        m_isFade = false;
        m_fadeTime = 0.0f;

        // 待機させるのに必要なパラメータの初期化
        int waitCount = 0;
        float waitMaxTime = m_waitTime * 60.0f;

        // フェードインが終了してからの待機時間
        while (waitCount < waitMaxTime)
        {
            // カウントを加算
            waitCount++;
            yield return null;
        }

        // イベント実行
        if( m_eventFadeExcute != null )
        {
            m_eventFadeExcute();
            m_eventFadeExcute = null;
        }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウトの処理
    /// </summary>
    /// <returns></returns>
    ///--------------------------------------------------------------
    private IEnumerator FadeOutProcess()
    {
        // カウントがフェード終了の時間になるまで処理を継続
        while(m_fadeTime < m_fadeLife.GetFadeOutLife )
        {
            m_fadeTime += 0.02f;                                                    // カウントを加算
            float fadeRatio = m_fadeTime / m_fadeLife.GetFadeOutLife;               // フェード時間の比率を算出
            m_fadeLife.FadeOut(m_fadeTime, m_fadeLife.GetFadeOutLife, fadeRatio);   // カーブを使ったフェードアウトの処理
            yield return null;
        }

        // フェードの処理が終了したのでパラメータの初期化
        m_isFade = false;
        m_fadeTime = 0.0f;
    }
}
