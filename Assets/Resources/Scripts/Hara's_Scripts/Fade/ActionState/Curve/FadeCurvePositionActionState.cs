﻿using UnityEngine;
using System.Collections;

///--------------------------------------------------------------
/// <summary>
/// カーブによって座標に変化を与えるフェードのクラス
/// </summary>
///--------------------------------------------------------------
public class FadeCurvePositionActionState : IFadeActionState
{
    /*** public変数 ***/
    public float m_magnification;
    public AnimationCurve m_fadeInPositionXLifeCurve;
    public AnimationCurve m_fadeOutPositionXLifeCurve;
    public AnimationCurve m_fadeInPositionYLifeCurve;
    public AnimationCurve m_fadeOutPositionYLifeCurve;

    /*** private変数 ***/
    private RectTransform m_rectTransform;
    private Vector3 m_position;

    ///--------------------------------------------------------------
    /// <summary>
    /// 開始処理
    /// </summary>
    ///--------------------------------------------------------------
	void Awake () 
    {
        // 座標を取得
        m_rectTransform = this.GetComponent<RectTransform>();

        // 座標を初期化
        m_position = m_rectTransform.position;
	}

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードイン時の寿命によって座標を変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    /// <param name="fadeRatio"></param>
    ///--------------------------------------------------------------
    public override void FadeInOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        // （フェードイン）今の座標をカーブで設定した座標にする
        m_position.x = (m_fadeInPositionXLifeCurve.Evaluate(fadeTime / fadeLife) * m_magnification);
        m_position.y = (m_fadeInPositionYLifeCurve.Evaluate(fadeTime / fadeLife) * m_magnification);
        m_rectTransform.localPosition = m_position;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウト時の寿命によって座標を変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    /// <param name="fadeRatio"></param>
    ///--------------------------------------------------------------
    public override void FadeOutOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        // （フェードアウト）今の座標をカーブで設定した座標にする
        m_position.x = m_fadeOutPositionXLifeCurve.Evaluate(fadeTime / fadeLife) * m_magnification;
        m_position.y = m_fadeOutPositionYLifeCurve.Evaluate(fadeTime / fadeLife) * m_magnification;
        m_rectTransform.localPosition = m_position;
    }
}
