﻿using UnityEngine;
using System.Collections;

///--------------------------------------------------------------
/// <summary>
/// カーブによってスケールに変化を与えるフェードのクラス
/// </summary>
///--------------------------------------------------------------
public class FadeCurveScaleActionState : IFadeActionState 
{
    /*** public変数 ***/
    public float m_magnification;
    public AnimationCurve m_fadeInScaleX_LifeCurve;
    public AnimationCurve m_fadeOutScaleX_LifeCurve;
    public AnimationCurve m_fadeInScaleY_LifeCurve;
    public AnimationCurve m_fadeOutScaleY_LifeCurve;

    /*** private変数 ***/
    private Vector3 m_scale;

    ///--------------------------------------------------------------
    /// <summary>
    /// 開始処理
    /// </summary>
    ///--------------------------------------------------------------
    private void Awake()
    {
        m_scale = this.transform.localScale;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードイン時の寿命によってスケールを変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    ///--------------------------------------------------------------
    public override void FadeInOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        // （フェードイン）今のスケールをカーブで設定したスケールにする
        m_scale.x = (m_fadeInScaleX_LifeCurve.Evaluate(fadeTime / fadeLife) * m_magnification);
        m_scale.y = (m_fadeInScaleY_LifeCurve.Evaluate(fadeTime / fadeLife) * m_magnification);
        transform.localScale = m_scale;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウト時の寿命でスケールを変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    ///--------------------------------------------------------------
    public override void FadeOutOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        // （フェードアウト）今のスケールをカーブで設定したスケールにする
        m_scale.x = m_fadeOutScaleX_LifeCurve.Evaluate(fadeTime / fadeLife) * m_magnification;
        m_scale.y = m_fadeOutScaleY_LifeCurve.Evaluate(fadeTime / fadeLife) * m_magnification;
        transform.localScale = m_scale;
    }
}
