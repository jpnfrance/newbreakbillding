﻿using UnityEngine;
using System.Collections;

///--------------------------------------------------------------
/// <summary>
/// フェードの行動クラスのインターフェイス
/// </summary>
///--------------------------------------------------------------
public abstract class IFadeActionState : MonoBehaviour 
{
    /*** 抽象メソッド ***/
    public abstract void FadeInOverLifetime( float fadeTime, float fadeLife, float fadeRatio );
    public abstract void FadeOutOverLifetime( float fadeTime, float fadeLife, float fadeRatio );
}