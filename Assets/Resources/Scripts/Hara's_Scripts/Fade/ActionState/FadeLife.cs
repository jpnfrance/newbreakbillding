﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

///--------------------------------------------------------------
/// <summary>
/// フェードの寿命クラス
/// </summary>
///--------------------------------------------------------------
public class FadeLife : MonoBehaviour 
{
    /*** public変数 ***/ 
    public float m_fadeInLife;
    public float m_fadeOutLife;
    public float m_waitTime = 0.0f;

    /*** private変数 ***/
    private int m_fadeActionCount;
    private IFadeActionState[] m_fadeActionStateArray;

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードインの寿命のゲッター
    /// </summary>
    ///--------------------------------------------------------------
    public float GetFadeInLife
    {
        get { return m_fadeInLife; }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウトの寿命のゲッター
    /// </summary>
    ///--------------------------------------------------------------
    public float GetFadeOutLife
    {
        get { return m_fadeOutLife; }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 待機時間のゲッター
    /// </summary>
    ///-------------------------------------------------------------- 
    public float GetWaitTime
    {
        get { return m_waitTime; }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// 開始処理
    /// </summary>
    ///--------------------------------------------------------------
	void Awake () 
    {
        // この子となっているフェードの行動クラスをすべて取得する
        m_fadeActionStateArray = this.GetComponentsInChildren<IFadeActionState>();
        m_fadeActionCount = m_fadeActionStateArray.Length;

    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードインの処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    ///--------------------------------------------------------------
    public void FadeIn( float fadeTime, float fadeLife, float fadeRatio )
    {
        // 取得した子が存在しなかったらフェードインの処理を行わない
        if( m_fadeActionStateArray == null ) return;

        // フェードを行うオブジェクトの数だけ回す
        for (int i = 0; i < m_fadeActionCount; ++i)
        {
            // フェードインの処理
            m_fadeActionStateArray[i].FadeInOverLifetime(fadeTime, fadeLife, fadeRatio);
        }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウトの処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    ///--------------------------------------------------------------
    public void FadeOut( float fadeTime, float fadeLife, float fadeRatio )
    {
        // 取得した子が存在しなかったらフェードインの処理を行わない
        if (m_fadeActionStateArray == null) return;

        // ふぇーどを行うオブジェクトの数だけ回す
        for (int i = 0; i < m_fadeActionCount; ++i)
        {
            // フェードアウトの処理
            m_fadeActionStateArray[i].FadeOutOverLifetime(fadeTime, fadeLife, fadeRatio);
        }
    }
}
