﻿using UnityEngine;
using System.Collections;

///--------------------------------------------------------------
/// <summary>
/// 補間を使ってスケールに変化を与えるフェードのクラス
/// </summary>
///--------------------------------------------------------------
public class FadeLerpScaleActionState : IFadeActionState 
{
    /*** public変数 ***/
    public Vector2 m_fadeInScale;
    public Vector2 m_fadeOutScale;

    /*** private変数 ***/
    private Vector3 m_scale;

    ///--------------------------------------------------------------
    /// <summary>
    /// 開始処理
    /// </summary>
    ///--------------------------------------------------------------
	void Awake () 
    {
        m_scale = this.transform.localScale;
	}

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードイン時の寿命によってスケールを変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    /// <param name="fadeRatio"></param>
    ///--------------------------------------------------------------
    public override void FadeInOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        m_scale.x = Mathf.Lerp( m_fadeOutScale.x, m_fadeInScale.x, fadeRatio );
        m_scale.y = Mathf.Lerp( m_fadeOutScale.y, m_fadeInScale.y, fadeRatio );
        this.transform.localScale = m_scale;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウト時の寿命によってスケールを変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    /// <param name="fadeRatio"></param>
    ///--------------------------------------------------------------
    public override void FadeOutOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        m_scale.x = Mathf.Lerp( m_fadeInScale.x, m_fadeOutScale.y, fadeRatio );
        m_scale.y = Mathf.Lerp( m_fadeInScale.y, m_fadeOutScale.y, fadeRatio );
        this.transform.localScale = m_scale;
    }
}
