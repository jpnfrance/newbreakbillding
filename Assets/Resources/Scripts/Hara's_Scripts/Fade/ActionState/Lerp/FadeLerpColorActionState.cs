﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

///--------------------------------------------------------------
/// <summary>
/// 補間を使ってカラーに変化を与えるフェードのクラス
/// </summary>
///--------------------------------------------------------------
public class FadeLerpColorActionState : IFadeActionState 
{
    /*** public変数 ***/
    public Color m_fadeInScale = new Color(1, 1, 1, 1);
    public Color m_fadeOutScale = new Color(1, 1, 1, 1);

    /*** private変数 ***/
    private Color m_color;
    private Image m_image;

    ///--------------------------------------------------------------
    /// <summary>
    /// 開始処理
    /// </summary>
    ///--------------------------------------------------------------
	void Awake () 
    {
        m_image = this.GetComponent<Image>();
        m_color = m_image.color;
	}

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードイン時の寿命によってカラーを変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    /// <param name="fadeRatio"></param>
    ///--------------------------------------------------------------
    public override void FadeInOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        m_color.r = Mathf.Lerp( m_fadeOutScale.r, m_fadeInScale.r, fadeRatio );
        m_color.g = Mathf.Lerp( m_fadeOutScale.g, m_fadeInScale.g, fadeRatio );
        m_color.b = Mathf.Lerp( m_fadeOutScale.b, m_fadeInScale.b, fadeRatio );
        m_color.a = Mathf.Lerp( m_fadeOutScale.a, m_fadeInScale.a, fadeRatio );
        m_image.color = m_color;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウト時の寿命によってカラーを変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    /// <param name="fadeRatio"></param>
    ///--------------------------------------------------------------
    public override void FadeOutOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        m_color.r = Mathf.Lerp( m_fadeInScale.r, m_fadeOutScale.r, fadeRatio );
        m_color.g = Mathf.Lerp( m_fadeInScale.g, m_fadeOutScale.g, fadeRatio );
        m_color.b = Mathf.Lerp( m_fadeInScale.b, m_fadeOutScale.b, fadeRatio );
        m_color.a = Mathf.Lerp( m_fadeInScale.a, m_fadeOutScale.a, fadeRatio );
        m_image.color = m_color;
    }
}
