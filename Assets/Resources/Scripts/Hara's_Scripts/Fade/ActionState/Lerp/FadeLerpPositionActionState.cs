﻿using UnityEngine;
using System.Collections;

///--------------------------------------------------------------
/// <summary>
/// 補間を使って座標に変化を与えるフェードのクラス
/// </summary>
///--------------------------------------------------------------
public class FadeLerpPositionActionState : IFadeActionState 
{
    /*** public変数 ***/
    public Vector2 m_fadeInPosition;
    public Vector2 m_fadeOutPosition;

    /*** private変数 ***/
    private Vector3 m_position;

    ///--------------------------------------------------------------
    /// <summary>
    /// 開始処理
    /// </summary>
    ///--------------------------------------------------------------
	void Awake () 
    {
        m_position = this.transform.localPosition;
	}

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードイン時の寿命によって座標を変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    /// <param name="fadeRatio"></param>
    ///--------------------------------------------------------------
    public override void FadeInOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        m_position.x = Mathf.Lerp(m_fadeOutPosition.x, m_fadeInPosition.x, fadeRatio);
        m_position.y = Mathf.Lerp(m_fadeOutPosition.y, m_fadeInPosition.y, fadeRatio);
        this.transform.localPosition = m_position;
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// フェードアウト時の寿命によって座標を変える処理
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="fadeLife"></param>
    /// <param name="fadeRatio"></param>
    ///--------------------------------------------------------------
    public override void FadeOutOverLifetime(float fadeTime, float fadeLife, float fadeRatio)
    {
        m_position.x = Mathf.Lerp(m_fadeInPosition.x, m_fadeOutPosition.x, fadeRatio);
        m_position.y = Mathf.Lerp(m_fadeInPosition.y, m_fadeOutPosition.y, fadeRatio);
        this.transform.localPosition = m_position;
    }
}

