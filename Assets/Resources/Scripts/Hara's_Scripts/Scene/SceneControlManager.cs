﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/14

＜内容＞
・シーンとポーズを管理する。
 
＜使い方＞ 

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// シーンをコントロールするマネージャのクラス
/// </summary>
///-------------------------------------------------------------- 
public class SceneControlManager : MonoBehaviour
{
    /*** private変数 ***/
    private bool m_isPause;

    /*** static変数 ***/
    private static SceneControlManager m_instance;

    ///--------------------------------------------------------------
    /// <summary>
    /// ゲットインスタンス
    /// </summary>
    ///--------------------------------------------------------------
    public static SceneControlManager GetInstance
    {
        get
        {
            if (m_instance == null)
            {
                GameObject go = new GameObject("SceneControlManager");
                m_instance = go.AddComponent<SceneControlManager>();
            }

            return m_instance;
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// ポーズ状態のゲッター
    /// </summary>
    ///-------------------------------------------------------------- 
    public bool GetIsPause
    {
        get { return m_isPause; }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
    void Awake()
    {
        // ポーズされていない状態で初期化する
        m_isPause = false;

        // 削除されない用意設定
        DontDestroyOnLoad(this);
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// シーンを変更する（文字列指定）
    /// </summary>
    /// <param name="nextSceneName"></param>
    ///--------------------------------------------------------------
    public void ChangeScene(string nextSceneName)
    {
        // シーンを変えるたびにポーズ状態を初期化する
        m_isPause = false;

        // シーンを変更する
        SceneManager.LoadScene(nextSceneName);
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// ポーズを追加する（文字列指定）
    /// </summary>
    /// <param name="addPauseName"></param>
    ///--------------------------------------------------------------
    public void AddPause( string addPauseName )
    {
        // ポーズ状態にする
        m_isPause = true;

        // タイマー系を止める
        Time.timeScale = 0.0f;

        // ポーズを追加する
        SceneManager.LoadScene(addPauseName, LoadSceneMode.Additive);
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// シーンの変更（クラス指定）
    /// </summary>
    /// <typeparam name="ISceneState"></typeparam>
    ///-------------------------------------------------------------- 
    public void ChangeScene<T>() where T : ISceneState
    {
        // シーンを変えるたびにポーズ状態を初期化する
        m_isPause = false;

        // シーン用の変数を作って継承を使ってクラスからシーンの名前を取得する
        GameObject go = new GameObject("Scene");
        ISceneState sceneState = go.AddComponent<T>();

        // シーンを変更する
        SceneManager.LoadScene(sceneState.GetSceneName);
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// ポーズを追加する（クラス指定）
    /// </summary>
    /// <typeparam name="T"></typeparam>
    ///-------------------------------------------------------------- 
    public void AddPause<T>() where T : IPauseState
    {
        // ポーズ状態にする
        m_isPause = true;

        Time.timeScale = 0.0f;

        // ポーズ用の変数を作って継承を使ってクラスからポーズの名前を取得する
        GameObject go = new GameObject("Pause");
        IPauseState pauseState = go.AddComponent<T>();

        // ポーズ用のシーンを追加する
        SceneManager.LoadSceneAsync(pauseState.GetPauseName, LoadSceneMode.Additive);

        // ポーズ用の変数が必要なくなったので削除
        Destroy(go);
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// ポーズを終了させる処理
    /// </summary>
    ///--------------------------------------------------------------
    public void EndPause( string endPauseName )
    {
        // ポーズ状態を解除する
        m_isPause = false;

        // タイマー系を進める
        Time.timeScale =1.0f;

        // ポーズのオブジェクトを削除する
        SceneManager.UnloadScene(endPauseName);
    }
}
