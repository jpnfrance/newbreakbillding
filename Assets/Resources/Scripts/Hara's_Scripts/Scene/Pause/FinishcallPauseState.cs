﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/07

＜内容＞
・

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// ポーズの名前空間
///-------------------------------------------------------------- 
namespace Pause
{
    ///-------------------------------------------------------------- 
    /// <summary>
    /// 終了コールのクラス
    /// </summary>
    ///-------------------------------------------------------------- 
    public class FinishcallPauseState : IPauseState
    {
        ///-------------------------------------------------------------- 
        /// <summary>
        /// 開始処理
        /// </summary>
        ///-------------------------------------------------------------- 
        void Awake()
        {
            m_pauseName = "FinishPause";

            GameManager.GetInstance.StartCull = true;
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// ポーズ状態を終了する
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ClosePause()
        {
            GameManager.GetInstance.StartCull = false;

            // コール
            SceneControlManager.GetInstance.EndPause(m_pauseName);

            // フェードインを開始
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.STENCIL);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.ResultSceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }
    }
}
