﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/15

＜内容＞
・ゲームのポーズを管理する。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// ポーズの名前空間
///-------------------------------------------------------------- 
namespace Pause
{
    ///-------------------------------------------------------------- 
    /// <summary>
    /// ゲーム中のポーズのクラス
    /// </summary>
    ///-------------------------------------------------------------- 
    public class GamePauseState : IPauseState
    {
        ///-------------------------------------------------------------- 
        /// <summary>
        /// 開始処理
        /// </summary>
        ///-------------------------------------------------------------- 
        void Awake()
        {
            m_pauseName = "GamePause";
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// ポーズを閉じる処理
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ClosePause()
        {
            GameManager.GetInstance.PauseState = false;

            // ポーズを解除する
            SceneControlManager.GetInstance.EndPause(m_pauseName);
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// タイトルシーンに戻る
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangeTitleScene()
        {
            // フェードインを開始
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.STENCIL);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.TitleSceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// ゲームをリトライする
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangeRetryPlayScene()
        {
            // フェードインを開始
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.STENCIL);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.PlaySceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// マニュアルを追加する
        /// </summary>
        ///-------------------------------------------------------------- 
        public void PopManual()
        {
            if( GameManager.GetInstance.StartCull ) return;

            GameManager.GetInstance.ManualState = true;

            // マニュアルシーンを追加する
            SceneControlManager.GetInstance.AddPause<Pause.ManualPauseState>();
        }
    }
}