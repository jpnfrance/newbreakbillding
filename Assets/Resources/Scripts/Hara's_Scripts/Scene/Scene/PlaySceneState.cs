﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/14

＜内容＞
・プレイシーンの処理。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using GamepadInput;

///-------------------------------------------------------------- 
/// シーンの名前空間
///-------------------------------------------------------------- 
namespace Scene
{
    ///-------------------------------------------------------------- 
    /// <summary>
    /// プレイシーンのステートクラス
    /// </summary>
    ///-------------------------------------------------------------- 
    public class PlaySceneState : ISceneState
    {
        /*** public変数 ***/ 
        public GameObject m_prefabGameSetup;

        /*** static変数 ***/ 
        private static PlaySceneState m_instance;

        ///-------------------------------------------------------------- 
        /// <summary>
        /// 開始処理
        /// </summary>
        ///-------------------------------------------------------------- 
        void Awake()
        {
            // シーンの名前を設定
            m_sceneName = "PlayScene";

            // 一度もセットアップされていなかったら行う
            if (!GameManager.GetInstance.GetIsSetup())
            {
                // セットアップ
                Instantiate(m_prefabGameSetup);

                // セットアップ完了した状態にする
                GameManager.GetInstance.GameSetUp();
            }

            // プレイのBGMを流す
            BgmManager.GetInstance.Play((int)E_BgmType.PLAY);

            SceneManager.LoadScene("RandStage", LoadSceneMode.Additive);

            // ゲーム開始コールを始める
            SceneControlManager.GetInstance.AddPause<Pause.StartCallPauseState>();
        }

        void Update()
        {
            if ( GameManager.GetInstance.PauseState ) return;

            if ( ( GamePad.GetButton( GamePad.Button.Start, GamePad.Index.Any ) )
                || ( Input.GetKeyDown(KeyCode.P) ) )
            {
                this.AddGamePause();
            }
        }
        
        ///-------------------------------------------------------------- 
        /// <summary>
        /// リザルトシーンを変える
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangeTheResultScene()
        {
            // フェードインを開始
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.STENCIL);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.ResultSceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// ポーズを追加する
        /// </summary>
        ///-------------------------------------------------------------- 
        public void AddGamePause()
        {
            if( GameManager.GetInstance.StartCull ) return;

            GameManager.GetInstance.PauseState = true;

            // ポーズを出す
            SceneControlManager.GetInstance.AddPause<Pause.GamePauseState>();
        }
    }
}
