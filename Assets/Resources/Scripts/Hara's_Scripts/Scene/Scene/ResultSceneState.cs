﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/14

＜内容＞
・リザルトシーンの処理。

---------------------------------------------------------------------------------------*/
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// シーンの名前空間
///-------------------------------------------------------------- 
namespace Scene
{
    ///-------------------------------------------------------------- 
    /// <summary>
    /// リザルトシーンのステートクラス
    /// </summary>
    ///-------------------------------------------------------------- 
    public class ResultSceneState : ISceneState
    {
        /*** public変数 ***/ 
        public GameObject m_prefabGameSetup;

        /*** private変数 ***/
        private int m_gameRuleType;
        private int[,] m_resultParameterArray;

        ///-------------------------------------------------------------- 
        /// <summary>
        /// 開始処理
        /// </summary>
        ///-------------------------------------------------------------- 
        void Awake()
        {
            // シーンの名前を決める
            m_sceneName = "ResultScene";

            // 一度もセットアップされていなかったら行う
            if( !GameManager.GetInstance.GetIsSetup() )
            {
                // セットアップ
                Instantiate(m_prefabGameSetup);
            }

            // セットアップ完了した状態にする
            GameManager.GetInstance.GameSetUp();

            // リザルトのBGMを流す
            BgmManager.GetInstance.Play((int)E_BgmType.RESULT);

            // プレイシーンの種類を取得する
            m_gameRuleType = GameManager.GetInstance.GameRuleType;

            // リザルトで表示するパラメータを取得する
            m_resultParameterArray = GameManager.GetInstance.ResultParameter;

        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// タイトルシーンを変える
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangeTitleScene()
        {
            // フェードインを開始
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.SIMPLE);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.TitleSceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// ゲームをリトライする
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangePlayScene()
        {
            // フェードインを開始
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.STENCIL);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.PlaySceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// エントリーシーンに変更する
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangeEntryScene()
        {
            // フェードインを開始
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.STENCIL);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.EntrySceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }
    }
}