﻿using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// シーンの名前空間
///-------------------------------------------------------------- 
namespace Scene
{
    ///-------------------------------------------------------------- 
    /// <summary>
    /// エントリーシーンのステートクラス
    /// </summary>
    ///-------------------------------------------------------------- 
    public class EntrySceneState : ISceneState
    {
        /*** public変数 ***/ 
        public GameObject m_prefabGameSetup;

        /*** private変数 ***/ 

        ///-------------------------------------------------------------- 
        /// <summary>
        /// 開始処理
        /// </summary>
        ///-------------------------------------------------------------- 
        void Awake()
        {
            // シーンの名前を決める
            m_sceneName = "EntryPlayer";

            if( !GameManager.GetInstance.GetIsSetup() )
            {
				// セットアップ
				Instantiate(m_prefabGameSetup);
            }

            // セットアップ完了した状態にする
            GameManager.GetInstance.GameSetUp();

            // エントリーシーンのBGMを鳴らす
            BgmManager.GetInstance.Play((int)E_BgmType.ENTRY);
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// タイトルシーンに変更する（戻る）
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangeTitleScene()
        {
            // フェードインを開始する
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.SIMPLE);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
                {
                    // シーン変更し、フェードアウトを行う
                    SceneControlManager.GetInstance.ChangeScene<Scene.TitleSceneState>();
                    FadeManager.GetInstance.StartFadeOut();
                };
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// プレイシーンに変更する
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChanegPlayScene()
        {
            // フェードインを開始する
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.SIMPLE);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.PlaySceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }
    }
}
