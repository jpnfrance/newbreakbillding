﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/14

＜内容＞
・タイトルシーンの処理。

---------------------------------------------------------------------------------------*/
using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// シーンの名前空間
///-------------------------------------------------------------- 
namespace Scene
{
    ///-------------------------------------------------------------- 
    /// <summary>
    /// タイトルシーンのステートクラス
    /// </summary>
    ///-------------------------------------------------------------- 
    public class TitleSceneState : ISceneState
    {
        /*** public変数 ***/ 
        public GameObject m_prefabGameSetup;

        ///-------------------------------------------------------------- 
        /// <summary>
        /// 開始処理
        /// </summary>
        ///-------------------------------------------------------------- 
        void Awake()
        {
            // シーンの名前を設定
            m_sceneName = "TitleScene";

            // 一度もセットアップされていなかったら行う
            if (!GameManager.GetInstance.GetIsSetup())
            {
                // セットアップ
                Instantiate(m_prefabGameSetup);

                // セットアップ完了した状態にする
                GameManager.GetInstance.GameSetUp();
            }

            // タイトルのBGMを流す
            BgmManager.GetInstance.Play((int)E_BgmType.TITLE);
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// バトルモードをソロで移行する
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangeSoloPlay()
        {
            // プレイモードをソロにする
            GameManager.GetInstance.GameRuleType = (int)E_GameRuleType.SOLO;

            // プレイシーンに移行
            this.ChangePlayScene();
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// ゲームルールをバトルモードで移行する
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangeBattlePlay()
        {
            // プレイモードをバトルにする
            GameManager.GetInstance.GameRuleType = (int)E_GameRuleType.BATTLE;

            // プレイシーンに移行
            this.ChangePlayScene();
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// プレイシーンの説明のシーンに変更する
        /// </summary>
        ///-------------------------------------------------------------- 
        public void PopManual()
        {
            GameManager.GetInstance.ManualState = true;

            // マニュアルを追加する
            SceneControlManager.GetInstance.AddPause<Pause.ManualPauseState>();
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// エントリーシーンに変更
        /// </summary>
        ///-------------------------------------------------------------- 
        public void ChangeEntryScene()
        {
            // プレイモードをバトルにする
            GameManager.GetInstance.GameRuleType = (int)E_GameRuleType.BATTLE;

            // フェードインを開始
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.STENCIL);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.EntrySceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }

        ///-------------------------------------------------------------- 
        /// <summary>
        /// プレイシーンを変える
        /// </summary>
        ///-------------------------------------------------------------- 
        private void ChangePlayScene()
        {
            // フェードインを開始
            FadeManager.GetInstance.StartFadeIn((int)E_FadeType.STENCIL);

            // フェードインが終了時に行う処理を設定
            FadeManager.GetInstance.m_eventFadeExcute += () =>
            {
                // シーン変更し、フェードアウトを行う
                SceneControlManager.GetInstance.ChangeScene<Scene.PlaySceneState>();
                FadeManager.GetInstance.StartFadeOut();
            };
        }
    }
}
