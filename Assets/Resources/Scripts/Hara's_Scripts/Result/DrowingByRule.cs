﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/01/22

＜内容＞
・

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

///-------------------------------------------------------------- 
/// <summary>
/// ルールによって表示するものを決めるクラス
/// </summary>
///-------------------------------------------------------------- n
public class DrowingByRule : MonoBehaviour 
{
    /*** public変数 ***/ 
    public GameObject m_soloPlayerParameter;
    public GameObject[] m_buttlePlayersParameter;

    /*** private変数 ***/
    private int m_playerNum;
    private int m_winnerGoalNum;
    private int[,] m_resultParameter;
    private List<int> m_players;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        ////=====================================================================
        //// デバッグ用にパラメータを設定する
        //DebugSetParameter();

        //// デバック用
        //GameManager.GetInstance.GameRuleType = (int)E_GameRuleType.BATTLE;
        //GameManager.GetInstance.PlayerNum = 1;
        ////=====================================================================

        // パラメータを取得
        m_resultParameter = GameManager.GetInstance.ResultParameter;

        // エントリーするプレイヤーの参加状態のリストを取得
        m_players = GameManager.GetInstance.EntryPlayers;

        //m_playerNum = GameManager.GetInstance.PlayerNum;         // プレイヤーの数を取得する
        GameObject[] parameters = new GameObject[4];               // 作る出す枠をプレイヤーの数にする

        // 最高得点を決める
        this.DecideWinner();

        // プレイヤーの数だけ表示する枠を生成
        for (int i = 0; i < 4; ++i)
        {
            Debug.Log("来た");

            if (m_players.Contains(i + 1))
            {
                Debug.Log("生成");

                // 参加したプレイヤーの数だけリザルトで表示する枠を生成する
                parameters[i] = Instantiate(m_buttlePlayersParameter[i]) as GameObject;
                parameters[i].transform.SetParent(transform, false);
                parameters[i].transform.localScale = Vector3.one;

                if (m_winnerGoalNum == m_resultParameter[i, (int)E_ResultParameterType.PLAYER_GOAL])
                {
                    parameters[i].GetComponent<IssueProduction>().Winner();
                }
                else
                {
                    parameters[i].GetComponent<IssueProduction>().Loser();
                }
            }
        }

        //-----------------------------------------------------------------------------------------------

        //// ソロプレイ時
        //if (GameManager.GetInstance.GameRuleType == (int)E_GameRuleType.SOLO)
        //{
        //    // 表示する枠を生成
        //    parameters[(int)E_ControlPlayerType.ONE] = Instantiate(m_soloPlayerParameter) as GameObject;
        //    parameters[(int)E_ControlPlayerType.ONE].transform.SetParent(transform, false);
        //    parameters[(int)E_ControlPlayerType.ONE].transform.localScale = Vector3.one;
        //}
        //// バトルプレイ時
        //else
        //{
        //    // 最高得点を決める
        //    this.DecideWinner();

        //    // プレイヤーの数だけ表示する枠を生成
        //    for (int i = 0; i < m_playerNum; ++i)
        //    {
        //        // 参加したプレイヤーの数だけリザルトで表示する枠を生成する
        //        parameters[i] = Instantiate(m_buttlePlayersParameter[i]) as GameObject;
        //        parameters[i].transform.SetParent( transform, false );
        //        parameters[i].transform.localScale = Vector3.one;

        //        if (m_winnerGoalNum == m_resultParameter[i, (int)E_ResultParameterType.PLAYER_GOAL])
        //        {
        //            parameters[i].GetComponent<IssueProduction>().Winner();
        //        }
        //        else
        //        {
        //            parameters[i].GetComponent<IssueProduction>().Loser();
        //        }
        //    }
        //}

        // ゲームプレイでのパラメータを取得する
        PlayerSetParameter[] playerSetParameters = new PlayerSetParameter[4];

        // それぞれのプレイヤーのパラメータを取得する
        for( int i = 0; i < 4; ++i )
        {
            if (m_players.Contains(i + 1))
            {
                playerSetParameters[i] = parameters[i].GetComponent<PlayerSetParameter>();
                playerSetParameters[i].SetParameterGoalNum(m_resultParameter[i, (int)E_ResultParameterType.PLAYER_GOAL]);
                playerSetParameters[i].SetParameterDeathNum(m_resultParameter[i, (int)E_ResultParameterType.PLAYER_DEATH]);
                playerSetParameters[i].SetParameterPutBombNum(m_resultParameter[i, (int)E_ResultParameterType.PLAYER_PUT_BOMB]);
            }
        }
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 勝利者の点数を決める
    /// </summary>
    ///-------------------------------------------------------------- 
    private void DecideWinner()
    {
        // ソート用の配列を初期化
        int[] sortArray = new int[4];
        List<int> sortList = new List<int>();

        // プレイヤーの数だけ回る
        for (int i = 0; i < 4; ++i)
        {
            if (m_players.Contains(i + 1))
            {
                // ソート用の配列にゴールした数を設定していく
                sortArray[i] = m_resultParameter[i, (int)E_ResultParameterType.PLAYER_GOAL];
            }
        }

        // ソートする
        sortList.AddRange(sortArray);
        sortList.Sort();
        sortList.Reverse();

        // 勝利者のゴールした数を取得する
        m_winnerGoalNum = sortList[0];            
    }
}
