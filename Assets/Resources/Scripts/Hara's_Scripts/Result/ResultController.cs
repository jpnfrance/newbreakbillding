﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/02

＜内容＞
・

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;
using GamepadInput;

///-------------------------------------------------------------- 
/// <summary>
/// リザルトシーンをコントロールするクラス
/// </summary>
///-------------------------------------------------------------- 
public class ResultController : MonoBehaviour
{
    /*** public変数 ***/
    public Scene.ResultSceneState m_resultScene;
    public LerpMove m_camera;
    public SelectCharacterMove m_selectCharacterMove;
    public SelectTypeImageScaling m_selectTypeImageScaling;
    public SelectedImage m_selectedImage;
    public SelectedRoom m_selectedRoom;

    /*** private変数 ***/ 
    private LimitCounter m_limitCounter;

    /*** 定数 ***/ 
    private const int SCENE_ENTRY = 0;
    private const int SCENE_PLAY = 1;
    private const int SCENE_TITLE = 2;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
    void Awake()
    {
        m_limitCounter = this.GetComponent<LimitCounter>();
        StartCoroutine(WaitNextInput());
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 更新処理
    /// </summary>
    ///-------------------------------------------------------------- 
    void Update()
    {
        m_selectedImage.DrawingImage(m_limitCounter.GetCount());
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// リザルトから選択画面に移行するまで待機させる処理
    /// </summary>
    /// <returns></returns>
    ///-------------------------------------------------------------- 
    private IEnumerator WaitNextInput()
    {
        // ループ状態のフラグの初期化
        bool isRoop = true;

        // ボタンが押されるまで待機（ループ）
        while (isRoop)
        {
            // ゲームパッドのAボタンが押されたらカメラを移動させる
            // キーボードのAボタンが押されたらカメラを移動させる
            if ( ( GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Any ) )
                || ( Input.GetKeyDown(KeyCode.A ) ) )
            {
                // 決定音
                SeManager.GetInstance.Play((int)E_SeType.CURSOR_OK);

                m_camera.StartMove();
                StartCoroutine(SelectControl());
                isRoop = false;
            }

            yield return null;
        }
    }

    ///--------------------------------------------------------------
    /// <summary>
    /// 次に行う事を選択する
    /// </summary>
    /// <returns></returns>
    ///--------------------------------------------------------------
    private IEnumerator SelectControl()
    {
        // カメラが動いている間は選択させないようにする
        while (m_camera.GetIsMove()) yield return null;

        // フラグをループ状態で初期化
        bool isRoop = true;

        // ループ
        while( isRoop )
        {
            // 選択されている番号に合わせて拡縮するイメージを決める
            m_selectTypeImageScaling.ScaleControl(m_limitCounter.GetCount());

            // ゲームパッドのスティックと十字ボタンの傾けた値を取得する
            Vector2 leftStickAxis = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any);
            Vector2 rightStickAxis = GamePad.GetAxis(GamePad.Axis.RightStick, GamePad.Index.Any);
            Vector2 dpadAxis = GamePad.GetAxis(GamePad.Axis.Dpad, GamePad.Index.Any);

            // ゲームパッドのスティックを傾けた量に合わせて選択を決める処理
            this.CheckTilt(leftStickAxis.x);
            this.CheckTilt(rightStickAxis.x);
            this.CheckTilt(dpadAxis.x);

            // キーボードによって移動を行う
            this.KeyControlMove();

            // 動いている時は決定させないようにする
            if (!m_selectCharacterMove.GetIsMove())
            {
                // ゲームパッドのAボタンが押されたらシーンを変える
                if ((GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Any))
                    || (Input.GetKeyDown(KeyCode.A)))
                {
                    // 決定音
                    SeManager.GetInstance.Play((int)E_SeType.BURST_NORMAL);

                    // 選択した所に爆発の演出を出す
                    EffectManager.GetInstance.StartEffect(
                        (int)E_EffectType.NORMAL_BURST,
                        m_selectCharacterMove.gameObject.transform.position,
                        Quaternion.identity);

                    // 次に進むシーンが決まったら削除するもの
                    m_selectTypeImageScaling.DestroySelectedImage( m_limitCounter.GetCount() );
                    m_selectedRoom.DestroySelectedRoom( m_limitCounter.GetCount() );
                    m_selectCharacterMove.DestroyPlayer();

                    // 選択している物を表示するイメージを点滅させる
                    m_selectedImage.StartFlash();

                    // 決定した時に選択している番号によって処理を決める
                    switch (m_limitCounter.GetCount())
                    {
                        // キャラクターセレクトシーンに変更する
                        case SCENE_ENTRY:
                            m_resultScene.ChangeEntryScene();
                            break;

                        // プレイシーンに変更する
                        case SCENE_PLAY:
                            m_resultScene.ChangePlayScene();
                            break;

                        // タイトルシーンに変更する
                        case SCENE_TITLE:
                            m_resultScene.ChangeTitleScene();
                            break;
                    }

                    // ループから抜けるようにする
                    isRoop = false;
                }
            }

            yield return null;
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// キーボードによって移動をする
    /// </summary>
    ///-------------------------------------------------------------- 
    private void KeyControlMove()
    {
        // 移動中だったら処理を行わない
        if (m_selectCharacterMove.GetIsMove()) return;

        // 左に移動する
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            m_limitCounter.CountDown();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
        // 右に移動する
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            m_limitCounter.CountUp();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 傾けた量をチェックする処理
    /// </summary>
    /// <param name="x"></param>
    ///-------------------------------------------------------------- 
    private void CheckTilt(float x)
    {
        // 選択用のキャラクターが動いていたら処理を行わない
        if( m_selectCharacterMove.GetIsMove() ) return;

        // 傾けた数値をチェックする数値
        float checkTiltLeft = -0.5f;
        float checkTiltRight = 0.5f;

        // 左に傾けた時
        if ( x < checkTiltLeft )
        {
            m_limitCounter.CountDown();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove( count );
        }
        // 右に傾けた時
        else if( x > checkTiltRight )
        {
            m_limitCounter.CountUp();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
    }

}
