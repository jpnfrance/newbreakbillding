﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/05

＜内容＞
・

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// 勝敗によって演出を決めるクラス
/// </summary>
///-------------------------------------------------------------- 
public class IssueProduction : MonoBehaviour
{
    /*** public変数 ***/ 
    public GameObject m_prefabWinnerProduction;
    public GameObject m_prefabLoserProduction;
    public Sprite m_winnerImage;
    public Sprite m_loserImage;

    /*** private変数 ***/
    private bool m_isIssue;
    private DrowingByRule m_drawingByRule;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 勝者にする
    /// </summary>
    ///-------------------------------------------------------------- 
    public void Winner()
    {
        m_isIssue = true;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 敗者にする
    /// </summary>
    ///-------------------------------------------------------------- 
    public void Loser()
    {
        m_isIssue = false;
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Start ()
    {
        GameObject childIssueImage = transform.FindChild("GameObject/Image_IssueImage").gameObject;
        Image issueImage = childIssueImage.GetComponent<Image>();

        // 子になっているキャラクターを取得する
        GameObject childCharacter = transform.FindChild("GameObject/Character").gameObject;
        GameObject issue;

        // 勝敗によって出す演出を決める
        if( m_isIssue )
        {
            // 勝者の演出
            issue = Instantiate(m_prefabWinnerProduction, childCharacter.transform.position + new Vector3(0.0f, 1.0f, 1.0f), Quaternion.identity) as GameObject;
            issue.transform.SetParent(transform);

            // 勝者のイメージを適応させる
            issueImage.sprite = m_winnerImage;
        }
        else
        {
            // 敗者の演出
            issue = Instantiate(m_prefabLoserProduction, childCharacter.transform.position + new Vector3(0.0f, 1.0f, 2.0f), Quaternion.identity) as GameObject;
            issue.transform.SetParent(transform);

            // 勝者のイメージを適応させる
            issueImage.sprite = m_loserImage;
        }
	}
}