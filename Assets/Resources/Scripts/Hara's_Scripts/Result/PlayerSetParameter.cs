﻿using UnityEngine;
using System.Collections;

///-------------------------------------------------------------- 
/// <summary>
/// ソロプレイのプレイヤーのパラメータをセットするクラス
/// </summary>
///-------------------------------------------------------------- 
public class PlayerSetParameter : MonoBehaviour 
{
    /*** public変数 ***/
    public DrawResultParameter m_parameterGoalNum;
    public DrawResultParameter m_parameterPutBombNum;
    public DrawResultParameter m_parameterDeathNum;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// ゴール数のセッター
    /// </summary>
    /// <param name="parameter"></param>
    ///-------------------------------------------------------------- 
    public void SetParameterGoalNum( int parameter )
    {
        // ゴール数をセットする
        m_parameterGoalNum.SetParameter(parameter);
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 死んだ数のセッター
    /// </summary>
    /// <param name="parameter"></param>
    ///-------------------------------------------------------------- 
    public void SetParameterDeathNum(int parameter)
    {
        // 死んだ数をセットする
        m_parameterDeathNum.SetParameter(parameter);
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 爆弾を置いた数のセッター
    /// </summary>
    /// <param name="parameter"></param>
    ///-------------------------------------------------------------- 
    public void SetParameterPutBombNum(int parameter)
    {
        // 爆弾の置いた数をセットする
        m_parameterPutBombNum.SetParameter(parameter);
    }
}
