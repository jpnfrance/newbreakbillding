﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/03

＜内容＞
・次にボタンを押させるように誘導するためのアニメーションを行う。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

///-------------------------------------------------------------- 
/// <summary>
/// ボタンのUIクラス
/// </summary>
///-------------------------------------------------------------- 
public class ButtonUI : MonoBehaviour 
{
    /*** public変数 ***/
    public Sprite m_noPushButtonImage;
    public Sprite m_pushbuttonImage;
    public float m_changeImageTime = 1.0f;

    /*** private変数 ***/
    private float m_changeImageCount;
    private bool m_isPushImage;
    private Image m_image;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        m_image = this.GetComponent<Image>();
	}

    ///-------------------------------------------------------------- 
	/// <summary>
	/// 更新処理
	/// </summary>
    ///-------------------------------------------------------------- 
	void Update () 
    {
        // カウントを増やす
        m_changeImageCount += Time.deltaTime;

        // 時間になったらボタンのイメージを変更する
	    if( m_changeImageCount > m_changeImageTime )
        {
            // カウントを初期化
            m_changeImageCount = 0.0f;

            // フラグによってイメージを設定する
            if (m_isPushImage)
            {
                // 押されていないボタンのイメージに変更
                m_image.sprite = m_noPushButtonImage;
                m_isPushImage = false;
            }
            else
            {
                // 押されていないボタンのイメージに変更
                m_image.sprite = m_pushbuttonImage;
                m_isPushImage = true;
            }

        }
	}
}
