﻿/*------------------------------------------------------------------------------------

◆ 制作社：原昌志
◆ 制作日：2016/02/05

＜内容＞
・ポーズをコントロールする。

---------------------------------------------------------------------------------------*/ 
using UnityEngine;
using System.Collections;
using GamepadInput;

///-------------------------------------------------------------- 
/// <summary>
/// ポーズをコントロールする
/// </summary>
///-------------------------------------------------------------- 
public class PauseController : MonoBehaviour 
{
    /*** public変数 ***/
    public Pause.GamePauseState m_gamePauseState;
    public SelectCharacterMove m_selectCharacterMove;
    public SelectTypeImageScaling m_selectTypeImageScaling;
    public SelectedImage m_selectedImage;

    /*** private変数 ***/
    private LimitCounter m_limitCounter;

    /*** 定数 ***/
    private const int SCENE_ENTRY = 0;
    private const int SCENE_END_PAUSE = 1;
    private const int SCENE_PLAY = 2;
    private const int SCENE_POP_MANUAL = 3;

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 開始処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Awake () 
    {
        m_limitCounter = this.GetComponent<LimitCounter>();
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 更新処理
    /// </summary>
    ///-------------------------------------------------------------- 
	void Update () 
    {
        // マニュアルが出ていたら処理を行わない
        if( GameManager.GetInstance.ManualState ) return;

        // ゲームパッドのBボタンが押されたらポーズを終了する
        if ((GamePad.GetButtonDown(GamePad.Button.B, GamePad.Index.Any))
            || (Input.GetKeyDown(KeyCode.B)))
        {
            // キャンセル音
            SeManager.GetInstance.Play( (int)E_SeType.CURSOR_CANCEL );

            // ポーズを終了させる
            m_gamePauseState.ClosePause();
        }

        // 選択している項目に合わせてイメージを切り替える
        m_selectedImage.DrawingImage(m_limitCounter.GetCount());

        // 選択されている番号に合わせて拡縮するイメージを決める
        m_selectTypeImageScaling.ScaleControl(m_limitCounter.GetCount());

        // ゲームパッドによってカーソル用プレイヤーの移動をコントロールする
        this.ControlGamePadMovePlayer();

        // キーボードによって移動を行う
        this.KeyControlMove();

        // 動いている時は決定させないようにする
        if (!m_selectCharacterMove.GetIsMove())
        {
            // ゲームパッドのAボタンが押されたらシーンを変える
            if ((GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Any))
                || (Input.GetKeyDown(KeyCode.A)))
            {
                // 決定音
                SeManager.GetInstance.Play( (int)E_SeType.BURST_NORMAL );

                // 決定した時に選択している番号によって処理を決める
                switch (m_limitCounter.GetCount())
                {
                    // キャラクターセレクトシーンに変更する
                    case SCENE_ENTRY:
                        m_gamePauseState.ChangeRetryPlayScene();
                        break;

                    // ポーズを終了する
                    case SCENE_END_PAUSE:
                        m_gamePauseState.ClosePause();
                        break;

                    // プレイシーンに変更する
                    case SCENE_PLAY:
                        m_gamePauseState.ChangeTitleScene();
                        break;

                        // マニュアルを出す
                    case SCENE_POP_MANUAL:
                        m_gamePauseState.PopManual();
                        break;
                }
            }
        }
	}

    ///-------------------------------------------------------------- 
    /// <summary>
    /// ゲームパッドによってカーソル用プレイヤーの移動をコントロールする
    /// </summary>
    ///-------------------------------------------------------------- 
    private void ControlGamePadMovePlayer()
    {
        // ゲームパッドのスティックと十字ボタンの傾けた値を取得する
        Vector2 leftStickAxis = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any);
        Vector2 rightStickAxis = GamePad.GetAxis(GamePad.Axis.RightStick, GamePad.Index.Any);
        Vector2 dpadAxis = GamePad.GetAxis(GamePad.Axis.Dpad, GamePad.Index.Any);

        // ゲームパッドのスティックを傾けた量に合わせて選択を決める処理
        this.CheckTilt(leftStickAxis.x);
        this.CheckTilt(rightStickAxis.x);
        this.CheckTilt(dpadAxis.x);
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// キーボードによって移動をする
    /// </summary>
    ///-------------------------------------------------------------- 
    private void KeyControlMove()
    {
        // 移動中だったら処理を行わない
        if (m_selectCharacterMove.GetIsMove()) return;

        // 左キーを押すと左に移動し始める
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            m_limitCounter.CountDown();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
        // 右キーを押すと右に移動し始める
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            m_limitCounter.CountUp();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
    }

    ///-------------------------------------------------------------- 
    /// <summary>
    /// 傾けた量をチェックする処理
    /// </summary>
    /// <param name="x"></param>
    ///-------------------------------------------------------------- 
    private void CheckTilt(float x)
    {
        // 選択用のキャラクターが動いていたら処理を行わない
        if (m_selectCharacterMove.GetIsMove()) return;

        // 傾けた数値をチェックする数値
        float checkTiltLeft = -0.5f;
        float checkTiltRight = 0.5f;

        // 左に傾けた時
        if (x < checkTiltLeft)
        {
            m_limitCounter.CountDown();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
        // 右に傾けた時
        else if (x > checkTiltRight)
        {
            m_limitCounter.CountUp();
            int count = m_limitCounter.GetCount();
            m_selectCharacterMove.StartMove(count);
        }
    }
}
